<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">



            <p>Third Coast Terminals is the Premier Chemical Service Company of Choice.</p>


            <p>Third Coast Terminals is a unique provider in the chemical industry, offering services such as Reaction Chemistry, Toll Processing, Chemical Logistics, Terminaling, Blending, and Packaging.</p>
            
            <p>Strategically located in Pearland, Texas (2 miles South of Beltway 8 &amp; near Hobby Airport), we are in the heart of the nation's largest petrochemical center, and next door to major ocean shipping facilities for the U.S. Gulf Coast.</p>


            <h4>Our Quality / Safety Mission</h4>

            <p>&ldquo;Meeting the quality needs and expectations of our customers is our highest priority. Third Coast is committed to ensuring customer satisfaction is achieved at all times. This is accomplished by providing the highest quality service and by continually improving our quality management system.&rdquo;</p>

            <p>Third Coast adheres to the following Quality Objectives:</p>

            <p></p>
            <ul>
                <li>
                    <p>Customer  satisfaction measured by the results of our Customer Feedback Report.</p>
                </li>

                <li>
                    <p>Continually  improving our quality management system by progressively reducing the number of non-conformances.</p>
                </li>

                <li>
                    <p>Third Coast Terminals assesses key performance indicators and integrates best practices, driving towards continuous improvement.</p>
                </li>
            </ul>
            <p></p>

            <p>It is our belief that, above and beyond success in business, our team member’s health and safety must come first. Our team members are our most valuable asset we have and without them Third Coast could not exist or grow. A healthy and safe work environment is a right to which every employee is entitled, and we intend to preserve that right with them.</p>

            <br/>

        </div>


        <div class="col-lg-6">

            <img src="assets/images/tct-general-operations.jpg" title="About Third Coast Terminals"
            alt="About Third Coast Terminals" class="img-responsive" height="75%" width="75%" style="max-height: 75%; max-width: 75%; float: 50%"><br>
            <p>Third Coast Terminals commits to: <br>

                <ul style="list-style: none;">

                    <li>Provide a safe and healthy workplace for our team members.</li>
                    <li>Provide dependability with consistent quality results.</li>
                    <li>Provide the highest level of service to our customer’s at the most competitive prices.</li>
                    <li>Create a partnership with all suppliers, customers and stakeholders.</li>

                </ul></p>  
            </div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
<br><br><br>
            <div class="col-lg-6" style="margin-left: 107px; width: 100% !important;">
               <video width="852" poster="assets/images/video-poster.png" controls>
                  <source src="assets/video/ThirdCoastHistoryWeb.mp4" type="video/mp4" >

                  </video>

              </div>
              <div class="clearfix"></div>
              <div class="clearfix"></div>
              <br>
              <div class="col-lg-6 contact-form">
                <div class="headline">
                    <h2 title="Contact Third Coast Terminals">
                        Send Us a Message
                    </h2>
                </div>
                <br>

                <form action="" method="post"
                class=" contact-style">
                <fieldset class="no-padding">

                    <input type="text" name="full_name" id="name" class="form-control" placeholder="Full Name *">
                    <input type="text" name="phone" id="name" class="form-control" placeholder="Phone *"> <input
                    type="text" name="email" id="email" class="form-control" placeholder="Email *">
                    <input type="text" name="company" id="email" class="form-control" placeholder="Company Name *">
                    <textarea class="form-control" name="message" placeholder="Message..."></textarea>

                    <p>
                        <button type="submit" class="btn  btn-primary btn-blue btn-contact">Send Message</button>
                    </p>
                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form>
        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
                        =========================================================-->
                        <?php include "footer.php" ?>
