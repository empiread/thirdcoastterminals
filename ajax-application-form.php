<?php

if(isset($_POST['first_name'])){

	$first_name = trim($_POST['first_name']);
    $last_name  = trim($_POST['last_name']);	
    $middle_name  = trim($_POST['middle_name']);	
    $street_address  = trim($_POST['street_address']);	
    $apt  = trim($_POST['apt']);	
    $city  = trim($_POST['city']);	
    $state  = trim($_POST['state']);	
    $zipcode  = trim($_POST['zipcode']);	
	$email = trim($_POST['email']);	
	$phone   = trim($_POST['phone']);
	
	if( $first_name != '' && $email != '' && $last_name != ''){
    if (!filter_var($email, FILTER_VALIDATE_EMAIL) === false) {	 	
		// subject
		$subject = 'Contact Form';
		// message
		$message  = '<table><tr><td><strong>First Name</strong></td><td>:</td><td>&nbsp;</td><td>'.$first_name.'</td></tr>';
		$message .= '<table><tr><td><strong>Last Name</strong></td><td>:</td><td>&nbsp;</td><td>'.$last_name.'</td></tr>';
		$message .= '<table><tr><td><strong>Middle Name</strong></td><td>:</td><td>&nbsp;</td><td>'.$middle_name.'</td></tr>';
		$message .= '<table><tr><td><strong>Street Address</strong></td><td>:</td><td>&nbsp;</td><td>'.$street_address.'</td></tr>';
		$message .= '<table><tr><td><strong>Apt</strong></td><td>:</td><td>&nbsp;</td><td>'.$apt.'</td></tr>';
		$message .= '<table><tr><td><strong>City</strong></td><td>:</td><td>&nbsp;</td><td>'.$city.'</td></tr>';
		$message .= '<table><tr><td><strong>State</strong></td><td>:</td><td>&nbsp;</td><td>'.$state.'</td></tr>';
		$message .= '<table><tr><td><strong>Zipcode</strong></td><td>:</td><td>&nbsp;</td><td>'.$zipcode.'</td></tr>';		
		$message .= '<tr><td><strong>Phone</strong></td><td>:</td><td>&nbsp;</td><td>'.$phone.'</td></tr>';
		$message .= '<tr><td><strong>Email</strong></td><td>:</td><td>&nbsp;</td><td>'.$email.'</td></tr></table>';			
		
		$email_from = 'info@thirdcoastterminals.com';

		$to = "hyder@obacksys.com";
		

		// Always set content-type when sending HTML email

		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		
		// Additional headers				

		$headers .= 'From: '.$email_from."\r\n". 					
					'X-Mailer: PHP/' . phpversion();	

		/*$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";*/        

		// Mail it

		if(mail($to, $subject, $message, $headers)){
			echo json_encode(array("type"=>"success","data"=>'<span class="success"><strong>Application Form Submitted!</strong> We will be in touch soon. Thank you.</span>'));
		}else{
			echo json_encode(array("type"=>"failure","data"=>'<span class="error">Email not sent. Please try again.</span>'));
		}			

	 }else{
		echo json_encode(array("type"=>"failure","data"=>'<span class="error">Please enter valid email address</span>'));
	}	
	}else{
	  echo json_encode(array("type"=>"failure","data"=>'<span class="error">Please fill all the fields</span>'));     		 
	}	 

}else{
	echo json_encode(array("type"=>"failure","data"=>'<span class="error">Please fill all the fields</span>'));     		 
}

?>