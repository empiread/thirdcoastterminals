
function loadGalleryTemplate($template_name){
	switch ($template_name){
      case "dynamo_game":        
	   $template = $dynamo_game;
       break;
	  
      case "crawfish_boil":	   
	   $template = $crawfish_boil;	   
       break;
	   
	  case "astros_game":
	   $template = $astros_game;
       break;
	   
      case "breakfast":
	   $template = $breakfast;
       break;
	   
	  case "summer_picnic":
	  $template = $summer_picnic;
       break;
	   
      case "christmas_party":	  
	  $template = $christmas_party;
       break;
      case "thanksgiving_lunch":	  
	  $template = $thanksgiving_lunch;
       break;
      case "speical_events":	  
	   $template =  $special_events;
       break;

      default: 
       alert('gallery not able to load');
       break;
   }
   return $template;
}

$().ready(function() {
	  $('.load_carousel').click(function(e){
		 e.preventDefault(); 
		 $('.load_carousel').removeClass('active');
		 $(this).addClass('active');
		 var template = $(this).data('template');
		 template = loadGalleryTemplate(template)
		 $('#myCarousel').html(template);
	  });
	  
	  
	  // validate signup form on keyup and submit
		$("#applicationForm").validate({
			rules: {
				last_name: "required",
				first_name: "required",
				middle_name: "required",
				street_address: "required",
				apt: "required",
				city: "required",
				state: "required",
				zipcode: "required",
				phone: "required",
				email: {
					required: true,
					email: true
				}
			},
			messages: {
				last_name: "",
				first_name: "",
				middle_name: "",
				street_address: "",
				apt: "",
				city: "",			
				state: "",
				zipcode: "",
                phone: "",				
				email: ""
			},
			submitHandler: function() {
			var dataString = $( "#applicationForm" ).serialize();
				   $.ajax({
							type:"POST",
							url:"ajax-application-form.php",
							data:dataString,
							dataType : "json",
							 beforeSend: function() {      
							 $("#application-form-submit").attr("disabled", "disabled");	
							 $('#response').html('<img src="images/ajax-loader.gif">');							 
							}, 
							success:function (response) {
								if(response.type == 'success'){
									$('#response').html(response.data);										
									$('#applicationForm')[0].reset();
								}else{
									$('#response').html(response.data);
								}
								$('#application-form-submit').removeAttr("disabled");	
							}
						});        
						return false;
		}
		});
});