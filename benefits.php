<?php include "header.php"
?>

<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

            

            <h3>Challenging work can be very rewarding. </h3>

            <p>Our competitive benefits program is meant to enhance
                and preserve your work/life balance and help you plan and prepare for tomorrow.
            </p>
            
            <div class="col-lg-12"><br><br><br></div>
            
            <div class="col-lg-12">
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/HealthInsurance.png" title="Health Insurance" alt="Health Insurance"></a>
					<p class="icons">MEDICAL</p>
				</div>
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/Dental.png" title="Dental" alt="Dental"></a>
					<p class="icons">DENTAL</p>
						
				</div>

				<div class="col-lg-3" style="text-align:center;">
					<img src="images/Pharmacy.png" title="Pharmacy" alt="Pharmacy"></a>
					<p class="icons">PHARMACY<br>COVERAGE</p>
				</div>
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/Vision.png" title="Vision" alt="Vision"></a>
					<p class="icons">VISION</p>
				</div>
			</div class="col-lg-12">
			<div class="col-lg-12"><br><br><br></div>
			<div class="col-lg-12">
				
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/LifeInsurance.png" title="Life Insurance" alt="Life Insurance"></a>
					<p class="icons">LIFE INSURANCE</p>
				</div>
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/PaidHolidays.png" title="Paid Holidays" alt="Paid Holidays"></a>
					<p class="icons">11 COMPANY<br>PAID HOLIDAYS</p>
				</div>

				<div class="col-lg-3" style="text-align:center;">
					<img src="images/401K.png" title="401K" alt="401K"></a>
					<p class="icons">4% 401K<br>MATCH</p>
				</div>
				<div class="col-lg-3" style="text-align:center;">
					<img src="images/DirectDeposit.png" title="Direct Deposit" alt="Direct Deposit"></a>
					<p class="icons">DIRECT<br>DEPOSIT</p>
				</div>
				
			</div class="col-lg-12">
			<div class="col-lg-12"><br><br><br></div>
			<div class="col-lg-12">
			<h3>Third Coast Terminals is dedicated to taking care of their employees. In addition to all of the standard benefits offered above, Third Coast Terminals also offers: </h3>
				<div class="col-lg-4">
					<p> 
               				<strong>Company Paid:</strong>
            		</p>
            		<ul>
            			<li> Short Term Disability</li>
            			<li> Onsite Annual Physicals</li>
            			<li> 3 Day Paternity Leave</li>
            			<li> 2 Weeks (Minimum) Paid Vacation</li>
            		</ul>
				</div>
				<div class="col-lg-4">
					 <p>
               				<strong>Additional Benefits:</strong>
            		</p>
            		<ul>
            			<li>Company Assisted Medical<br> Deductible Reimbursement</li>
            			<li> Employee Assistance Program</li>
            			<li> Education Reimbursement</li>
            		</ul>
				</div>		
				
			</div>
			<div class="col-lg-12"><br><br><br></div>
            
            
            
            <br>
			<br>
            </p>
        </div>
    </div>
</div>

<!--========================================================        FOOTER
=========================================================-->
<?php include "footer.php" ?>
