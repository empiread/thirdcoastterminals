<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

           <!-- <h1 title="About Third Coast Terminals.">About Third Coast Terminals</h1>-->

            <p>Third Coast Terminals 是您的首选化学品服务公司。</p>


            <p>Third Coast Terminals 是一个为化学化工行业提供服务的公司。公司的服务包括化学品的储存，转运，分装，中转，混合，合成加工等。公司位于德克萨斯州的Pearland，在休斯敦市8号绕城路南段的外面三公里，地理位置得天独厚。我们位于全美最大的石化中心的心脏部位，与墨西哥海湾的主要海洋运输设施比邻而居。 </p>


            <p>Third Coast Terminals 成立于1998年，随即迅速成长为广为认可的最佳供应链合作伙伴。为满足全球石化产品市场客户的需求，我们逐步提供液体储存、代工生产（包括按配方混合和生产化学反应产品）、代为包装、中转服务而声名雀起。我们的客户信赖Third Coast Terminals在市场机会出现时迅速而高效地移动各种资产而盈利。</p>
            <p>Third Coast 为大中小企业在改进原料供应链、管理产品流动、提高存货管理效率、以及提供高效的优化方案上积累了丰富的经验。</p>

        </div>


        <div class="col-lg-6">

            <img src="images/about-third-coast-terminals.jpg" title="About Third Coast Terminals"
                 alt="About Third Coast Terminals" class="img-responsive"><br>
      	  </div>
      	<div class="clearfix"></div>
      	
   <!--   	
        <div class="col-lg-6 contact-form">
            <div class="headline">
                <h2 title="Contact Third Coast Terminals">
                    Send Us a Message
                </h2>
            </div>
            <br>

            <form action="" method="post"
                  class=" contact-style">
                <fieldset class="no-padding">

                    <input type="text" name="full_name" id="name" class="form-control" placeholder="Full Name *">
                    <input type="text" name="phone" id="name" class="form-control" placeholder="Phone *"> <input
                        type="text" name="email" id="email" class="form-control" placeholder="Email *">
                    <input type="text" name="company" id="email" class="form-control" placeholder="Company Name *">
                    <textarea class="form-control" name="message" placeholder="Message..."></textarea>

                    <p>
                        <button type="submit" class="btn  btn-primary btn-blue btn-contact">Send Message</button>
                    </p>
                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form>
        </div>
-->
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
