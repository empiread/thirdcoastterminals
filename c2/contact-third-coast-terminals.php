<?php include "header.php" ?>


<div class="container contact">

    <div class="mtb-25">
        <div class="col-lg-6" style="padding-right: 50px">

            
            <br>

            <p>所有中国地区的生意，请联系我们在香港的代理Billy Lo先生。<br>
电子邮件：Blo-hk@3cchemicals.com<br />
电话：+852 98104527<br />
对涉及化学反应的项目若需技术讨论，也可直接联系Yong Ming 博士。<br />
电子邮件：yming@3cterminals.com<br />
电话：（美国）346-207-4702<br />
<br /><br />
公司地址：<br />
1871 Mykawa Rd<br />
Pearland， TX 77581<br />
USA （美国）
</p>
            <!-- Google Map -->
            <div id="map" class="map map-box map-box-space1 margin-bottom-40">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3470.053432875018!2d-95.29698968489589!3d29.573048582056355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x86409143e11e8a25%3A0x666fc47f0e49b31!2s1871+Mykawa+Rd%2C+Pearland%2C+TX+77581%2C+USA!5e0!3m2!1sen!2sin!4v1450368289440"
                    width="600" height="450" frameborder="0" style="border:0;width: 100%;height: 260px"
                    allowfullscreen></iframe>

            </div><!---/map-->

            <div class="clearfix mt-5"></div>
<!--
            <div class="col-lg-6 pl-0">

                <ul class="list-unstyled">
                    <li><strong title="Third Coast Terminals Contact emails">Emails</strong></li>
                    <li><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
                    <li><a href="mailto:customersvc@3cterminals.com">customersvc@3cterminals.com</a></li>

                </ul>
 

            </div>
            <div class="col-lg-6 ">
                <ul class="list-unstyled">
                    <li><span><strong title="Third Coast Terminals phone numbers">Phone:</strong>  </span>281.412.0275</li>
                    <li><span> <strong title="Third Coast Terminals phone numbers">Toll Free:</strong></span> 877.412.0275</li>
                    <li><span> <strong title="Third Coast Terminals fax">Fax</strong></span> 281.997.5098
                    </li>

                </ul>

            </div>


        </div><!--/col-lg-3-->



<!--
        <div class="col-lg-6 contact-form" style="padding-left: 50px">
            <div class="headline">
                <h2 title="Contact Third Coast Terminals">
                    Send Us a Message
                </h2>
            </div>
            <br>

            <form role="form" id="contactForm" data-toggle="validator" class="shake contact-style">
                <fieldset class="no-padding">
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Full Name *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="phone" id="name" class="form-control" placeholder="Phone *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="company" id="email" class="form-control" placeholder="Company Name *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Message..."></textarea>

                        <div class="help-block with-errors"></div>
                    </div>


                    <button type="submit" id="form-submit" class="btn  btn-primary btn-blue btn-contact ">Submit
                    </button>
                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                    <div class="clearfix"></div>


                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form> -->
        </div>
        <div class="clearfix mt-5"></div>
    </div>
    <div class="clearfix mt-5"></div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



