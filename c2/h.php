<?php include "header.php" ?>


    <div class="container ">
    	<div class="mtb-25">
    		
            	
            
    		<img src="images/third-coast-terminals-2015-full.jpg" class="img-responsive" title="Third Coast Terminals in 2015" alt="Third Coast Terminals in 2015">
    		<div class="clearfix"></div>
    	

        <div class="mtb-25">

            <!--Block stats here -->
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals establishment">
                    Year 1998 
                </h2>
            </div>
            <br>
                <p>Establishment of Third Coast Terminals Pearland Location.</p>

                </div>
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-1998.jpg" class="img-responsive" title="Third Coast Terminals in 1998" alt="Third Coast Terminals in 1998">
            </div>
            <div class="clearfix"></div>
            <!--BLocks End-->

<br>

            <!--Block stats here -->
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2002.jpg" class="img-responsive" title="Third Coast Terminals in 2002" alt="Third Coast Terminals in 2002">
            </div>
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2002"> 
                    Year 2002
                </h2>
            </div>
                <p><br>Purchase of additional Third Coast Terminals Property.</p>

                </div>

            <div class="clearfix"></div>
            <!--BLocks End-->

<br>
            <!--Block stats here -->
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2005">
                    Year 2005
                </h2>
            </div>
                <p><br>Construction of Front Office, additional storage tanks and foundation of new tank farm.</p>

                </div>
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2005.jpg" class="img-responsive" title="Third Coast Terminals in 2005" alt="Third Coast Terminals in 2005">
            </div>
            <div class="clearfix"></div>
            <!--BLocks End-->
<br>

             <!--Block stats here -->
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2008.jpg" class="img-responsive" title="Third Coast Terminals in 2008" alt="Third Coast Terminals in 2008">
            </div>
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2008">
                    Year 2008
                </h2>
            </div>
                <p><br>Double the number of storage tanks and the construction of an on-site Lab.</p>

                </div>

            <div class="clearfix"></div>
            <!--BLocks End-->

<br>
            <!--Block stats here -->
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2008">
                    Year 2008
                </h2>
            </div>
                <p><br>Aerial shot of land that would eventually become property of Third Coast Terminals.</p>

                </div>
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2008-full.jpg" class="img-responsive" title="Third Coast Terminals in 2008" alt="Third Coast Terminals in 2008">
            </div>
            <div class="clearfix"></div>
            <!--BLocks End-->
            
 <br>           
             <!--Block stats here -->
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2010.jpg" class="img-responsive" title="Third Coast Terminals in 2010" alt="Third Coast Terminals in 2010">
            </div>
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2010">
                    Year 2010
                </h2>
            </div>
                <p><br>Double the number of storage tanks and the construction of an on-site Lab.</p>

                </div>

            <div class="clearfix"></div>
            <!--BLocks End-->
<br>            
            <!--Block stats here -->
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2012">
                    Year 2012
                </h2>
            </div>
                <p><br>Purchase of additional land, additional construction of second entrance and warehouses.</p>

                </div>
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2012.jpg" class="img-responsive" title="Third Coast Terminals in 2012" alt="Third Coast Terminals in 2012">
            </div>
            <div class="clearfix"></div>
            <!--BLocks End-->
            
 <br>           
             <!--Block stats here -->
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2013.jpg" class="img-responsive" title="Third Coast Terminals in 2013" alt="Third Coast Terminals in 2013">
            </div>
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2013">
                    Year 2013
                </h2>
            </div>
                <p><br>Additional warehouse and storage tank construction.</p>

                </div>

            <div class="clearfix"></div> 
            <!--BLocks End-->
<br>            
            <!--Block stats here -->
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2014">
                    Year 2014
                </h2>
            </div>
                <p><br>Additional warehouse and rail storage capacity added.</p>

                </div>
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2014.jpg" class="img-responsive" title="Third Coast Terminals in 2014" alt="Third Coast Terminals in 2014">
            </div>
            <div class="clearfix"></div>
            <!--BLocks End-->
            
 <br>           
             <!--Block stats here -->
            <div class="col-lg-6">
               <img src="images/third-coast-terminals-2015.jpg" class="img-responsive" title="Third Coast Terminals in 2015" alt="Third Coast Terminals in 2015">
            </div>
            <div class="col-lg-6">
            	<div class="headline">
                <h2 title="Third Coast Terminals in 2015">
                    Year 2015
                </h2>
            </div>
                <p><br>Completion of original warehouse plan and start of reaction chemistry building.</p>

                </div>

            <div class="clearfix"></div>
            <!--BLocks End--> 

            </div>


        </div>
    </div>


    <!--========================================================
                            FOOTER
    =========================================================-->
<?php include "footer.php" ?>