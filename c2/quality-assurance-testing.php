<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third
                Coast Terminals has an extensive on-site laboratory, which can
                provide our customers with certified quality control and quality
                assurance testing for all manufactured products. Our Chemists,
                Analysts and Technicians support our services with over 100 years of
                Quality Assurance, Quality Control and product management experience.</p>

            <p>The
                Third Coast Analytical Technologies (TCAT) Laboratory is included in
                the site ISO9001:2008 Registration. The Lab participates in
                round-robin-testing with our customers’ labs, outside laboratory
                confirmation, and employs contract calibration services for equipment
                maintenance and reliability. In addition to standard QA/QC testing,
                we offer a flexible work environment that makes for easy scheduling
                of standardized production test requirements.</p>

            <p>For
                a full listing of our laboratory-based services please visit the
                Third Coast Analytical Technologies website below.</p>

            <p>Third
                Coast Analytical Technologies
                <a href="http://www.thirdcoastanalyticaltechnologies.com" target="_blank" title="Third Coast Analytical Technologies">http://www.thirdcoastanalyticaltechnologies.com</a>
            </p>


            <p>Contact TCAT at 281-412-0275</p>
        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/quality-control-testing.jpg" title="Quality Control Testing"
                             alt="Quality Control Testing">

                    </div>


                    <div class="item">
                        <img src="images/quality-assurance-testing.jpg" title="Quality Assurance Testing"
                             alt="Quality Assurance Testing">

                    </div>

                  

                    <div class="item">
                        <img src="images/quality-control-testing-3.jpg" title="Quality Assurance Testing"
                             alt="Quality Assurance Testing">

                    </div>

                    <div class="item">
                        <img src="images/quality-control-testing-4.jpg" title="Quality Control Testing"
                             alt="Quality Assurance Testing">

                    </div>

                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
