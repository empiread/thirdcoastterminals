<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>Responsible
                Distribution is a third-party verification environmental, health,
                safety &amp; security program through which members demonstrate their
                commitment to continuous performance improvement in every phase of
                chemical storage, handling, transportation, and disposal. NACD
                members play a leadership role in their communities as information
                resources and provide the same assistance and guidance to local,
                state, and federal legislators on technical issues relating to the
                safe handling, storage, transportation, use, and disposal of chemical
                products.</p>
            <h4>Key
                Benefits of Responsible Distribution</h4>
            <ul>
                <li><p>
                        Lower
                        occurrences of safety and environmental incidents</p>
                </li>
                <li><p>
                        Enjoy
                        better documentation of company policies</p>
                </li>
                <li><p>
                        Employ
                        practices of excellence and quality systems throughout your
                        operations</p>
                </li>
                <li><p>
                        Engage
                        in better communication with local communities</p>
                </li>
                <li><p>
                        Reduce
                        audit costs and time spent on audits</p>
                </li>
                <li><p>
                        Save
                        on insurance expenses</p>
                </li>
                <li><p>
                        Improve
                        marketplace visibility and earn credibility through performance</p>
                </li>
            </ul>
        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/responsible-distribution.jpg" title="Responsible Distribution: nacd verified" alt="Responsible Distribution: nacd verified">
                    </div>


                </div>
            </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



