<?php include "header.php" ?>


    <div class="container ">

        <div class="mtb-25">
            <div class="col-lg-6">
                <p>The
                    safety of our team members is extremely important to us both at work and
                    at home. Toolbox safety topics are not limited to at-work issues as
                    there are hazards in all aspects of life. Safety training does not
                    stop for us when our team members leave our gates. Without them
                    returning the next day, we would not be able to continue the high
                    quality of service that we have become known for. Safety education
                    and the resulting heightened awareness are principles we train and
                    encourage our team members to follow every day. Team members are not permitted to possess fire-arms, weapons, drugs or
                    alcohol while on duty. We have an open-door, management driven policy
                    where all accidents and situations which could lead to an accident,
                    injury or contamination are reported immediately to local supervisors
                    for correction or escalation.</p>

              

                <p>Third
                    Coast Terminals complies with all industry regulations including, but
                    not limited to:</p>

                <p>Federal
                    Department of Transportation (DOT) Written Hazard Communication
                    Program 49CFR.</p>

                <p>Complete
                    respiratory training program as specified in Occupational Safety &amp;
                    Health Administration (OSHA) 29CFR 1910.134.</p>

                <p>Confined
                    Space – All operations team members are trained and certified annually
                    in confined space entry procedures in accordance with 29CFR 1910.146.</p>

                <p>Lockout
                    / Tagout – Policies and procedures for lock-out/ tag-out have been
                    written and are adhered to in accordance with 29CFR 1910.147.</p>

                <p>Blood-borne
                    Pathogen – Team members are trained annually in the handling and
                    disposal of blood borne Pathogens as outlined in 29CFR 1910.1030.</p>

                <p>Fall
                    Protection training in accordance with 29CFR 1926.503 is accomplished
                    annually.</p>

                <p>Noise
                    Exposure.</p>

                <p>HAZWOPER
                    and HAZCOM training is conducted annually. In addition, prior to the
                    arrival of new products, each affected team member receives training
                    appropriate for that product.</p>

                <p>CPR/First
                    Aid – several team members throughout the facility are trained in
                    first aid and CPR with more trained individuals being added over
                    time.</p>

                <p>Emergency
                    Planning and Fire Prevention (annual training) every team member
                    receives hands-on fire extinguisher training.</p>

                <p>Flood
                    and Hurricane Procedures.
                </p>

                <p>We
                    abide by all DOT Regulations 49CFR Parts 171 – 180. All hazmat
                    team members receive extensive training within the first 90 days of
                    employment. Prior to their training, new team members work side-by-side with
                    experienced hazmat employees.</p>

                <p>Accident
                    and Injury Response.</p>

                <p>Personal
                    Protection Equipment in accordance with 29CFR 1910.132.</p>

                <p>Forklift
                    Safety Program in accordance with 29CFR 1910.178. All operation
                    team members are cross-trained and certified to operate forklifts.</p>

                <p>Third
                    Coast Terminals employs a certified trainer so that all mandatory
                    training may be conducted on-site.</p>

                <p>Third
                    Coast Terminals' continuous team member training program includes
                    mandatory weekly Toolbox Meetings and monthly Departmental Training.</p>

                <p>All
                    team members in the Blending Department have attended the Texas A&amp;M
                    Fire School in Bryan/College Station, TX not to fight fires, but to
                    be more capable in an emergency situation.</p>

                <p>Subcontractors
                    are fully trained before being allowed into the facility while
                    visitors receive a safety indoctrination.</p>

                <p></p>

                <p>Annual
                    insurance audits reflect continuous improvement in our safety &amp;
                    health achievements following Third Coast Terminal’s policy of
                    safety culture.</p></div>


            <div class="col-lg-6">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="images/safety.jpg" title="Safety" alt="Man near safety regulation sign.">
                        </div>
                        
                        <div class="item">
                            <img src="images/safety2.jpg" title="Safety" alt="Safety">
                        </div>
                        
                        
                        <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>


                    </div>
                </div>


            </div>


        </div>
    </div>


    <!--========================================================
                            FOOTER
    =========================================================-->
<?php include "footer.php" ?>