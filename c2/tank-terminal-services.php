<?php include "header.php" ?>




    <div class="mtb-25">
        <div class="col-lg-6">




            <p>Third Coast Terminal 现有170多个储罐。规划是将在Pearland的储存能力提高到超过千万加仑 （37800立方米）。储罐大小从1000加仑 （3.8 立方米）至90000加仑（340立方米）。罐体材料为碳钢（有或没有内衬）和不锈钢。大容量储罐通常都有自动测位计，蒸汽加热蛇管，氮气，和专门的物料管线和物料泵。我们的厂区现有60多英亩（250，000平方米），如果需要我们可以在厂区内增加火车罐的储存，因此在货物流动上无论长期或短期储存给了我们的客户很大的灵活性。</p>



            <p></p>

            <p>装罐服务可以是从任何大的容器比如火车罐，大卡车，软性液体集装袋，国际标准罐转存到储罐。无论进罐或出罐我们都会取样分析确保产品符合客户的要求。</p></div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/tank-terminal-services.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services2.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services3.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">
                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services4.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services5.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>


</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
