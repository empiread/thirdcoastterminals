<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">



            <p>Third
                Coast Terminals offers a variety of technical services including</p>
            <ul>
                <li><p>
                        Multistep
                        organic or inorganic synthesis</p>
                </li>
                <li><p>
                        Formulation
                    </p>
                </li>
                <li><p>
                        Application
                        Support</p>
                </li>
                <li><p>
                        Scale
                        up from laboratory study to industrial production</p>
                </li>
                <li><p>
                        Current
                        process troubleshooting and optimation</p>
                </li>
            </ul>
            <p>R&amp;D
                Equipment and Facilities</p>
            <ul>
                <li><p>
                        High
                        pressure reactors (Parr reactor)</p>
                </li>
                <li><p>
                        Glass
                        reactors – atmospheric and pressurized up to 30 psi</p>
                </li>
                <li><p>
                        On-site
                        analytical instruments: gas chromatography (GC), ion chromatography
                        (IC), FT-IR, UV-Vis., ICP, Auto-titrator, Karl-Fischer moisture
                        machines</p>
                </li>
                <li><p>
                        Local
                        access to HPLC, GC-MS, NMR, and other possible</p>
                </li>
            </ul>
            <p>Chemical
                Families</p>
            <ul>
                <li><p>
                        Ester
                    </p>
                </li>
                <li><p>
                        Amides
                    </p>
                </li>
                <li><p>
                        Amines
                    </p>
                </li>
                <li><p>
                        Amine
                        Oxides</p>
                </li>
                <li><p>
                        Amidazolines
                    </p>
                </li>
                <li><p>
                        Quats
                    </p>
                </li>
                <li><p>
                        Betaines
                    </p>
                </li>
                <li><p>
                        Polymers
                        (pre-polymers)</p>
                </li>
            </ul>
            <p>Processes</p>
            <ul>
                <li><p>
                        Batch
                        reaction</p>
                </li>
                <li><p>
                        Distillation
                        (batch/continuous, flash/fractional)</p>
                </li>
                <li><p>
                        Centrifugation
                    </p>
                </li>
                <li><p>
                        Filtration
                    </p>
                </li>
                <li><p>
                        Phase
                        separation</p>
                </li>
            </ul>
            <p>Market</p>
            <ul>
                <li><p>
                        Extractive:
                        oilfield chemicals, refinery chemicals. AG chemicals, mining
                        chemicals</p>
                </li>
                <li><p>
                        Automotive
                        Chemicals: fuel/lube additives, components, tire/rubber</p>
                </li>
                <li><p>
                        Bio-sustainable:
                        biofuels, chemicals, esters, base oils</p>
                </li>
                <li><p>
                        Others:
                        soecialty chemical, paper and ink, textile</p>
                </li>
            </ul>
            <p>Contact:</p>

            <p></p>

            <p>Please
                call Dr.Yong Ming at 346-207-472 or send an email to
                <b><a
                        href="mailto:yming@3cterminals.com">yming@3cterminals.com</a></b>
            </p>
        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/research-and-development.jpg" title="Research & Development"
                             alt="Research & Development">
                    </div>

                    <div class="item">
                        <img src="images/research-and-development-2.jpg" title="Research & Development"
                             alt="Research & Development">
                    </div>

                    <div class="item">
                        <img src="images/research-and-development-3.jpg" title="Research & Development"
                             alt="Research & Development">
                    </div>


                    <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>


        </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
