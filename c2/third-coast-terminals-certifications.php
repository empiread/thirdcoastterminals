<?php include "header.php" ?>
    <div class="container ">
        <div class="mtb-25">
            <div class="col-lg-12">


<h2>Certifications and Approvals</h2>
                    <p>Meeting
                                the quality needs and expectations of our customers is our highest
                                priority. Third Coast Terminals is completely committed to ensuring
                                customer satisfaction is achieved at all times.
                    </p>

                    <p>This
                                will be accomplished by providing the highest quality service and by
                                continually improving our quality management system, including
                                maintaining industry standard safety &amp; compliance Certifications
                                &amp; Approvals such as:</p>



            <ul>



                    <li>Federal
                                Guarantee under section 303(C)(2) of the Food, Drug, and Cosmetic Act</li>

                    <li>Texas
                                Department of Health, Food, and Drug License
                    </li>

                    <li>FDA
                                Labeler Code Number 67073</li>

                    <li>ISO
                                9001:2008 Certified</li>
                  </ul>




          <ul>

                    <li>Federal
                                Spill Prevention Control and Counter Measures (SPCC) Plan</li>

                    <li>Federal
                                Storm Water Pollution Prevention Plan (SWP)</li>

                    <li>Texas
                                Commission on Environmental Quality Notice of Intent (NOI)
                    </li>

                    <li>Texas
                                Natural Resource Conservation Commission Notice of Registration
                    </li>

                    <li>American Chemistry Council (ACC).</li>

                    <p>
                    </p>

					</ul>

          <p>Kosher
                      (Kashruth) certificate with:</p>

          <ul style="list-style:none; font-size: 10.5pt; color:rgb(50,50,50) ">
                    <li>Orthodox
                                Union</li>

                    <li>KOF-K
                                Kosher Overseers Associates of America</li>

                    <li>Organized
                                Kashruth Laboratories</li>
					</ul>


                    <p>Third
                                Coast is committed to the conservation of our resources &amp;
                                environment observing all rules &amp; regulations.</p></div>
            </div>
        </div>
    </div><!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
