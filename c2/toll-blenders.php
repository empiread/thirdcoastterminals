<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>Third Coast Terminals 可将客户从简单到复杂的各种化学配方通过混合变成客户的最后产品。我们的经验，设备和质控过程确保我们比客户要求的做得更好。</p>

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item">
                        <img src="images/toll-blenders.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/toll-manufacturing.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>

                    <div class="item">
                        <img src="images/toll-blenders-4.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item active">
                        <img src="images/toll-blenders-3.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
