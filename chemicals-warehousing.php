<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third Coast Terminals offers 10 high-dock doors for daily receiving and shipping of packaged goods,
                including two LTL doors from 10 AM to 4 PM. Our warehouse complex manages over 30,000 drums in its
                160,000 square feet of space. One half of our warehouse is equipped to handle flammable and other
                hazardous materials.</p>

            <p>With more than 110 time slots, Third Coast's Internal Sales and Plant Operations team members can
                discharge (or receive) up to 3200 drums daily. Your customer dedicated Inside Sales Representative will
                adopt the skills needed to deliver high quality, reliable logistics services in a manner that fits your
                company and quality needs.</p>

            <p>Third Coast Terminals makes is easy for our customers to operate in a complex work environment. As
                needed, your Inside Sales Representative, and his or her support staff will handle your orders,
                inventory, finished goods, reporting, critical documentation, containerization, and freight procurement,
                as a partner in your supply chain.</p>


        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/chemicals-warehousing.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">
                    </div>

                    <div class="item">
                        <img src="assets/images/third-coast-terminals-logistics.jpg" title="Chemical Distribution"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing2.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing3.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing4.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing5.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>



                    <div class="item">
                        <img src="images/chemicals-warehousing7.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>
                    
                    <div class="item">
                        <img src="images/chemicals-warehousing-8.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>


                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
