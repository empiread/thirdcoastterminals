<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>
                Third Coast 长期包装医药及食品级的产品并积累了丰富的经验，非常熟悉并严格按照“医药及食品管理条例”的要求进行包装。
            </p>

            <!-- Todo link add -->

          

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/cgmp-haccp.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                        <img src="images/cgmp.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                        <img src="images/haccp.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                        <img src="images/cgmp-haccp-2.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                        <img src="images/cgmp-haccp-3.jpg" title="cGMP" alt="HACCP">
                    </div>

				 <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>


        </div>


    </div><!--/col-lg-3-->


</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
