<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">
            <h3 title="Corporate social responsibility">Working at Third Coast Terminals</h3>

            <p>In
                our efforts to promote a strong partnership with our community and
                other stakeholders, Third Coast Terminals has promoted and
                participate in activities such as training of emergency response
                personnel, as well as other local members of our industry. Third
                Coast has representatives in, and patronize organizations such as
                Transcaer®, Brazoria County Emergency Planning Committee, Community
                Emergency Response Team, as well as other local civic and
                governmental institutions. In addition, Third Coast Terminals
                maintains a strong relationship with the Pearland Fire Department.</p>

            <h3 title="Going green">Third
                Coast encourages the following actions:</h3>
            <br>
            <h4 title="Recycle">ENVIRONMENTAL</h4>

            <p>Third
                Coast Terminals encourage it's employees, neighbors, and stakeholders
                in general, to recycle unused paint, brake fluid, antifreeze,
                cleaning products, oils, and other authorized items, by dropping them
                off at the Pearland Recycling Center.</p>

            <p>White
                goods (appliances) and electronics (phones, computers, etc.) also
                need to be collected for recycling.</p>

            <p>Third
                Coast recycles all batteries and provides receptacles throughout the
                facility for their collection.</p>
            <br>
            <h4 title="Going paperless Third Coast Terminals">GOING
                PAPERLESS</h4>

            <p>By
                using the most adequate electronic programs and technologies, we have
                been able to reduce, and continue to reduce, our paper consumption,
                while increasing efficiency and customer service. Discarded paper is
                recycled.</p>
            <br>
            <h4 title="Mosquito extermination Third Coast Termimals">HEALTH</h4>

            <p>Third
                Coast Terminals employees, stakeholders and citizenry in general are
                exhorted to participate in the eradication of mosquitoes in the area.

                Do not allow old tires or water
                collecting materials to rest in your backyard or other places where
                they can provide mosquitoes a place to breed and incubate their
                larvae.</p>
            <br>
            <h4 title="Employee safety and disaster training Third Coast Terminals">TRAINING</h4>

            <p>Third
                Coast Terminals is involved in training and providing information to
                it's employees and members of the community in emergency response and
                hurricane preparedness.</p>

            <p>Third
                Coast Terminals training program in Hazardous Materials
                identification is available upon request to civic groups and other
                parties.</p></div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



