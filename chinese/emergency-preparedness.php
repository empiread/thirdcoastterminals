<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third
                Coast Terminals recognizes that our local area is periodically
                exposed to violent storms and other events. TCT has prepared our
                organization for rapid response, recuperation, community assistance
                and business continuity. Our commitment to emergency preparedness
                includes:</p>

            <p>Third
                Coast Terminals has prepared an Emergency Action Plan Manual.
            </p>

            <p>Established
                communication with local emergency management for assistance and
                cooperation when necessary and possible.</p>

            <p>Planning
                and conducting periodic, documented emergency exercises</p>

            <p>Installed
                generators which are capable of supplying energy to run the entire
                facility including onsite Corporate Offices and Analytical
                Laboratory.</p>

            <p>Fire
                monitors are in place throughout the plant, with an additional
                200,000 gallon water storage tank and pump capable of moving 4000
                gallons per minute as part of our fire suppression system.</p>

            <p>Assigned
                responsibility, and training, for employee deployment in disaster
                conditions.</p>

            <p>Capability
                to provide support resources in the community.</p>

            <p>Spill
                containment, spill control equipment and trained operators on staff
                during operational hours.</p>

            <p>Extensive
                and detailed planning for emergency response.</p>

            <p>Personnel
                trained and certified in Incident Command, Federal Emergency
                Management Agency (FEMA) 100, 200, 700, Hazardous Materials
                Prevention, Guide to Points of Distribution and Chemical Security
                Awareness, among others.</p></div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/emergency-preparedness.jpg" title="Emergency Preparedness"
                             alt="Emergency Preparedness">
                    </div>

                    <div class="item">
                        <img src="images/emergency-preparedness-1.jpg" title="Emergency Preparedness"
                             alt="Emergency Preparedness">
                    </div>

                    <div class="item">
                        <img src="images/emergency-preparedness-2.jpg" title="Emergency Preparedness"
                             alt="Emergency Preparedness">
                    </div>


                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
