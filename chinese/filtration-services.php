<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>Third Coast Terminals 提供各种过滤服务，包括Cuno过滤，碳床过滤，滤孔从5到100微米的袋过滤（常温和高温）以及用“Sparkler”过滤装置来清除液体里的少量残余固体颗粒。各种滤器使用不锈钢外壳。</p>


        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/filtration-services.jpg" title="Filtration Services" alt="Filtration Services">
                    </div>

                    <div class="item">
                        <img src="images/filtration-services2.jpg" title="Filtration Services"
                             alt="Filtration Services">
                    </div>

                    <div class="item">
                        <img src="images/filtration-services3.jpg" title="Filtration Services"
                             alt="Filtration Services">
                    </div>
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



