</section>
	<footer class="footer_block" >
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 footer_container">
			<div class="common_width">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0">
					<div class="row footer_top_container">
						<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-0 third_coast_block">
							<h3>会员网站</h3>
							<div class="row">
								<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 p-0 "> 
									<ul>
										<li><a class="footer-home" href="https://thirdcoast.com/" target="_blank">第三海岸</a></li>
										<li><a class="footer-home" href="https://thirdcoastinternational.com/" target="_blank">第三海岸国际</a></li>
										<li><a class="footer-home" href="https://www.thirdcoastchemicals.com/" target="_blank">第三海岸化学品</a></li>
									</ul>
								</div>
								<div class="col-xl-5 col-lg-5 col-md-6 col-sm-12 col-12 p-0"> 
									<ul>
										<li><a class="footer-home" href="https://www.thirdcoastanalyticaltechnologies.com/" target="_blank">第三海岸分析</a></li>
										<li><a class="footer-home" href="https://www.tciqatar.com/" target="_blank">卡塔尔第三海岸</a></li>
										<li><a class="footer-home" href="https://acmewelding.com/wp/" target="_blank">ACME焊接</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12 p-0">
							<div class="footer_contact_block">
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 p-0"> 
										<h3>地址</h3>
										<div class="fa_map_marker_block"><a href="https://www.google.co.in/maps/place/Third+Coast+Terminals/@29.5744049,-95.2951142,17z/data=!3m1!4b1!4m5!3m4!1s0x8640914403377d4d:0x1305740ce43f8ebc!8m2!3d29.5744003!4d-95.2929255" target="_blank"><span class="fa fa-map-marker fa_icon"></span><span>1871 Mykawa Rd, Pearland, TX 77581</span></a></div>
										<div class="fa_envelope_block"><a href="mailto:sales@3cterminals.com"><span class="fa fa-envelope fa_icon"></span><span>sales@3cterminals.com</span></a></div>
									</div>
									<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 p-0 footer_phone_container"> 
										<h3>电话号码</h3>
										<a href="tel: 877 412 0275"><span class="fa fa-phone fa_icon"></span><span class="header_contact_btn"> 877 412 0275</span></a>
										<a class="fax_tag"><span class="fa_icon"> <img src="../assets/images/fax_icon.svg" class="fax_icon svg" ></span><span> 281 412 0245</span></a>
									</div>
									<div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 pr-0 footer_follow_container"> 
										<h3>跟着我们</h3>
										<a href="https://twitter.com/TCTerminals" class="footer_follow_icon_tag" target="_blank"><img src="../assets/images/twitter_icon.svg"  alt="twitter_icon" class="svg footer_follow_icons"></a>
										<a href="javascript:void(0)" class="footer_follow_icon_tag"><img src="../assets/images/facebook_icon.svg" alt="facebook_icon" class="svg footer_follow_icons"></a>
										<a href="https://accounts.google.com/ServiceLogin?passive=1209600&osid=1&continue=https://plus.google.com/111648529809799884969&followup=https://plus.google.com/111648529809799884969" class="footer_follow_icon_tag" target="_blank"><img src="../assets/images/google_plus_icon.svg"  alt="google_plus_icon" class="svg footer_follow_icons"></a>
										<a href="https://www.youtube.com/user/thirdcoastint" class="footer_follow_icon_tag"  target="_blank" ><img src="../assets/images/youtube_icon.svg" alt="youtube_icon" class="svg footer_follow_icons"></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 footer_bottom_container">
						<div class="row">
							<div class="col-xl-8 col-lg-8 col-md-7 col-sm-12 col-12 p-0">
								<p class="copy_right_txt">版权 &copy; 2019 第三海岸码头.<br> 版权所有</p>
							</div>
							<div class="col-xl-4 col-lg-4 col-md-5 col-sm-12 col-12 p-0 text-right">
								<p class="site_text">Site design by PetroPages<span class="copy_right_block"><a href="https://petropages.com/" target="_blank" class="copy_block"><img src="../assets/images/petro-logo-white.png" alt="Petropages" class="copy_right_img"></a></span></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</main>
</body>
</html>