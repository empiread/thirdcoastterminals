
<?php
$current = basename($_SERVER['PHP_SELF']);
$pages = array(
array(
"title" => '<br />关于我们', //About Us
"url" => 'about-third-coast-terminals.php',
"seo-title" => 'About Third Coast Terminals - Third Coast Terminals',
"keywords" => 'About Third Coast Terminals',
"desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
"h1" => '关于我们',
'right' => '-30',
"sub" => array(
array(
"title" => '关于我们', //About Us
"url" => 'about-third-coast-terminals.php',
"seo-title" => 'Premier Chemical Service Company - Third Coast Terminals',
"keywords" => 'About Third Coast Terminals',
"desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
"h1" => '关于我们'
)
)
) ,



array( 
"title" => '<br />货运终端服务',
"url" => '',
"seo-title" => '',
"keywords" => '',
"desc" => '',
'right' => '-45',
"sub" => array(



array(
"title" => '包装',
"url" => 'industrial-chemicals-packaging.php',
"seo-title" => 'Packaging Chemicals - Third Coast Terminals',
"keywords" => 'Chemicals Packaging',
"desc" => 'Third Coast Terminals has six industrial chemical packaging lines for products to accommodate either drums or IBC’s.',
"h1" => '工业包装',
"banner" => 'packaging-banner.jpg'
) ,
array(
"title" => '医药和食品级物品包装',
"url" => 'cgmp-haccp.php',
"seo-title" => 'HACCP | CGMP - Third Coast Terminals',
"keywords" => 'CGMP,HACCP',
"desc" => 'Our team is intimately familiar with HACCP requirements and we stand ready to partner with you regarding your packaging needs.',
"h1" => '医药和食品级物品包装',
"banner" => 'packaging-banner.jpg'
) ,
array(
"title" => 'Halal/Kosher 包装服务',
"url" => 'food-product-packaging.php',
"seo-title" => 'Kosher Packaging | Halal Packaging - Third Coast Terminals',
"keywords" => 'Food Product Packaging,Kosher Packaging,Halal Packaging',
"desc" => 'Third Coast is equipped to handle a wide range of food-grade products from vegetable oil to pharmaceutical grade propylene glycols.',
"h1" => 'Halal/Kosher 包装服务',
"banner" => 'packaging-banner.jpg'
) ,
array(
"title" => '危险物品的包装',
"url" => 'hazardous-materials-packaging.php',
"seo-title" => ' Packaging Hazardous Materials - Third Coast Terminals',
"keywords" => 'Hazardous Materials Packaging',
"desc" => 'Drum or tote filling for hazardous materials.',
"h1" => '危险物品的包装',
"banner" => 'packaging-banner.jpg'
)
)
) ,
/* end of tab 2 */



array(
"title" => '<br>供应链服务',
"url" => '',
"seo-title" => '',
"keywords" => '',
"desc" => '',
'right' => '-32',

// 'divmenu'=>'1',

"sub" => array(
array(
"title" => '储罐服务',
"url" => 'tank-terminal-services.php',
"seo-title" => 'Tank Terminals - Third Coast Terminals',
"keywords" => 'Tank Terminal Services',
"desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.',
"h1" => '储罐服务',
"banner" => 'supply-banner.jpg'
) ,
array(
"title" => '铁路及运输服务',
"url" => 'railcar-loading-and-unloading.php',
"seo-title" => 'Railcar Loading and Unloading - Third Coast Terminals',
"keywords" => 'Railcar Loading,Railcar Unloading',
"desc" => '',
"h1" => '铁路及运输服务',
"banner" => 'supply-banner.jpg'
) ,

array(
"title" => '仓储和配发中心',
"url" => 'chemicals-warehousing.php',
"seo-title" => 'Handling Chemicals in the Warehouse - Third Coast Terminals',
"keywords" => 'Chemicals Warehousing',
"desc" => 'One half of our warehouse is equipped to handle flammable and other hazardous materials.',
"h1" => '仓储和配发中心',
"banner" => 'supply-banner.jpg'
) ,
array(
"title" => '拖挂车停放服务',
"url" => 'trailer-storage.php',
"seo-title" => 'Container Storage | Trailer Storage | Drop and Swap - Third Coast Terminals',
"keywords" => 'Trailer Storage,Container Storage,Drop and Swap',
"desc" => 'Trailer storage is offered for our blending, reaction chemistry, trans-loading and packaging customers.',
"h1" => '拖挂车停放服务',
"banner" => 'supply-banner.jpg'
)
)
) ,










array(
"title" => '<br />与化学反应相关的服务',
"url" => '',
"seo-title" => '',
"keywords" => '',
"desc" => '',
'right' => '-20',
"sub" => array(
array(
"title" => '反应器列表',
"url" => 'reactor-capability.php',
"seo-title" => 'Toll Manufacturing Chemicals - Third Coast Terminals',
"keywords" => 'Reactor Capabilities,Toll Manufacturing Chemicals',
"desc" => '',
"h1" => '反应器列表'
),


/* TOLL BLENDING */
array(
"title" => '混合类化学品',
"url" => 'toll-blenders.php',
"seo-title" => ' Toll Blenders - Third Coast Terminals',
"keywords" => 'Toll Blenders,Toll Manufacturing',
"desc" => 'It’s a great idea to use our expertise, specialized equipment, and resources to complete your Toll Manufacturing.',
"h1" => '混合类化学品',
),

/* FILTRATION*/
array(
"title" => '过滤服务',
"url" => 'filtration-services.php',
"seo-title" => 'Filtration - Third Coast Terminals',
"keywords" => 'Filtration Services',
"desc" => 'Third Coast Terminals offers a variety of filtration capabilities.',
"h1" => '过滤服务',
),

/* R&D AND QC LAB CAPABILITIES */
array(
"title" => '研发和质控室',
"url" => 'third-coast-research-and-development.php',
"seo-title" => 'Third Coast R & D - Third Coast Terminals',
"keywords" => 'Third Coast Research & Development',
"desc" => 'Third Coast Terminals offers a variety of technical services.',
"h1" => 'Third Coast 研发和质控室',
"banner" => 'lab-banner.jpg'
)
)
) ,


array(
"title" => '<br />来料加工程序',
"url" => 'contract-manufacturing-and-packaging.php',
"seo-title" => 'Contract Manufacturing and Packaging - Third Coast Terminals',
"keywords" => 'Contract Manufacturing and Packaging,Chem',
"desc" => '',
'right' => '-20',
"sub" => array(
array(
"title" => '来料加工程序',
"url" => 'contract-manufacturing-and-packaging.php',
"seo-title" => 'Contract Manufacturing and Packaging - Third Coast Terminals',
"keywords" => 'Contract Manufacturing and Packaging,Chem',
"desc" => '',
"h1" => '来料加工程序'
)
)
) ,




array(
"title" => '<br />联系我们',
"url" => 'contact-third-coast-terminals.php',
"seo-title" => '',
"keywords" => '',
"desc" => '',
'right' => '-25',
"sub" => array(
array(
"title" => '联系我们',
"url" => 'contact-third-coast-terminals.php',
"seo-title" => 'Contact Third Coast - Third Coast Terminals',
"keywords" => 'Contact Third Coast Terminals',
"desc" => 'Contact information for Third Coast Terminals .',
"h1" => '联系 Third Coast Terminals',
"show" => 1,
"banner" => 'hr-banner.jpg'
)
)
)
);

// current page value

$currentPage = false;
$currentTopPage = false;
$currentInnerPage = false;

foreach($pages as $page)
{
if ($page['url'] == $current)
{
$currentPage = $page;
}

if (isset($page['sub']))
{
foreach($page['sub'] as $sub)
{
if ($sub['url'] == $current)
{
$currentPage = $sub;
$currentTopPage = $page;
$currentInnerPage = $sub;
}
}
}
}

?>
<?php

// stub creater

/*$mock = file_get_contents('/home/suresh/softs/hyper/oc/dev/mock.stub');

foreach ($pages as $page) {

$d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $page['url'], $mock);
if (isset($page['sub'])) {
foreach ($page['sub'] as $sub) {
$d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $sub['url'], $mock);
}
}
}

exit;*/
?>


<?php

// stub creater

/*$mock = file_get_contents('/home/suresh/softs/hyper/oc/dev/mock.stub');

foreach ($pages as $page) {

$d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $page['url'], $mock);
if (isset($page['sub'])) {
foreach ($page['sub'] as $sub) {
$d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $sub['url'], $mock);
}
}
}

exit;*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php

if (!$_SESSION['desktopmode'])
{
?>

    <?php
}

?>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <?php

if ($current == 'index.php')
{
?>
        <title> Third Coast Terminals | Contract Manufacturing | Reactive Chemistry - Third Coast Terminals</title>
        <meta name="keywords" content="Third Coast Terminals,Contract Manufacturing,Reactive Chemistry">
        <meta name="description"
              content="Third Coast Terminals was established in 1998 and we have quickly grown to be recognized as one of the best supply chain partners.">
    <?php
}
  else
{
?>
        <title><?php echo $currentPage['seo-title'] ?></title>
        <meta name="keywords" content="<?php echo $currentPage['keywords'] ?>">
        <meta name="description" content="<?php echo $currentPage['desc'] ?>">
    <?php
}

?>
    <meta name="author" content="www.petropages.com/creative/">
    <meta name="geo.region" content="US-TX"/>
    <meta name="geo.placename" content="Pearland"/>
    <meta name="geo.position" content="29.573049;-95.294782"/>
    <meta name="ICBM" content="29.573049, -95.294782"/>

    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">


    <link href="assets/style.css" rel="stylesheet">
    <?php

if ($current !== 'index.php')
{
?>
        <style>


            /* Sticky footer styles
    -------------------------------------------------- */
            html {
                position: relative;
                min-height: 100%;
            }

            body {
                /* Margin bottom by footer height */
                margin-bottom: 170px;
            }

            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 170px;

            }

            /* Custom page CSS
            -------------------------------------------------- */
            /* Not required for template or sticky footer method. */

            body > .container {
                padding: 10px 0px 0;
                min-height: 600px;
            }

            .footer > .bg {
                background: #333132;
            }

        </style>
    <?php
}

?>
<style>
@media (max-width: 992px){
.navbar-theme .navbar-nav > li > a{
    padding: 5px 10px 12px;
}
}
@media (max-width: 1023px){
.col-lg-6.bg-1{
min-height:250px;width:100% !important;
}
.sprit {
width: 100%;  margin-left: 0;  padding-left: 10px;  margin-top: 20px;
}
.learn-more[
bottom: -100px !important;
]
.bg-2 .learn-more {
    bottom: -90px !important;
    right: 40px !important;
}
.pull-right {
float: left !important;
}
.bg-2 {
padding-right: 0;min-height:250px;
}
.bg-1-width {
width: 100%;
}

}
</style>

</head>
<body>
<!--<script type="text/javascript">
        if (screen.width <= 740) {
        window.location = "/mobile";
    }
</script>-->

<!--========================================================
                          HEADER
=========================================================-->
<header class="<?php echo $current == 'index.php' ? '' : 'inner'; ?>">
    <div class="top-bar  hidden-xs">
        <div class="container ">
            <div class="clearfix">
                <div class="leftside col-lg-4">
                    <div>
     <!--`<a href="<?php
echo $_SERVER["DOCUMENT_ROOT"] . '/path/to/about.html';
?>">about</a>`-->                
                    
   


                        <a href="https://www.google.co.in/maps/place/1871+Mykawa+Rd,+Pearland,+TX+77581,+USA/@29.5730486,-95.2969897,17z/data=!3m1!4b1!4m2!3m1!1s0x86409143e11e8a25:0x666fc47f0e49b31"
                           target="_blank"> 1871 Mykawa Rd, Pearland, TX 77581</a>
                    </div>
                </div>

                <div class="rightside col-lg-8 ">
                    <div>

                        <ul class="list-inline list-unstyled pull-right">
                            <li>+852.9810.4527</li>
                            
                            <li><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
                            <li> <a href="/index.php">English</a> |  
   
       <a href="index.php">中文</a></li>
                            
                        </ul>
                    </div>

                </div>
                <div class="clearfix"></div>

            </div>
        </div>

    </div>

    <!--<div class="mobile-top-bar hidden-lg">
        <div class="container mobile-top">
            <div class="col-md-6 right-border col-xs-6 s">

                <span>877.412.0275</span>
            </div>
            <div class="col-md-6 col-xs-6 s">
                <span><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>-->

    <div class="menu">
        <div class="container">
            <?php
include "nav.php";

?>
        </div>
    </div>

</header>


<?php

if ($current == 'index.php')
{
?>
    <section class="full" id="slider">

        <div
            id="header-video"
            class="full-height black-wrapper video2"
            data-vide-bg="mp4: assets/video/3C Front page color-SD, webm: style/video/ocean, ogv: style/video/ocean, poster: style/video/ocean.jpg"
            data-vide-options="position: 0% 10%">
            <!-- replace the above options with your own -->

            <div class="text-center slider-text">
                <!-- multiple h1's are perfectly acceptable on a page in valid HTML5, when wrapped in individual sections, they're 100% semantically correct -->

                <div class="title">化学品代工生产和储存转运服务公司</div>
               
               <!--
                <div class="title">Service Company</div>
                <div class="sub">Providing customized solutions</div>
-->

                <a href="about-third-coast-terminals.php" class="btn btn-white btn-primary btn-white slider-btn">了解更多 </a>


            </div>
            <!--<a href="#content" class="down"> <i class="ion-chevron-down"></i></a>-->
            <!-- seriously -->
        </div>

    </section>
    
<?php
}
  else
{
if ($currentInnerPage)
{
$img = !isset($currentInnerPage['banner']) ? "assets/images/banner2_02.jpg" : "assets/images/" . $currentInnerPage['banner'];
?>
        <section id="inner-slider" style="background: url(<?php echo $img ?>)">


            <div class="container" style="position: relative">
                <div class="inner-header">

                    <nav class="inner-nav hidden-md hidden-xs">
                        <ul class="nav nav-tabs">

                            <?php
foreach($currentTopPage['sub'] as $item)
{
?>
                                <li class="<?php echo $current == $item['url'] ? 'active' : ''; ?>"><a
                                        href="<?php echo $item['url'] ?>"><?php echo $item['title'] ?></a>
                                </li>

                            <?php
}

?>
                        </ul>
                    </nav>
                    <div class="page-title">
                        <h1><?php echo $currentInnerPage['h1'] ?></h1>
                    </div>

                </div>

            </div>

        </section>
    <?php
}

?>
<?php
}

?>
<div id="content" class="clearfix"></div>

<div class="container  shadow"  >
