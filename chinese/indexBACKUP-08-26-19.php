<?php include "header.php"
?>
<!--  
<div class="mtb-25 deliver" >
  <div class="container">


        <div class="col-lg-6 ">
            <img src="images/optimized-solutions.jpg" class="img-responsive" alt="">
        </div>
        <div class="col-lg-6">
            <div class="headline">
            
                <h2 title="An industry leader in liquid storage, toll manufacturingcontract packaging, and trans-load services."><b>We Deliver Optimized Solutions</b></h2>
            </div>

            <p>
                <br>
                Third Coast Terminals was established in 1998 and we have quickly grown to be recognized as one of the
                best supply chain partners. Our reputation as a provider for liquid storage, toll manufacturing (which
                includes blending and reactive processes), contract packaging, and trans-load services, continues to
                grow as we match our business model to the needs of our customers serving the global petrochemical
                market. Our customers rely on Third Coast Terminals to quickly and cost effectively mobilize
                resources that capitalize on market opportunities as they arise.

                <div>
                    <br>
                    <a href="contact-third-coast-terminals.php" class="btn  btn-primary btn-blue" title="Contact Third Coast Terminals"> Contact Us </a>
                </div>

            </p>
        </div>
    </div>
</div>
<div class="mb-15 ">
    <div class="col-lg-6 bg-1" style="width: 49%;">
        <div class="pull-left bg-1-width">

            <h3><b><u title="Improves supply chains.">We Pursue Logistical Solutions</u> </b></h3>

            Third Coast has extensive experience in  improving supply chains, managing product flow, reducing
            inefficiencies in inventory management, and effectively delivering optimized solutions for any size
            organization.

            <div class="learn-more" style="bottom:-175px">
                <a href="tank-terminal-services.php" class="btn  btn-primary btn-blue" title="Tank terminal services"> Learn More </a>
            </div>

        </div>
    </div>

    <div class="col-lg-6 bg-2 sprit">
        <div class="pull-left bg-1-width">
            <h3><b><u title="Supply chain safety.">World Class Safety</u></b></h3>

            <p>
                Third Coast Terminals is committed to the Environmental, Health, Safety and Security of our Team Members, Customers and our Surrounding Community. We have been awarded quality certifications including ISO 9001-2008, Responsible Distribution and a Responsible Care Partner. Additionally, in 2016 we were recognized by Texas Mutual Insurance for our dedication to Employee Health and Safety.
            </p>

            <div class="learn-more" style="right: 0px;bottom:-161px">
                <a href="environmental.php" class="btn  btn-primary btn-blue" title="Environmental"> Learn More </a>
            </div>
        </div>

    </div> 
    <div class="clearfix"></div>
</div>

<div class="home-reaction ">
    <div class="container">
        <div class="col-lg-6" style="padding-right: 30px">
            <div class="headline white">
                <h2 title="Reaction Chemistry on Polyurethane Prepolymers and MDI/TDI Blending."><b>Chemical Reaction Capabilities</b></h2>
            </div>
            <br>
            <p>
                We Offer Reaction Chemistry on Polyurethane Prepolymers and MDI/TDI Blending.
            </p>

            <p>
                In order to respond cost effectively to changing customer demands, our processes and equipment are
                designed to be flexible. In addition, we engineer our facilities to accept modifications and/or capacity
                increases quickly and with minimum disruption.
            </p>
            <br>
            <p>
                Please contact our sales team who work intimately with our manufacturing and
                engineering functions to discuss your reaction chemistry needs.

            </p>
        </div>

        <div class="col-lg-6">
            <div class="headline white">
                <h2 title="Condensations, esterifications, rearrangements, substitutions, oxidations, and hydrogenation."><b>Developmental Chemical Reaction Capabilities</b></h2>
            </div>

            <p style="margin-top: -12px">
                <br>
                <ul style="margin: 0;    padding-left: 16px;line-height: 30px">
                    <li>
                        Condensations – Aldol, Mannich Bases, Amides
                    </li>
                    <li>
                        Esterifications – Alcohol/Acid, Glycol/Polyacids, Methyl Esters
                    </li>
                    <li>
                        Rearrangements – Catalytic, Thermal
                    </li>
                    <li>
                        Substitutions
                    </li>
                    <li>
                        Oxidations
                    </li>
                    <li>
                        Hydrogenation
                    </li>

                </ul>

            </p>

            <div>
                <br>
                <br>
                <a href="contract-manufacturing-and-packaging.php" class="btn btn-white btn-primary btn-white" title="Contract manufacturing and packaging."> Learn More </a>
            </div>

        </div> 
    </div> 

</div>-->

<?php include "footer.php" ?>
