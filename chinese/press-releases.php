<?php include "header.php" ?>


<div class="container ">

         <div class="mtb-25">
        <div class="col-lg-6">
              <p>
                Texas Mutual Insurance Company announced that Third Coast Packaging, Inc. in Galveston County has been awarded the company’s top honor for workplace safety.  
              </p>

            <p>To download the entire press release, <a href="images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
            <a href="images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank"><img class="img-responsive" src="images/TXM-logo-2014-ol.png" title="Texas Mutual: Workers' Compensation Insurance"
                 alt="Texas Mutual: Workers' Compensation Insurance logo"></a>


        </div>
    </div>
    
    <div class="mtb-25">
        <div class="col-lg-6">
            <p>Third Coast Terminals has deployed M-Files in a hybrid cloud environment for disaster recovery
              to ensure the company's content is secured, protected and available in case the company's on-premises
              systems are compromised by a hurricane or another catastrophic event.</p>

              <p>
                “Being located in the hurricane prone Gulf Coast region,
                we required an information management solution that could provide disaster
                recovery capabilities and ensure our vital information assets were protected in the
                event of a natural disaster situation or hardware failure. Implementing M-Files in a
                HYBRID CLOUD environment provides us with a business continuity plan and disaster recovery
                system that will keep us up and running at all times”  
              </p>

            <p>To download the entire press release, <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank" title="Download press releases.">Click Here</a></p>
        </div>

        <div class="col-lg-6">
            <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank"><img class="img-responsive" src="images/press-release-third-coast-hybrid-cloud-disaster-recovery.jpg" title="Online security software press release"
                 alt="Online security software press release"></a>


        </div>
    </div>
    

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
