<?php include "header.php" ?>
<style type="text/css">

.topRowbg {
background-color:#5b9bd5 !important;
color:#FFF;
}
tr:nth-child(even) {
background-color: #d2deef;

}
tr:nth-child(odd) {background-color: #eaeff7;}

td:nth-child(even) { padding-left: 5px;}
td:nth-child(odd) { padding-left: 5px;}
</style>

<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>
            Third Coast Terminals 现有反应器列表
            
            <br />
            <table   style="width: 850px; line-height: 24px; " border="1" >
            <thead>
            	
            	  <tr class="topRowbg" style="text-align: center;">
            	    <td colspan="2"><strong>反应器容量</strong></td>
            	    
            	    <td><strong>材质</strong></td>
            	
            	    <td colspan="2"><strong>最高温度</strong></td>
            	
            	    <td><strong>真空度</strong></td>
            	    <td><strong>最高压力 (psi)</strong></td>
            	    <td><strong>现状</strong></td>
            	  </tr>
            </thead>
  <tbody>
  	<tr>
  	  <td>加仑</td>
  	  <td>立方米</sup></td>
  	  <td>&nbsp;</td>
  	  <td>°F</td>
  	  <td>°C</td>
  	  <td>&nbsp;</td>
  	  <td>&nbsp;</td>
  	  <td>&nbsp;</td>
  	</tr>
  	<tr>
  	  <td>400</td>
  	  <td>1.5</td>
  	  <td>304SS</td>
  	  <td>320</td>
  	  <td>160</td>
  	  <td>全真空</td>
  	  <td>15</td>
  	  <td>运行中</td>
  	</tr>
  	<tr>
  	  <td>1300</td>
  	  <td>4.9</td>
  	  <td>304SS</td>
  	  <td>320</td>
  	  <td>160</td>
  	  <td>全真空</td>
  	  <td>15</td>
  	  <td>运行中</td>
  	</tr>
  	<tr>
  	  <td>2000</td>
  	  <td>7.7</td>
  	  <td>316SS</td>
  	  <td>320</td>
  	  <td>160</td>
  	  <td>全真空</td>
  	  <td>88</td>
  	  <td>管道连接中</td>
  	</tr>
  	<tr>
  	  <td>2600</td>
  	  <td>9.8</td>
  	  <td>304SS</td>
  	  <td>320</td>
  	  <td>160</td>
  	  <td>全真空</td>
  	  <td>15</td>
  	  <td>运行中</td>
  	</tr>
  	<tr>
  	  <td>4200</td>
  	  <td>15.8</td>
  	  <td>304SS</td>
  	  <td>320</td>
  	  <td>160</td>
  	  <td>全真空</td>
  	  <td>64</td>
  	  <td>管道连接中</td>
  	</tr>
  	<tr>
  	  <td>11200</td>
  	  <td>41.6</td>
  	  <td>316L SS</td>
  	  <td>345</td>
  	  <td>174</td>
  	  <td>全真空</td>
  	  <td>35</td>
  	  <td>2016年四季度</td>
  	</tr>
  	<tr>
  	  <td>11200</td>
  	  <td>41.6</td>
  	  <td>304SS</td>
  	  <td>345</td>
  	  <td>174</td>
  	  <td>全真空</td>
  	  <td>35</td>
  	  <td>2016年四季度</td>
  	</tr>
  	<tr>
  	  <td>27000</td>
  	  <td>102.2</td>
  	  <td>304SS</td>
  	  <td>170</td>
  	  <td>77</td>
  	  <td>常压</td>
  	  <td>常压</td>
  	  <td>运行中</td>
  	</tr>
  </tbody>
</table>
            <br /><br />
            
            
            	
            	
            </p>
            
            <p><br />
            代工生产化学品种类<br />
            
<ul>
            	<li>酯类</li>
            	<li>酰胺类</li>
            	<li>胺类</li>
            	<li>氧化胺类</li>
            	<li>咪唑类</li>
            	<li>季胺盐类</li>
            	<li>甜菜碱两亲表面活性剂类</li>
            	<li>聚酰胺的预聚物</li>
            	<li>更多的反应产物请与我们联系</li>
            </ul>
            
            </p>
<br /><br /><br />
        </div>
        <div class="col-lg-6">
         <!--  
         
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides
             
           
                <div class="carousel-inner" role="listbox">
                    <div class="item">
                        <img src="images/toll-blenders.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/toll-manufacturing.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>

                    <div class="item">
                        <img src="images/toll-blenders-4.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item active">
                        <img src="images/toll-blenders-3.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>
                </div>


                <!-- Left and right controls 
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>
-->

        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
