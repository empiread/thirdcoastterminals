<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25 img-responsive-center">
        <div class="col-lg-12">
            <p>Meeting the quality needs and expectations of our customers is our highest priority. Third Coast
                Terminals is completely committed to ensuring customer satisfaction is achieved at all times. We are currently proud members of the organizations below:</p>

            <div class="col-lg-2"><br/> <img src="assets/s/NACD.jpg" alt="National Association of Chemical Distributors logo"></div>
            <div class="col-lg-10">
                <h3 title="National Association of Chemical Producers and National Association of Chemical Distributors.">National Association of Chemical Producers (NACP), National Association of Chemical Distributors (NACD)</h3>
                <p>An international association of chemical distributor companies that purchase and take title of
                    chemical products from manufacturers. Its members follow a strict code of Responsible Distribution&reg;.
                    It provides a guideline for companies to protect the environment, promote health and safety of
                    employees and community members, enhance product stewardship and ensure the security of its
                    facilities and products. Compliance is enforced through third-party verification.<br><a
                        href="http://www.nacd.com" target="_blank">www.nacd.com</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/afpm.png" class="img-responsive" alt="American Fuel & Petrochemical Manufacturers logo"/></div>
            <div class="col-lg-10">
                <h3 title="American Fuel & Petrochemical Manufacturers">AFPM | American Fuel &amp; Petrochemical
                    Manufacturers</h3>

                <p>Members serve you and America by manufacturing vital products for your life every day, while
                    strengthening America's economic and national security.<br><a href="http://www.afpm.org" target="_blank">www.afpm.org</a>
                </p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_32.jpg" alt="Petroleum Packaging Council logo"/></div>
            <div class="col-lg-10">
                <h3 title="Petroleum Packaging Council">PPC | Petroleum Packaging Council</h3>

                <p>Petroleum Packaging Council, an association providing technical leadership and education to the
                    petroleum packaging industry.<br><a href="http://www.ppcouncil.org" target="_blank">www.ppcouncil.org</a>
                </p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_26.jpg" alt="European Petrochemical Association logo"></div>
            <div class="col-lg-10">
                <h3 title="European Petrochemical Association">EPCA | European Petrochemical Association</h3>

                <p>Global Network and Communication Platform for the Chemical Industry and its Logistics Service
                    Providers.<br><a href="http://www.epca.be" target="_blank">www.epca.be</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_27.jpg" alt="Association of Chemical Industry of Texas logo"></div>
            <div class="col-lg-10">
                <h3 title="Association of Chemical Industry of Texas">ACIT | Association of Chemical Industry of Texas</h3>

                <p>ACIT is composed of businesses that share a symbiotic relationship with the Texas chemical
                    industry.<br><a href="http://www.acit.org" target="_blank">www.acit.org</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/ilma.jpg" alt=""/></div>
            <div class="col-lg-10">
                <h3>ILMA | Independent Lubricant Manufacturers Association</h3>

                <p>A Trade organization for manufacturers of industrial lubricants, focused on advocacy, networking and
                    business promotion.<br><a href="https://www.ilma.org/" target="_blank">www.ilma.org</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_28.jpg" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="Independent Liquid Terminals Association">ILTA | Independent Liquid Terminals Association</h3>

                <p>Independent Liquid Terminals Association (ILTA) represents bulk liquid terminals and above ground
                    storage tank operators - bulk liquid storage tanks - for-hire and throughput services, marketing and
                    pipeline terminals.<br><a href="http://www.ilta.org" target="_blank">www.ilta.org</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_30.jpg" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="Houston Chemical Association">Houston Chemical Association</h3>

                <p>The Association shall have for its purposes: Exists to foster and promote the education of its
                    members and the public by gathering and distributing information of general interest within the
                    field of chemical manufacture and distribution.<br><a href="http://www.houstonchemical.org" target="_blank">www.houstonchemical.org</a>
                </p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/third-coast-terminals_31.jpg" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="Pearland Chamber of Commerce">Pearland Chamber of Commerce</h3>

                <p>The Pearland Chamber of Commerce is a not-for-profit organization and is owned and operated by local
                    business leaders who work to promote business growth in the community. The goal of the chamber is to
                    conceive, inspire and influence public policy decisions that will benefit the local community and
                    businesses.<br><a href="http://www.pearlandchamber.com" target="_blank">www.pearlandchamber.com</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/american-chemistry-council.png" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="American Chemistry Council">American Chemistry Council</h3>

                <p>The American Chemistry Council's (ACC's) mission is to deliver business value using exceptional
                    advocacy using best-in-class performance, political engagement, communications and scientific
                    research. We are committed to sustainable development by fostering progress in our economy,
                    environment and society.<br><a href="http://www.americanchemistry.com" target="_blank">www.americanchemistry.com</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/transcaer.png" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="Transcaer&reg;">TRANSCAER&reg;</h3>

                <p>TRANSCAER is a voluntary national outreach effort that: Promotes safe transportation and handling of
                    hazardous materials, educates and assists communities near major transportation routes about
                    hazardous materials and aids community emergency response planning for hazardous material
                    transportation incidents.<br><a href="http://www.transcaer.com" target="_blank">www.transcaer.com</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="col-lg-2"><br/> <img src="assets/s/chemtrec.png" alt=""/></div>
            <div class="col-lg-10">
                <h3 title="Chemtrec&reg;">Chemtrec&reg;</h3>

                <p>The CHEMTREC vision is to continue to be recognized by emergency responders, industry, government,
                    and others as the world's foremost emergency call center for information on hazardous materials and
                    dangerous goods.<br><a href="http://www.chemtrec.com" target="_blank">www.chemtrec.com</a></p>
            </div>
            <div class="clearfix">&nbsp;</div>
            <br><br> 
            <!--Todo logo need to add-->
            <div id="owl-example" class="owl-carousel">
                <div class="item"><img src="assets/s/afpm.png" alt=""/></div>
                <div class="item"><img src="assets/s/american-chemistry-council.png" alt=""/></div>
                <div class="item"><img src="assets/s/chemtrec.png" alt=""/></div>
                <div class="item"><img src="assets/s/ilma.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/NACD.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/responsible-care.png" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_26.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_27.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_28.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_30.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_31.jpg" alt=""/></div>
                <div class="item"><img src="assets/s/third-coast-terminals_32.jpg" alt=""/></div>
            </div>
        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



