<?php include "header.php" ?>


<div class="container" id="shadow">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>拖挂车停放服务提供给我们需要混合，反应，和换装服务的客户。您会发现我们提供的可停放桶装、吨装桶、小桶的拖挂车服务可极大地加速包装和运输服务从而快速地把您的产品送到您的用户手中。</p>

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/trailer-storage.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-1.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-2.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-3.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-4.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>
                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>


            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
