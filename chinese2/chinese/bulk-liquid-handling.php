<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>Third Coast Terminals is a veteran provider of Bulk Liquid Handling
                Services for the companies that make up the worldwide Petrochemical,
                Fine Chemical, Food and Pharmaceutical Industries. These industries
                form the foundation of our global economy. In order to meet the
                needs of these complex and highly regulated industries, we rely on
                highly developed systems that operate on the foremost level of
                quality, safety and efficiency.</p>

            <p>Third
                Coast Terminals services are ISO 9001 quality system proven, with the
                capability to manage strategically important products and provide
                global distribution solutions. Our resources are based in Texas, but
                we have affiliated Sales offices and facilities in Singapore, Hong
                Kong, London and the newest terminal facilities in Doha, Qatar. Since
                all of these resources and affiliates have their accompanying quality
                systems in place, we confidently serve all our customers with the
                highest standards, and aim to exceed your expectations.</p>

            <p>Our
                family of Third Coast Terminals Services include:</p>
            <ul>
                <li><p>Contract
                        Chemical Manufacturing
                    </p>
                </li>
                <li><p>Toll
                        Blending and Reactive Chemistry Processes</p>
                </li>
                <li><p>Filtration</p>
                </li>
                <li><p>Transloading
                        to and from Railcars, ISO tanks, Flexitanks, and Bulk Trucks</p>
                </li>
                <li><p>Blend
                        capabilities from one drum up to 90,000 gallons.
                    </p>
                </li>
                <li><p>High
                        purity product handling and packaging that meets USP, FDA, Kosher,
                        and Halal requirements</p>
                </li>
                <li><p>Industrial
                        Product Packaging
                    </p>
                </li>
                <li><p>Flammable
                        and Combustible Material Handling and Packaging</p>
                </li>
                <li><p>Rail
                        storage to manage your inbound and outbound railcar inventory</p>
                </li>
                <li><p>Packaged
                        Goods Warehousing and Distribution</p>
                </li>
                <li><p>Containerization</p>
                </li>
                <li><p>Accredited
                        Laboratory and Analytical Services</p>
                </li>
                <li><p>Product
                        Development and Modeling</p>
                </li>
                <li><p>Flexible
                        operating hours</p>
                </li>
            </ul>
        </div>

        <div class="col-lg-6">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">


              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  <div class="item active">
                      <img src="images/service-overview-1.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-2.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-3.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-4.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-5.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-6.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-7.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-8.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-9.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-10.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-11.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-12.jpg" title="Services Overview" alt="Services Overview">
                  </div>

                  <div class="item">
                    <img src="images/service-overview-13.jpg" title="Services Overview" alt="Services Overview">
                  </div>





              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>
          </div>

        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
