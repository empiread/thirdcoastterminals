<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third Coast Terminals  有10个高架装卸码头用于装卸货物，包括两个零星货物装卸门从上午十点开到下午四点。我们的仓储车间负责管理在一万五千平方米里的三万多个圆桶。大约一半的仓储适用于易燃和其它危险品的储存，每天我们可以进出3200个圆桶。</p>


        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/chemicals-warehousing.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">
                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing2.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing3.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing4.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>

                    <div class="item">
                        <img src="images/chemicals-warehousing5.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>



                    <div class="item">
                        <img src="images/chemicals-warehousing7.jpg" title="Chemicals Warehousing"
                             alt="Chemicals Warehousing">

                    </div>


                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
