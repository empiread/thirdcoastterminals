<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>
                来料加工致力于使用用户您自有的专业知识和知识产权来为您们的最终用户生产产品。我们将在原料的选择和生产过程的控制上与您同心协力确保每次按时按质完成生产。我们专业化的商业，技术，工艺，生产，设计，维修，和供应链工作人员，将与您的专家手挽手工作，以确保产品安全地，可靠地，及时地，可盈利地，无缝地到达您的最终用户手上，一如产品在您自己厂里生产的一样。
            </p>

            <p align="justify">
                <strong title="Get quote, Third Coast Termainals.">第一步：意向表达</strong>
            </p>

            <p align="justify">
                您成功的故事开始于您向我们表达让我们代工的意向。 <a href="contact-third-coast-terminals.php">请点击这里联系我们。</a>.

            <p align="justify">
                <strong title="Personal meetings with potential customers, Third Coast Terminals.">第二步：信息交换</strong>
            </p>

            <p align="justify">
                经过初始联系，可安排您和我们的团队会面。这次会面，您将与我们的生产、技术和商业部门更深入交流代工项目。我们也会向您介绍我们的成功故事和各种生产能力。
            </p>

            <p align="justify">
                <strong title="Joint Non Disclosure Execution, Third Coast Terminals.">第三步：签相互保密协议</strong>
            </p>

            <p align="justify">
                我们致力于确保用户的知识产权不外泄。因此，在任何知识产权交换前我们将和您签保密协议，确保您和我们的知识产权信息受到保护。
            </p>

            <p align="justify">
                <strong title="Detailed discussions, Third Coast Terminals.">第四步：详细讨论</strong>
            </p>

            <p align="justify">
              一旦签订保密协议，将讨论详细的代工项目。讨论将集中于生产步骤，工程设计和改进，成本和合同条件，物流协议，质量控制，和安全评估。
            </p>

            <p align="justify">
                <strong title="Project Execution, Third Coast Terminals.">第五步：项目执行</strong>
            </p>

            <p align="justify">
                项目的终点是项目的执行。在这最后阶段，你会见证您的原料以最安全，及时和有效的方式转化为合格的产品。
            </p>

        </div>
        <div class="col-lg-6">

            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/chemical-blending.jpg" title="Chemical Blending" alt="Chemical Blending">

                    </div>

                    <div class="item">
                        <img src="images/chemical-blending2.jpg" title="Chemical Blending" alt="Chemical Blending">

                    </div>

                    <div class="item">
                        <img src="images/contract-manufacturing-and-packaging.jpg" title="Contract Manufacturing and
                             Packaging"
                             alt="Contract Manufacturing and Packaging">
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>


            </div>

        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
