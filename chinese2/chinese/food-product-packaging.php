<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>我们有高纯度包装线用于在无尘室装填钢桶，方形吨装桶，1.25 立方米以下的中型容器， 和20升的小桶。我们有德州卫生局，美国农业部，美国药品管理局的批准并有四个犹太教协会的资质鉴定。

            </p>

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/food-product-packaging.jpg" title="Kosher Packaging" alt="Halal Packaging">
                    </div>

                    <div class="item">
                        <img src="images/kosher-packaging.jpg" title="Kosher Packaging" alt="Halal Packaging">
                    </div>

                    <div class="item">
                        <img src="images/packaging-2.jpg" title="Food Packaging" alt="Food Packaging">
                    </div>

                    <div class="item">
                        <img src="images/packaging-3.jpg" title="Food Packaging" alt="Food Packaging">
                    </div>
                     <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>


        </div>


    </div><!--/col-lg-3-->


</div>


<!--========================================================
FOOTER
=========================================================-->
<?php include "footer.php" ?>
