<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <h1 title="Petrochemical &amp; Fine Chemicals Manufacturing and Handling Experts, Third Coast Terminals.">Petrochemical &amp; Fine Chemicals Manufacturing and Handling Experts</h1>

            <p>
                Third Coast Terminals is the service provider of choice for America's Best and Largest Petrochemical
                Companies.
            </p>

            <p>
                Third Coast Terminals is a custom manufacturer and manager of petrochemicals. We can manage a wide range
                of
                chemicals, CGMP and High Purity Products, to key blended products for Oil/Gas sectors, refineries,
                polymer,
                and surfactant industries. Our people and facilities are focused on exceeding industry and customer
                requirements for logistic solutions, specialized storage, toll processing, reactive chemistry,
                multi-component blending, packaging, and lab services. We serve the Gulf Coast Petrochemical Industry
                and
                are strategically located in Pearland, Texas, near the Port of Houston and the heart of the nation's
                petrochemical facilities.
            </p>


        </div><!--/col-lg-3-->
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/gulf-coast-chemical-industry.jpg" title="Reactive Chemistry" alt="Blending">
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



