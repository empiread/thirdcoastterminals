<?php include "header.php"
?>

<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

            <p style="font-size: 18pt; color:#0C5E86" title="Third Coast Terminals employment">
                Our people make the difference.
            </p>

            <p>
                Working together, we create the essential elements for life&mdash;polymers, chemicals and fuels
                that go into products and materials that you use every day.
            </p>

            <p>
                We operate with a lean staff, making the contributions of each individual critical to our
                success. We emphasize efficient, safe and reliable operations. Our shared purpose is to create
                exceptional value for our customers while providing world class service.
            </p>

            <p>
                We seek talented, results-oriented team players who...
            </p>
            <ul>
                <ul>
                    <li>
                        thrive on change
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        communicate effectively
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        challenge each other to improve
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        treat others with respect
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        operate safely and responsibly with a focus on integrity
                    </li>
                </ul>
                </li>
            </ul>

            <p>
                In return, we
                nurture a culture where you can...
            </p>

            <ul>
                <ul>
                    <li>
                        succeed professionally
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        grow personally
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        team with others to develop innovative solutions
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        be rewarded well for your results
                    </li>
                </ul>
                </li>
            </ul>
            <p>
                Our
                opportunities for growth and achievement are strengthened by the innovation and creativity of our
                people. To succeed, we need individuals who believe they can&mdash;and do&mdash;make a
                difference....providing world class service.
            </p>

            <h3 title="Third Coast Terminals careers">Current Job Openings</h3>

            <p style="text-decoration:underline;">
                <i>Growth. &nbsp;Opportunity. Development. &nbsp;That&rsquo;s what you can look forward to at
                Third Coast Terminals.</i>
            </p>

            <p>
                At Third Coast Terminals, we offer big opportunities for big careers in
                a challenging environment where learning never stops. We expect and reward high performance, and we&rsquo;ve
                created a compensation and incentive program that supports this goal. We know amazing results are the
                outcome of amazing work, and we&rsquo;re committed to becoming the top performer in our
                industry.
                <br/>
                <br/>
                With our newly expanded capabilities and increased global reach, our future is bright
                and our potential is unlimited. It&rsquo;s an exciting time to be part of our company and we hope you
                will consider joining our team.
                <br/>
                <br/>
            </p>



            <h3 title="Third Coast Terminals career application">Application Process</h3>

            <p>
                Before applying with us, we encourage you to browse our site to learn more about who we are, what we do,
                our values, culture and the people who make up the Third Coast Terminals team. This will help you assess
                your own suitability for working at Third Coast Terminals.
                <br/>
                <br/>
                <strong title="Download Third Coast Terminals career application">Step 1 &ndash; Complete an
                Application for Employment</strong>
                <br>
                <br/>
                To download our employment application, please click here: <a href="Employment Application.docx" title="Third Coast Terminals Application for Employment">Third Coast Terminals Employment Application</a>. Return it in person or via
                email to <a href="mailto:HR@3cterminals.com">HR@3cterminals.com</a>. &nbsp;You may also include your
                resume for consideration.
            </p>
            <br>

            <p>
                <strong title="Third Coast Terminals careers">Step 2 &ndash; Assessment of Qualifications</strong>
                <br>
                <br/>
                After submitting your application via email, you will get
                an e-mail confirmation that it was received. Your application is then automatically made available to
                the Human Resources department who specialize in matching your skills, strengths, and areas of interest
                to current openings. Following your submission, a member of the Human Resources team will review your
                application and if there is an appropriate fit, they will schedule an interview.
                <br/>
                <br/>
                <strong title="Third Coast Terminals interview process">Step 3 &ndash;
                Interview</strong>
                <br/>
                <br>
                During the interview process, we will assess your fit with our needs. This also provides
                you a better opportunity to assess your fit with our organization and gain a better understanding of the
                opportunity. After the first round of interviews, depending upon the nature of the position, you may be
                asked to come back at a later date for a second round of interviews. The entire interview process can
                span over a period of a few weeks.
                <br/>
                <br/>
                <strong title="Third Coast Terminals job offers">Step 4 &ndash; Selection</strong>
                <br>
                <br/>
                Successful applicants will
                receive a verbal offer followed by a written offer letter. Offers are conditional subject to background
                checks and a drug screen/physical.
                <br/>
                <br/>
                Once the position has been filled, unselected applicants
                who were interviewed will be notified about their application status. If you were not selected for the
                position for which you originally applied your Application for Employment will be kept active for 1
                year. We frequently add new positions and by keeping your application on file, Human Resources will have
                access to your information when seeking qualified candidates.
            </p>

            <h3 title="Third Coast Terminals benefits">Benefits</h3>

            <p>
                Challenging work can be very rewarding. Our competitive benefits program is meant to enhance
                and preserve your work/life balance and help you plan and prepare for tomorrow.
            </p>
            <ul>
                <li>
                    <strong title="Compensation, Third Coast Terminals.">Compensation</strong> - We offer a competitive package to recognize your contributions to our
                    current and future success.
                </li>
                <li>
                    <strong title="Health coverage, Third Coast Terminals">Health coverage</strong> - We offer medical, dental, vision and other benefits to help you
                    improve and maintain your health and the well-being of your family.
                </li>
                <li>
                    <strong title="Vacation time, Third Coast Terminals.">Vacation</strong> - To help you recharge, we offer a minimum of two weeks paid time off.
                </li>
                <li>
                    <strong title="401k retirement plan, Third Coast Terminals.">401k Plan</strong> &ndash; Our plan offers a 4 percent Company matching contribution.
                </li>
                <li>
                    <strong title="Life insurance, short term and long-term disability, employee assistance program and education reimbursement.">Additional Benefits</strong> - Life Insurance, Short-Term and Long-Term Disability, Employee
                    Assistance Program and Education Reimbursement.
                </li>
            </ul>
            <h3 title="Equal opportunity employer, Third Coast Terminals.">Equal Employment Opportunity Policy</h3>

            <p>
                Third Coast is an equal employment opportunity corporation and will adhere to all federal,
                state, and local employment rules, regulations, and laws including the following:
            </p>

            <p>
                Third Coast prohibits job discrimination because of religious opinions or
                affiliations or on the basis of race, color, age, sex, national origin and/or disability.
            </p>
            <br>

            </p>
        </div>
    </div>
</div>

<!--========================================================        FOOTER
=========================================================-->
<?php include "footer.php" ?>
