<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">



            <p>Third Coast Terminals  有六条工业填装线用于桶类和方形吨装容器。四条填装线用来装填低到中等粘度的产品，一条可移动式填装线用于高粘度和高温装填。</p>

            <p></p>

          

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/chemicals-packaging.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                    	<img src="images/chemicals-packaging-2.jpg" title="Chemicals Packaging" alt="Chemicals Packaging">
                    </div>

                    <div class="item">
                    	<img src="images/chemicals-packaging3.jpg" title="Chemicals Packaging" alt="Chemicals Packaging">
                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>


        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
