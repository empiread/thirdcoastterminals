<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third Coast Terminals has six industrial chemical packaging lines for products to accommodate either drums or IBC’s. Four fill lines are designed to handle light to medium viscosity products while one is a flexible fill, high viscosity and high temperature packaging line.</p>

            <p>All of our chemical packaging lines are fill by weight with access to inbound truck, rail car and terminal operations and 100 percent visually inspected packaging containers. We have well over 50,000 gallons of daily packaging capacity and the flexibility to provide chemical packaging for a wide variety of petrochemical products within our operations.</p>



        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                	<div class="item active">
                        <img src="images/chemicals-packaging.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/chemicals-packaging-2.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/chemicals-packaging3.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>


                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div><!--/col-lg-3-->


</div>


<!--========================================================
FOOTER
=========================================================-->
<?php include "footer.php" ?>
