<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

            <h2 title="About ISO 9001">About ISO 9001</h2>

            <p>The
                ISO 9001 Quality Management standard is implemented by over one
                million companies and organizations in over 170 countries. The
                standard provides a common self-improvement template for businesses
                and is continually evolving.</p>

            <h3 title="Customer focused business management.">Quality
                Management Principles
            </h3>

            <p>The
                standard is based on a process approach to business management.
                Using this approach, business processes are managed through
                monitoring of key performance indicators. Emphasis is given to
                customer focus. The quality of the business is measured according to
                how well it performs against the self-established indicators. A
                cycle of continual improvement is established to assure positive
                change. The cycle is monitored using internal and external
                surveillance audits and a minimum of one top management review per
                year.</p>



            <p>Third
                Coast Terminals is ISO 9001:2008 Certified by Perry Johnson
                Registrars.</p>

        </div>


    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



