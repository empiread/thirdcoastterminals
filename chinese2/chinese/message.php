<?php

$errorMSG = false;

// NAME
if (empty($_POST["name"])) {
    $errorMSG = "Name is required ";
} else {
    $name = $_POST["name"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "Message is required ";
} else {
    $message = $_POST["message"];
}

// MESSAGE
if (empty($_POST["company"])) {
    $errorMSG .= "company is required ";
} else {
    $company = $_POST["company"];
}

// MESSAGE
if (empty($_POST["phone"])) {
    $errorMSG .= "phone is required ";
} else {
    $phone = $_POST["phone"];
}


$EmailTo = "sales@3cterminals.com";
$Subject = "Website Contact Form";

// prepare email body text
$Body = "";
$Body .= "Name: ";
$Body .= $name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email;
$Body .= "\n";

$Body .= "phone: ";
$Body .= $phone;
$Body .= "\n";
$Body .= "Message: ";
$Body .= $message;
$Body .= "\n";
$Body .= "company: ";
$Body .= $company;
$Body .= "\n";


// redirect to success page
if ($errorMSG) {
    echo $errorMSG;

} else {
    // send email
    if (mail($EmailTo, $Subject, $Body, "From:" . $email))
        echo "success";
}

?>