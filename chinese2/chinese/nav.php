<!-- Static navbar -->
<nav class="navbar navbar-default navbar-theme">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">
                <img src="assets/images/logo.jpg" width="200" class="img-responsive" alt="">
            </a>
        </div>
        <div id="navbar" class="navbar-right navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php foreach ($pages as $page) {
                    if (isset($page['sub'])) { ?>
                        <li class="dropdown">


                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false"><?= $page['title'] ?></a>


                        <ul class="dropdown-menu" style="right:<?= $page['right'] ?>px">

                        <?php foreach ($page['sub'] as $sub) { ?>

                            <li>
                                <a href="<?= $sub['url'] ?>"><?= $sub['title'] ?></a>
                            </li>
                        <?php }
                    } ?>
                    </ul>

                    </li>
                <?php } ?>

            </ul>

        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>