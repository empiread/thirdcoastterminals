<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
        	<p>Third Coast Terminals 有多年的火车罐装卸经验。最近，我们的火车罐储存位已多达八十多个。我们进行与火车罐有关的操作位已增加到十二个。BNSF是我们的火车主道服务的提供者。</p>

            

            <p>Third Coast Terminals 可提供与铁路运输有关的各种服务，比如把化学品从火车罐转到卡车上、储罐里，或是我们的桶装车间，或者是铁路直接中转。我们的客户最喜爱的一项服务是直接把化学品从储罐转到火车罐里，然后运到他们的客户手中。我们的经验，储存能力，与BNSF铁路直接相连的便捷，十几台经计量认证的地磅，使您货物运输的需要变得快速而高效。</p>

            









        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/railcar-loading.jpg" title="Railcar Loading" alt="Railcar Loading">
                    </div>

					<div class="item">
                        <img src="images/railcar.jpg" title="Railcar" alt="Railcar">
                    </div>

                    <div class="item">
                        <img src="images/railcar-unloading.jpg" title="Railcar Unloading" alt="Railcar Unloading">
                    </div>

                    <div class="item">
                        <img src="images/railcar-pumping.jpg" title="Railcar Pumping" alt="Railcar Pumping">
                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
