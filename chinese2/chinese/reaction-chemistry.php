<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>
                Third Coast Terminals has over 20 years of chemical production experience between the location in Pearland,
                Texas and affiliate companies in Singapore and Qatar.
            </p>

            <p>
                We focus on specialty markets where our supply chain position, combined with our chemical process, development
                and engineering expertise, leads to strong customer alliances.
            </p>

            <p>
              Extending the company’s competence in supply chain efficiency, Third Coast offers reaction chemistry currently
              majoring on polyurethane prepolymers. Leveraging our expertise in polyurethane systems and Isocyanate handling
              in Pearland, Texas and polyol manufacturing in Singapore, customers have access to the following comprehensive
              capabilities:
            </p>
            <ul>
                <li>
                    <p>
                        Market Development scale reactor (up to 1.5 tonne batches)
                    </p>
                </li>
                <li>
                    <p>
                        Two pairs of full scale processing reactors (batch sizes from 2 to 16 tonne)
                    </p>
                </li>
                <li>
                    <p>
                        High viscosity polyol capable (e.g. polyester polyols)
                    </p>
                </li>
                <li>
                    <p>
                        Isocynanate storage and handling
                    </p>
                </li>
                <li>
                    <p>
                        Packing in pails, drums, totes and bulk.
                    </p>
                </li>
                <li>
                    <p>
                        Full laboratory test facilities including all relevant analytics and product testing for both in
                        process and finished product quality assurance.
                    </p>
                </li>
            </ul>
            <p>
                In order to respond cost effectively to changing customer demands, our processes and equipment are
                designed to be flexible. In addition, we engineer our
                facilities to accept modifications and/or capacity increases quickly and with very minimum disruption.
                <br/>
            </p>

            <p>
                <a href="contact-third-coast-terminals.php" title="Contact Third Coast Terminals">Contact Us</a>
            </p>

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/reaction-chemistry-1.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>

                    <div class="item">
                        <img src="images/reaction-chemistry-2.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>

                    <div class="item">
                        <img src="images/reaction-chemistry-3.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>

                    <div class="item">
                        <img src="images/reaction-chemistry-4.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>

                    <div class="item">
                        <img src="images/reaction-chemistry-5.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>

                    <div class="item">
                        <img src="images/rc-tank-farm.jpg" title="Tank Farm" alt="Tank Farm">
                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
