<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
       
 
 <div style='padding-left: 50px; padding-right: 50px;'>           
            
            
            <p><em>“Meeting the quality needs and expectations of our customers is our highest priority. Third
Coast Terminals is completely committed to ensuring their satisfaction is achieved at all times.
This will be obtained by providing the highest quality service and by continually improving our
quality management system.”</em></p>
            
            <p>Third Coast Terminals adheres to the following Quality Objectives to fulfill our quality
policy

<ul>
	<li>Customer satisfaction as measured by improving or matching the previous performance
as measured by our Customer Feedback Report</li>
	<li>Continually improving our quality management system by progressively reducing
the number of nonconformance’s found during internal and external quality audits</li>
	<li>Third Coast Terminals will review and document key performance indicators as
identified during the Management Review, as a means towards continuous improvement.</li>
</ul>
</p>

<p>As Vice President, I personally affirm my commitment to this policy.<br />
Grif Carnes, Vice President</p>
</div>
            
            
             <div class="col-lg-12"> 
<h3>Third Coast Terminals Permits</h3>
          <ul style="font-size: 10.5pt; color: #333333">


            <li>
                Texas Department of Agriculture (TDA) - Weights and Measures</li>

            <li>

                Department of Transportation (DOT) - Hazmat Registration</li>


            <li>
                Food and Drug Administration (FDA) - Drug Facility Registration</li>


            <li>
                Texas Commission on Environmental Quality (TCEQ) - Waste Water
            </li>



            <li>
                Food and Drug Administration (FDA) - Food Facility Registration
                Department of State Health Services - Wholesale Distributor of
                Drugs
            </li>



            <li>
                Texas Department of State Health Services - Wholesale Distributor of
                Drugs</li>


            <li>
                State of Texas (Commissioner of Health) - Hazardous Substances
                Distributor</li>



            <li>
                Environmental Protection Agency (EPA) Pesticide Establishment</li>



            <li>
                State of Texas Dept. of Public Safety permit - Precursor Chemicals
                and Laboratory Apparatus</li>



            <li>
                Department of Treasury Alcohol and Tobacco Operating permit -
                Warehousing and processing of Spirits for Industrial Use</li>

            </ul>

            </div>
            
            
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
