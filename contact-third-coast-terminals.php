<?php include "header.php" ?>


<div class="container contact">

    <div class="mtb-25">
        <div class="col-lg-6" style="padding-right: 50px">

            <div class="headline">
                <h2>
                    Third Coast Terminals
                </h2>
            </div>
            <br>

            <p>
                When you call Third Coast Terminals, you'll find a friendly and helpful person who can answer your
                questions
                quickly or direct you to the best person on our staff to help you further.

            </p>
            <div class="clearfix mt-5"></div>
            <br>
			<div class="col-lg-6 pl-0">

                <ul class="list-unstyled">
                    <li><strong>Email:</strong></li>
                    <li><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
                    

                </ul>
 

            </div>
            <div class="col-lg-6 ">
                <ul class="list-unstyled">
                    <li><span><strong>Phone:</strong>  </span>281.412.0275</li>
                    <li><span> <strong>Toll Free:</strong></span> 877.412.0275</li>
                    <li><span> <strong>Fax:</strong></span> 281.997.5098
                    </li>

                </ul>

            </div>
            <br>
            <!-- Google Map -->
            <div id="map" class="map map-box map-box-space1 margin-bottom-40">
                <iframe
                    src="https://www.google.com/maps/d/embed?mid=1tHQz4Z3oMCsouihXUhx3kaJsFCu7G1W8&hl=en" width="600" height="450" frameborder="0" style="border:0;width: 100%;height: 260px" allowfullscreen></iframe>



            </div><!---/map-->

            <div class="clearfix mt-5"></div>
			
            <div class="col-lg-6 pl-0">

                <ul class="list-unstyled">
                    <li><strong>Main Location:</strong></li>
                    <li>1871 Mykawa Rd. <br> Pearland, TX 77581</li>
                    <li><span><strong>Phone:</strong>  </span>877.412.0275</li>
                    

                </ul> 
 

            </div>
            <div class="col-lg-6 ">
                <ul class="list-unstyled">
                    <li><span><strong>Support Location:</strong></span></li>
                    <li><span> 18410 Dace Rd. <br> Alvin, Texas 77511</span></li>
                    <li><span><strong>Phone:</strong>  </span>281.997.5080</li>
                    </li>

                </ul>

            </div>



            


        </div>

        <!--/col-lg-3-->

        <div class="col-lg-6 contact-form" style="padding-left: 50px">
            <div class="headline">
                <h2>
                    Send Us a Message
                </h2>
            </div>
            <br>

            <form role="form" id="contactForm" data-toggle="validator" class="shake contact-style">
                <fieldset class="no-padding">
                    <div class="form-group">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Full Name *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="phone" id="name" class="form-control" placeholder="Phone *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <input type="text" name="company" id="email" class="form-control" placeholder="Company Name *">

                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group">
                        <textarea class="form-control" name="message" placeholder="Message..."></textarea>

                        <div class="help-block with-errors"></div>
                    </div>


                    <button type="submit" id="form-submit" class="btn  btn-primary btn-blue btn-contact ">Submit
                    </button>
                    <div id="msgSubmit" class="h3 text-center hidden"></div>
                    <div class="clearfix"></div>


                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form>
        </div>
        <div class="clearfix mt-5"></div>
    </div>
    <div class="clearfix mt-5"></div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



