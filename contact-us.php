<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-md-6">

            <div class="headline">
                <h2>
                    Third Coast Terminals
                </h2>
            </div>

            <p>
                When you call Third Coast Terminals, you'll find a friendly and helpful person who can answer your
                questions
                quickly or direct you to the best person on our staff to help you further.

            </p>

            <p><strong>Address </strong></p>

            <p>1871 Mykawa Rd. Pearland, TX 77581</p>
            <!-- Google Map -->
            <div id="map" class="map map-box map-box-space1 margin-bottom-40">
            </div><!---/map-->

            <div class="clearfix"></div>

            <div class="col-md-6">
                <p>Emails </p>

            </div>
            <div class="col-md-6">


            </div>


        </div><!--/col-md-3-->

        <div class="col-md-6 contact-form">
            <div class="headline">
                <h2>
                    Send Us a Message
                </h2>
            </div>
            <form action="#" method="post"
                  class=" contact-style">
                <fieldset class="no-padding">

                    <input type="text" name="name" id="name" class="form-control" placeholder="Full Name">
                    <input type="text" name="name" id="name" class="form-control" placeholder="Phone">


                    <input type="text" name="email" id="email" class="form-control" placeholder="Email">
                    <input type="text" name="email" id="email" class="form-control" placeholder="Company Name">
                    <textarea class="form-control" placeholder="Message"></textarea>

                    <p>
                        <button type="submit" class="btn  btn-primary">Send Message</button>
                    </p>
                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form>
        </div><!--/col-md-9-->
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



