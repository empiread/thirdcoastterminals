<?php include "header.php"
?>

<div class="container ">

    <div class="mtb-25">
    	<div class="col-lg-12">
        	<div class="col-lg-5">
        		<p>Third Coast is an equal employment opportunity corporation and will adhere to all federal, state, and local employment rules, regulations, and laws including the following:
            	</p>

            	<p>Third Coast prohibits job discrimination because of religious opinions or affiliations or on the basis of race, color, age, sex, national origin and/or disability.
            	</p>
			</div>
			<div class="col-lg-7">
				<img src="images/thanksgiving-lunch2.jpg" title="Thanksgiving Lunch" alt="Thanksgiving Lunch">
			</div>
        </div>
    </div>
</div>

<!--========================================================        FOOTER
=========================================================-->
<?php include "footer.php" ?>
