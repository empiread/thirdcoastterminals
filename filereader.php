<?php
$dynamo_game   = fileReader('Dynamo Soccer Game');
$crawfish_boil = fileReader('Crawfish Boil');
$astros_game   = fileReader('Astros Game');
$breakfast     = fileReader('Breakfast at the Gate');
$summer_picnic = fileReader('Summer Picnic');
$christmas_party = fileReader('Christmas Party');
$thanksgiving_lunch = fileReader('Thanksgiving Lunch');
$special_events     = fileReader('Special Events');
$tct_cares = fileReader('Third Coast Cares');

function fileReader($folder_name){

    $www_root = 'http://thirdcoastterminals.com/assets/images/'.$folder_name;
	$curr_dir = str_replace('/mobile', '', getcwd());
    //echo $curr_dir; exit;
    $dir = $curr_dir.'/assets/images/'.$folder_name;
    $file_display = array('jpg', 'jpeg', 'png', 'gif');
    $gallery_images = '';
    if ( file_exists( $dir ) == false ) {
       echo 'Directory \'', $dir, '\' not found!';
    } else {
       $dir_contents = scandir( $dir );
        $i = 1;
		$gallery_images .= '<div class="carousel-inner" role="listbox">';
        foreach ( $dir_contents as $file ) {
           $file_type = strtolower( end( explode('.', $file ) ) );
           if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
			   $active = '';
			   if($i==1){
				   $active = 'active';
			   }
			  $gallery_images .= '<div class="item '.$active.'"><div class="col-lg-12">';
			  $gallery_images .= '<img src="'. $www_root. '/'. $file. '" style="width:100%" title="" alt="">';
			  $gallery_images .= '</div></div>';
			  $i++;
           }
        }
		$gallery_images .= '</div>';
		$gallery_images .= '<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">';
		$gallery_images .= '<span class="ion-chevron-left" aria-hidden="true"></span><span class="sr-only">Previous</span></a>';
		$gallery_images .= '<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">';
		$gallery_images .= '<span class="ion-chevron-right" aria-hidden="true"></span><span class="sr-only">Next</span></a>';
		
		$gallery_images .= '<div class="pagi"><ol class="carousel-indicators" style="position: static">';
		$j = 0;
		 foreach ( $dir_contents as $file ) {
           $file_type = strtolower( end( explode('.', $file ) ) );
           if ( ($file !== '.') && ($file !== '..') && (in_array( $file_type, $file_display)) ) {
			   $active = '';
			   if($j==0){
				   $active = 'active';
			   }
			  $gallery_images .= '<li data-target="#myCarousel" data-slide-to="'.$j.'" class="'.$active.'"></li>';
			  $j++;
           }
        }
		
		
		$gallery_images .= '</ol></div>';
    }

    return $gallery_images;

}




?>