<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>Third Coast Terminals offers a variety of filtration capabilities including: Cuno Filtrations, Carbon Bed
                Filtrations, nominal 5 - 100 micron Bag Filtrations (ambient & high temperature) with all stainless
                exterior
                construction and we also offer “Sparkler” polishing - liquid filtration capabilities.
            </p>

            <h2>Filtration Services</h2>

            <p>Sparkler Filters with a laboratory model filter for pre-screening filtering operations, and a full size
                plant model with a scale-up ratio of approximately 1 to 120 by filtration surface area. This horizontal
                plate filter is designed for polishing and securing filtration of liquids with limited solids content.
                Both
                plant and lab model filters are capable of developing differential pressures to 50 PSID with plenty of
                safety margin below designed pressure ratings.</p>


        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/filtration-services.jpg" title="Filtration Services" alt="Filtration Services">
                    </div>

                    <div class="item">
                        <img src="images/filtration-services2.jpg" title="Filtration Services"
                             alt="Filtration Services">
                    </div>

                    <div class="item">
                        <img src="images/filtration-services3.jpg" title="Filtration Services"
                             alt="Filtration Services">
                    </div>
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



