<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third
                Coast is equipped to handle a wide range of food-grade products from
                vegetable oil to pharmaceutical grade propylene glycols (Better known
                as PG USP).</p>

            <p>We
                offer high purity packaging lines from drums, totes, IBC’s (up to
                330 gallons), and 5 gallon pails in white rooms.</p>

            <p>Our
                facility and staff are prepared and experienced in following all
                required procedures to meet Halal and Kosher packaging standards.</p>

            <p>We
                are approved by the Texas Department of Health, USDA, FDA, and
                certified by four rabbinical societies for Kosher packaging.</p>

            <p></p>

            <p>All
                of our white-room packaging lines have access to truck, railcar, ISO
                container, and terminal operations with combined filling capacities
                in excess of 50,000 gallons/8 hour shift and utilizing dedicated
                transfer pumps, process piping lines and transfer hoses all based
                upon customer request. All filling is conducted by weight and all
                packaging containers are visually inspected inside and out prior to
                filling; packaging containers can be nitrogen purged and blanketed
                upon customer request.

            </p>

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/food-product-packaging.jpg" title="Kosher Packaging" alt="Halal Packaging">
                    </div>

                    <div class="item">
                        <img src="images/kosher-packaging.jpg" title="Kosher Packaging" alt="Halal Packaging">
                    </div>

                    <div class="item">
                        <img src="images/packaging-2.jpg" title="Food Packaging" alt="Food Packaging">
                    </div>

                    <div class="item">
                        <img src="images/packaging-3.jpg" title="Food Packaging" alt="Food Packaging">
                    </div>
                     <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>


        </div>


    </div><!--/col-lg-3-->


</div>


<!--========================================================
FOOTER
=========================================================-->
<?php include "footer.php" ?>
