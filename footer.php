</div>
<footer class="footer">
    <div class="bg">
        <div class="container">
            <div class="col-lg-9 pl-0">
                <div class="col-lg-4 pl-0">
                    <div class="title"><h4 title="Third Coast Terminals address">Address</h4></div>
                    <div class="desc">
                        <p>
                            <a href="https://www.google.co.in/maps/place/1871+Mykawa+Rd,+Pearland,+TX+77581,+USA/@29.5730486,-95.2969897,17z/data=!3m1!4b1!4m2!3m1!1s0x86409143e11e8a25:0x666fc47f0e49b31"
                               target="_blank" title="Third Coast Terminals address"> 1871 Mykawa Rd, Pearland, TX 77581</a></p>
                               <p><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></p>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="title"><h4 title="Third Coast Terminals phone number">Phone Number</h4></div>
                    <div class="desc">
                        <p title="Third Coast Terminals phone number">TF: 877.412.0275 </p>
                        <p title="Third Coast Terminals phone number">P:&nbsp;&nbsp;  281.997.5098 </p> 
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="title"><h4 title="Affiliate sites, Third Coast Terminals">Affiliate Sites</h4></div>
                    <div class="desc">
                    	<p><a href="http://thirdcoast.com/" target="_blank">Third Coast</a></p>
                      <p><a href="http://thirdcoastinternational.com/" target="_blank">Third Coast International</a></p>
                       <p><a href="http://www.thirdcoastchemicals.com/" target="_blank">Third Coast Chemicals</a></p> 
                       <p><a href="http://www.thirdcoastanalyticaltechnologies.com" target="_blank">Third Coast Analytical</a></p>
                       
                      
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="title"><h4 title="Affiliate sites, Third Coast Terminals">Affiliate Sites</h4></div>
                    <div class="desc">
                    	<p><a href="http://www.tciqatar.com/" target="_blank">Third Coast Qatar</a></p>
                    	 <p><a href="https://acmewelding.com/wp/">ACME Welding</a></p>
                    </div>
                </div>
                

                <div class="col-lg-3 mobile-social hidden-lg">
                    <div class="clearfix"></div>
                    <div class="title"><h4 title="Follow Third Coast Terminals">Follow us</h4></div>
                    <div class="desc">
                        <a href="about-third-coast-terminals.php"><i class="ion-social-twitter"></i></a>
                        <a href="about-third-coast-terminals.php"><i class="ion-social-googleplus"></i></a>
                        <a href="about-third-coast-terminals.php"><i class="fa fa-youtube"></i></a>

                    </div>
                </div>
            </div>
            <div class="col-lg-2 pull-right hidden-xs">
                <div class="social-last"><h4>Follow us</h4></div>
                <div>
                    <ul class="list-inline list-unstyled social pull-right">
						<li><a href="https://www.facebook.com/ThirdCoastTerminals/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/TCTerminals" target="_blank"><i class="ion-social-twitter"></i></a></li>
                        <li><a href="https://www.youtube.com/user/thirdcoastint" target="_blank"><i class="fa fa-youtube"></i></a></li>
                        

                    </ul>
                </div>
            </div>

            <div class="clearfix"></div>
            <hr>

            <div class="col-lg-6 pl-0" id="desktop">
                <div class="copy-right">
                    Copyright © <script>document.write(new Date().getFullYear())</script> Third Coast Terminals. All Rights Reserved.
                </div>

            </div>
            <div class="col-lg-6 hidden-md">

                <div class="by pull-right">
                    <span class="switch switch-desktop"></span>
                    <a target="_blank" href="http://petropages.com/creative">
                        Site design by
                        PetroPages</a>


                </div>
            </div>


        </div>
    </div>
</footer>
<?php $filename = basename($_SERVER["SCRIPT_FILENAME"], '.php');
if( $filename != 'photo-gallery'){ ?>
	<script>
var $dynamo_game   = "";
var $crawfish_boil = "";
var $astros_game   = "";
var $breakfast     = "";
var $summer_picnic = "";
var $christmas_party = "";
var $thanksgiving_lunch = "";
var $special_events     = "";
</script>
<?php  } ?>
<script src="assets/jquery/dist/jquery.js"></script>
<script src="assets/mobile_redirection.js"></script>
<script src="assets/script.js"></script>
<script src="assets/jquery.validate.min.js"></script>
<script src="assets/custom.js"></script>
<script>
$(document).ready(function(){
$('#myCarousel').carousel({
  interval: 2000
});
});
</script>
<!--<script>
$(document).ready(function(){
 $('html, body').animate({
        scrollTop: $("#photo").offset().top - 20
    }, 2000);
});
</script>-->

<?php include "analyticstracking.php" ?>
</body>
</html>