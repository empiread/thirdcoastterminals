<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>
                In
                addition to traditional fill lines, Third Coast has added a Robotic Filling line.  <br>
                This is a “State-of-the-art” fully
                automated filling station. This filler is enclosed <br>in a stainless
                steel housing with automatic sliding doors (right and left), for in<br>
                and outbound pallet movements. Attached to this new filling station,
                is a thermal oxidizer and scrubbing system.</p>

            <h3>Technology Offers:</h3>
            <ul>
                <li>Drum
                    and tote filling for flammable and vapor controlled materials
                </li>

                <li>Consistently
                    accurate fill weights
                </li>

                <li>No
                    human exposure / intervention
                </li>


                
            </ul>
        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
             <div class="item active">
                        <img src="images/hazardous-materials-packaging.png" title="Hazardous Materials Packaging"
                             alt="Hazardous Materials Packaging">
    </div> 
                    
                    <div class="item">
                        <img src="images/automated-filling-station.jpg" title="Hazardous Materials Packaging"
                             alt="Hazardous Materials Packaging">
                    </div>
                    
                    <div class="item">
                        <img src="images/vapor-controlled-materials.jpg" title="Hazardous Materials Packaging"
                             alt="Hazardous Materials Packaging">
                    </div>
                    
                    <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
                </div>
            </div>


        </div>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



