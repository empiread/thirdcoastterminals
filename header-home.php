<!doctype html>
<html lang="en">
	<head>
		<title>Third Coast Terminals</title> 
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=Edge"/> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/bootstrap-ie9.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/main.css">
		<link rel="shortcut icon" href="assets/images/favicon.png" type="image/x-icon">

		<!--[if IE 8]>
			<script src="assets/js/IE8.js" type="text/javascript"></script>
		<![endif]-->
		<!--[if gte IE 8]>
		<link rel="stylesheet" href="assets/css/bootstrap-ie9.css">
		<![endif]-->

		<!--[if lt IE 9]>
			<script src="assets/js/respond.min.js"></script>
			<script src="assets/js/html5shiv.js"></script>
		<![endif]-->
		<script type="text/javascript" src="assets/js/html5.js"></script>
		<script type="text/javascript" src="assets/js/jquery-3.3.1.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/respond.min.js"></script>
		<script type="text/javascript" src="assets/js/flowtype.js"></script>
		<script type="text/javascript" src="assets/js/popper.min.js"></script>
		<script type="text/javascript" src="assets/js/main.js"></script>
	</head>

	<body>
	
	<main>
	<header class="header_block" >
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0 header_container">
			<div class="header_inner_block">
				<div class="blue_header">
					<div class="header_address_block">
						<div class="common_width">
							<div class="row">
								<div class="col-xl-6 col-lg-5 col-md-12 col-sm-12 col-12 p-0 location_top_contanier">
									<div class="location_block">
										<p><a href="https://www.google.co.in/maps/place/Third+Coast+Terminals/@29.5744049,-95.2951142,17z/data=!3m1!4b1!4m5!3m4!1s0x8640914403377d4d:0x1305740ce43f8ebc!8m2!3d29.5744003!4d-95.2929255" target="_blank"><span class="fa fa-map-marker fa_icon"></span><span>1871 Mykawa Rd, Pearland, TX 77581</span></a></p>
									</div>
								</div>
								<div class="col-xl-6 col-lg-7 col-md-12 col-sm-12 col-12 p-0 header_contact_inner_container">
									<div class="header_contact_block">
										<div class="row">
											<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 p-0 header_contact_inner_block_1">
												<a href="tel: 877 412 0275"><span class="fa fa-phone fa_icon"></span><span class="header_contact_btn"> 877 412 0275</span></a>
											</div>
											<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 p-0 header_contact_inner_block_1">
												<a class="fax_tag"><span class="fa_icon"> <img src="assets/images/fax_icon.svg" class="fax_icon svg" ></span><span class="header_contact_btn"> 281 412 0245</span></a>
											</div>
											<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 p-0 header_contact_inner_block_2">
												<a href="mailto:sales@3cterminals.com"><span class="fa fa-envelope fa_icon"></span><span> sales@3cterminals.com</span></a>
											</div>
											<div class="col-xl-2 col-lg-2 col-md-12 col-sm-12 col-12 p-0 header_contact_inner_block_3 text-right">
												<a href="index.php" class="english_content ">English</a> <a class="line">|</a> <a href="chinese/index.php" class="english_content ">CN</a>
											</div>
										</div>
									</div>
									<div class="mobile_contact_block">
										   <div class="contact_icons">
												<a href="tel: 877 412 0275"><span class="fa fa-phone fa_icon"></span></a>
											</div>
											
											<div class="contact_icons">
												<a href="mailto:sales@3cterminals.com"><span class="fa fa-envelope fa_icon"></span></a>
											</div>
											<div class="contact_icons">
												<a href="javascript:void(0)" class="english_content ">English | CN</a>
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_menu_block">
					<div class="common_width">
						<div class="dektop_menu">
						<div class="row">
							<div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 col-12 p-0">
								<div class="logo_block">
									<a href="index.php"><img src="assets/images/third_coast_logo.png" alt="third_coast_logo"></a>
								</div>
							</div>
							<div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12 p-0">
								<div class="menu_contaniner">
									<ul class="nav">
										<li class="nav-item">
											<a href="#" >Contract <br> Manufacturing</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Contract_Manufacturing_icon_dropdown.svg" class="svg menu_icons"></li>
												<li class="nav-item"><a href="contract-manufacturing-and-packaging.php" >Contract Manufacturing</a></li>
												<li class="nav-item"><a href="toll-blenders.php">Toll Blending</a></li>
												<li class="nav-item"><a href="reaction-chemistry.php">Reaction Chemistry</a></li>
												<li class="nav-item"><a href="filtration-services.php">Filtration</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >Packaging <br> Services</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Industrial_Packaging_icon_dropdown.svg"  alt="Industrial_Packaging_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="industrial-chemicals-packaging.php" >Industrial Packaging</a></li>
												<li class="nav-item"><a href="cgmp-haccp.php" >cGMP/HAACP</a></li>
												<li class="nav-item"><a href="food-product-packaging.php" >Halal/Kosher Packaging</a></li>
												<li class="nav-item"><a href="hazardous-materials-packaging.php"  >Hazardous Packaging</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="logistics.php" >Logistics </a>
											<ul class="submenu">
						<li class="nav-item"><img src="assets/images/Terminal_icon_dropdown.svg"  alt="Terminal_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="logistics.php" >Logistics</a></li>
												<li class="nav-item"><a href="tank-terminal-services.php" >Terminal</a></li>
												<li class="nav-item"><a href="railcar-loading-and-unloading.php" >Rail Services</a></li>
												<li class="nav-item"><a href="chemicals-warehousing.php" >Warehousing &amp; Distribution</a></li>
												<li class="nav-item"><a href="trailer-storage.php" >Trailer Storage</a></li>

											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >Lab Research <br> & Development</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Services_icon_dropdown.svg" alt="Services_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="quality-assurance-testing.php" >Services</a></li>
												<li class="nav-item"><a href="third-coast-research-and-development.php" >R&D</a></li>
											
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >Quality </a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Policies_and_Permits_icons_dropdown.svg" alt="Policies_and_Permits_icons_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="third-coast-terminals-permits.php" >Policies and Permits</a></li>
												<li class="nav-item"><a href="iso-9001-quality-system.php" >ISO Quality Management</a></li>
												<li class="nav-item"><a href="third-coast-terminals-certifications.php" >Certifications</a></li>
												<li class="nav-item"><a href="responsible-care-guiding-principles.php" >Responsible Care</a></li>
												<li class="nav-item"><a href="responsible-distribution.php" >Responsible Distribution</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >EHS&S</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Environmental_icon_dropdown.svg" alt="Environmental_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="environmental.php" >Environmental</a></li>
												<li class="nav-item"><a href="health.php" >Health</a></li>
												<li class="nav-item"><a href="safety.php" >Safety</a></li>
												<li class="nav-item"><a href="security.php" >Security</a></li>
												<li class="nav-item"><a href="emergency-preparedness.php" >Emergency Preparedness</a></li>
												<li class="nav-item"><a href="visitor-orientation.php" >Visitor Orientation</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >Human Resources</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Human_Resources_icon_dropdown.svg" alt="Human_Resources_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="human-resources.php">Human Resources </a></li>
												<li class="nav-item"><a href="apply.php" >Apply Now</a></li>
												<li class="nav-item"><a href="tct-cares.php" >TCT Cares</a></li>
												<li class="nav-item"><a href="benefits.php" >Benefits</a></li>
												<li class="nav-item"><a href="equal-opportunity.php" >Equal Opportunities</a></li>
												<li class="nav-item"><a href="photo-gallery.php">Photo Gallery</a></li>
												<li class="nav-item"><a href="contact-third-coast-human-resources.php" >Contact Us</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" >About Us</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/About_Us_icon_dropdown.svg" alt="About_Us_icon_dropdown" class="svg about_menu_icons"></li>
													
												<li class="nav-item"><a href="about-third-coast-terminals.php" >About Us</a></li>
												<li class="nav-item"><a href="history.php" >History</a></li>
												<li class="nav-item"><a href="bulk-liquid-handling.php" >Service Overview</a></li>
												<li class="nav-item"><a href="press-releases.php" >Press Releases</a></li>
												<li class="nav-item"><a href="https://www.thirdcoast.com/">Global Affiliates</a></li>
											</ul>
										</li>
										<li class="nav-item">
											<a href="#" > Contact Us</a>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Contact_Info_icon_dropdown.svg" alt="Contact_Info_icon_dropdown" class="svg about_menu_icons"></li>
												<li class="nav-item"><a href="contact-third-coast-terminals.php" >Contact Info</a></li>
												<li class="nav-item"><a href="inside-sales.php">Inside Sales</a></li>
												<li class="nav-item"><a href="third-coast-terminals-affiliations.php" >Affiliations</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						</div>
					</div>
				
					<div class="mobile_menu">
						<div class="common_width">
							<div class="row">
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 p-0">
									<div class="logo_block">
										<a href="index.php" class="ss_logo_div"><img src="assets/images/third_coast_logo.png"  alt="third_coast_logo" /></a>
									</div>
								</div>
								<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 menu_icon_block p-0">
									<div class="parent_div">
										<div class="child_div">
											<div class="menu_icon_inner_block">
												<a href="javascript:void(0)" class="menu_open"><img src="assets/images/menu_icon.svg" class="menu_icon svg"/></a>
												<a href="javascript:void(0)" class="menu_close"><img src="assets/images/close_icon.svg" class="menu_icon  svg"/></a>
											</div>
										</div>
									</div>
								</div>
								<div class="menu_section">
									<div class="menu_inner_section">
									<ul class="mobile_menu_nav nav">
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#"  class="sub-item">Contract Manufacturing</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Contract_Manufacturing_icon_dropdown.svg" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/contract-manufacturing-and-packaging.php" >Contract Manufacturing</a></li>
												<li class="nav-item"><a href="mobile/toll-blenders.php" >Toll Blending</a></li>
												<li class="nav-item"><a href="mobile/reaction-chemistry.php" >Reaction Chemistry</a></li>
												<li class="nav-item"><a href="mobile/filtration-services.php" >Filtration</a></li>
											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#" class="sub-item" >Packaging Services</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Industrial_Packaging_icon_dropdown.svg"  alt="Industrial_Packaging_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/industrial-chemicals-packaging.php" >Industrial Packaging</a></li>
												<li class="nav-item"><a href="mobile/cgmp-haccp.php" >cGMP/HAACP</a></li>
												<li class="nav-item"><a href="mobile/food-product-packaging.php" >Halal/Kosher Packaging</a></li>
												<li class="nav-item"><a href="mobile/hazardous-materials-packaging.php" >Hazardous Packaging</a></li>
				 							</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#" class="sub-item">Logistics </a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Terminal_icon_dropdown.svg"  alt="Terminal_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/tank-terminal-services.php" >Terminal</a></li>
												<li class="nav-item"><a href="mobile/railcar-loading-and-unloading.php" >Rail Services</a></li>
												<li class="nav-item"><a href="mobile/trans-loading-services.php" >Trans-loading</a></li>
												<li class="nav-item"><a href="mobile/chemicals-warehousing.php" >Warehousing & Distribution</a></li>
												<li class="nav-item"><a href="mobile/trailer-storage.php" >Trailer Storage</a></li>

											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#"  class="sub-item">Lab Research & Development</span></a><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Services_icon_dropdown.svg" alt="Services_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/quality-assurance-testing.php" >Services</a></li>
												<li class="nav-item"><a href="mobile/third-coast-research-and-development.php" >R&D</a></li>
											
											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#" class="sub-item" >Quality</a> </span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Policies_and_Permits_icons_dropdown.svg" alt="Policies_and_Permits_icons_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/third-coast-terminals-permits.php" >Policies and Permits</a></li>
												<li class="nav-item"><a href="mobile/iso-9001-quality-system.php" >ISO Quality Management</a></li>
												<li class="nav-item"><a href="mobile/third-coast-terminals-certifications.php" >Certifications</a></li>
												<li class="nav-item"><a href="mobile/responsible-care-guiding-principles.php" >Responsible Care</a></li>
												<li class="nav-item"><a href="mobile/responsible-distribution.php" >Responsible Distribution</a></li>
											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#" class="sub-item">EHS&S</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Environmental_icon_dropdown.svg" alt="Environmental_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/environmental.php" >Environmental</a></li>
												<li class="nav-item"><a href="mobile/health.php" >Health</a></li>
												<li class="nav-item"><a href="mobile/safety.php" >Safety</a></li>
												<li class="nav-item"><a href="mobile/security.php" >Security</a></li>
												<li class="nav-item"><a href="mobile/emergency-preparedness.php" >Emergency Preparedness</a></li>
												<li class="nav-item"><a href="mobile/visitor-orientation.php" >Visitor Orientation</a></li>
											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#"  class="sub-item">Human Resources</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/Human_Resources_icon_dropdown.svg" alt="Human_Resources_icon_dropdown" class="svg menu_icons"></li>
												<li class="nav-item"><a href="mobile/human-resources.php" >Human Resources </a></li>
												<li class="nav-item"><a href="mobile/apply.php" >Apply Now</a></li>
												<li class="nav-item"><a href="mobile/tct-cares.php" >TCT Cares</a></li>
												<li class="nav-item"><a href="mobile/benefits.php" >Benefits</a></li>
												<li class="nav-item"><a href="mobile/equal-opportunity.php" >Equal Opportunities</a></li>
												
												<li class="nav-item"><a href="mobile/photo-gallery.php" >Photo Gallery</a></li>
												<li class="nav-item"><a href="mobile/contact-third-coast-human-resources.php">Contact Us</a></li>
											</ul>
										</li>
										<li class="nav-item">
											
											<span class="mobile_link"><a href="#" class="sub-item">About Us</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item"><img src="assets/images/About_Us_icon_dropdown.svg" alt="About_Us_icon_dropdown" class="svg about_menu_icons"></li>
												<li class="nav-item"><a href="mobile/about-third-coast-terminals.php" >About Us</a></li>
												<li class="nav-item"><a href="mobile/history.php" >History</a></li>
												<li class="nav-item"><a href="mobile/bulk-liquid-handling.php" >Service Overview</a></li>
												<li class="nav-item"><a href="mobile/press-releases.php" >Press Releases</a></li>
												<li class="nav-item"><a href="https://www.thirdcoast.com/">Global Affiliates</a></li>
											</ul>
										</li>
										<li class="nav-item contact_last_block">
											
											<span class="mobile_link"><a href="#" class="sub-item">Contact Us</a></span><span class="mobile_arrow"><img src="assets/images/textbox_downarrow.svg" class="down_arrow header_arrow svg"/><img src="assets/images/arrow-up.svg" class="up_arrow header_arrow svg"/></span>
											<ul class="submenu">
												<li class="nav-item">
													<img src="assets/images/Contact_Info_icon_dropdown.svg" alt="Contact_Info_icon_dropdown" class="svg about_menu_icons">
												</li>
												<li class="nav-item">
													<a href="mobile/contact-third-coast-terminals.php" >Contact Info</a>
												</li>
												<li class="nav-item">
													<a href="mobile/inside-sales.php" >Inside Sales</a>
												</li>
												<li class="nav-item">
													<a href="mobile/community.php" >Community</a>
												</li>
												<li class="nav-item">
													<a href="mobile/third-coast-terminals-affiliations.php">Affiliations</a>
												</li>
											</ul>
										</li>
									</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>
	<section class="main_section section_padding">
		

