
<?php
$current = basename($_SERVER['PHP_SELF']);
$pages = array(
    array(
        "title" => '<br>Contract<br> Manufacturing',
        "url" => 'contract-manufacturing-and-packaging.php',
        "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
        "keywords" => 'Contract Manufacturing and Packaging,Chemical Blending',
        "desc" => '',
        'right' => '-20',
        "sub" => array(
            array(
                "title" => 'Contract Manufacturing',
                "url" => 'contract-manufacturing-and-packaging.php',
                "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
                "keywords" => 'Contract Manufacturing and Packaging,Chemical Blending',
                "desc" => 'Contract Manufacturing is the deciated, custom production of products using a customers trade knowledge and intellectual property to manufacture products for them and their end customers.',
                "h1" => 'Contract Manufacturing and Packaging',
                "banner" => 'header-contract-manufacturing.jpg'
            ),
            array(
                "title" => 'Toll Blending',
                "url" => 'toll-blenders.php',
                "seo-title" => 'Chem Toll Blenders | Toll Blending & Toll Manufacturing - Third Coast Terminals',
                "keywords" => 'Toll Blending,Toll Manufacturing',
                "desc" => 'It’s a great idea to use our expertise, specialized equipment, and resources to complete your Toll Manufacturing.',
                "h1" => 'Toll Blending',
                "banner" => 'header-contract-manufacturing.jpg'
            ),
            array(
                "title" => 'Reaction Chemistry',
                "url" => 'reaction-chemistry.php',
                "seo-title" => 'Reaction Chemistry | Prepolymers - Third Coast Terminals',
                "keywords" => 'Reaction Chemistry,Urethane,Prepolymers',
                "desc" => 'Third Coast offers reaction chemistry currently majoring on polyurethane prepolymers.',
                "h1" => 'Reaction Chemistry',
                "banner" => 'header-contract-manufacturing.jpg'
            ),
            array(
                "title" => 'Filtration',
                "url" => 'filtration-services.php',
                "seo-title" => 'Filtration Services - Third Coast Terminals',
                "keywords" => 'Filtration Services',
                "desc" => 'Sparkler Filters with a laboratory model filter for pre-screening filtering operations, and a full size plant model with a scale-up ratio of approximately 1 to 120 by filtration surface area.',
                "h1" => 'Filtration Services',
                "banner" => 'header-contract-manufacturing.jpg'
            )
        )

    ),


    array(
        "title" => '<br>  Packaging<br> Services',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "sub" =>
            array(
                array(
                    "title" => 'Industrial Packaging',
                    "url" => 'industrial-chemicals-packaging.php',
                    "seo-title" => 'Chemicals Packaging - Third Coast Terminals',
                    "keywords" => 'Chemicals Packaging',
                    "desc" => 'Third Coast Terminals has six industrial chemical packaging lines for products to accommodate either drums or IBC’s.',
                    "h1" => 'Industrial Packaging',
                    "banner" => 'packaging-banner.jpg'
                	
                	                
                ),
                
                array(
                    "title" => 'cGMP/HACCP',
                    "url" => 'cgmp-haccp.php',
                    "seo-title" => 'CGMP | HACCP - Third Coast Terminals',
                    "keywords" => 'CGMP,HACCP',
                    "desc" => 'Our team is intimately familiar with HACCP requirements and we stand ready to partner with you regarding your packaging needs.'
                , "h1" => 'cGMP/HAACP',
                "banner" => 'packaging-banner.jpg'
                  
                ),
                array(
                    "title" => 'Halal/Kosher Packaging',
                    "url" => 'food-product-packaging.php',
                    "seo-title" => 'Food Product Packaging | Kosher Packaging | Halal Packaging - Third Coast Terminals',
                    "keywords" => 'Food Product Packaging,Kosher Packaging,Halal Packaging',
                    "desc" => 'Third Coast is equipped to handle a wide range of food-grade products from vegetable oil to pharmaceutical grade propylene glycols.'
                , "h1" => 'Halal/Kosher Food Product Packaging',
                "banner" => 'packaging-banner.jpg'
                 
                ),
                array(
                    "title" => 'Hazardous Packaging',
                    "url" => 'hazardous-materials-packaging.php',
                    "seo-title" => 'Hazardous Materials Packaging - Third Coast Terminals',
                    "keywords" => 'Hazardous Materials Packaging',
                    "desc" => 'Drum or tote filling for hazardous materials.'
                , "h1" => 'Hazardous Material Packaging',
                "banner" => 'packaging-banner.jpg'
                   
                )
            )
    ), 
    array(
        "title" => '<br>Logistics',
        "url" => 'logistics.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-32',
        //'divmenu'=>'1',
        "sub" =>
            array(
                array(
                    "title" => 'Logistics',
                    "url" => 'logistics.php',
                    "seo-title" => 'Logistics Services - Third Coast Terminals',
                    "keywords" => 'Terminal Logistics Services',
                    "desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.'
                , "h1" => 'Logistics',
                "banner" => 'header-logistics.jpg'
                ),
                array(
                    "title" => 'Terminal',
                    "url" => 'tank-terminal-services.php',
                    "seo-title" => 'Tank Terminal Services - Third Coast Terminals',
                    "keywords" => 'Tank Terminal Services',
                    "desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.'
                , "h1" => 'Tank Terminal Services',
                "banner" => 'header-logistics.jpg'
                ),
                array(
                    "title" => 'Rail Services',
                    "url" => 'railcar-loading-and-unloading.php',
                    "seo-title" => 'Railcar Loading | Railcar Unloading - Third Coast Terminals',
                    "keywords" => 'Railcar Loading,Railcar Unloading',
                    "desc" => ''
                , "h1" => 'Rail Services',
                "banner" => 'header-logistics.jpg'
                ),
                array(
                    "title" => 'Warehousing & Distribution',
                    "url" => 'chemicals-warehousing.php',
                    "seo-title" => 'Chemicals Warehousing - Third Coast Terminals',
                    "keywords" => 'Chemicals Warehousing',
                    "desc" => 'One half of our warehouse is equipped to handle flammable and other hazardous materials.'
                , "h1" => 'Chemicals Warehousing',
                "banner" => 'header-logistics.jpg'
                ),
                array(
                    "title" => 'Trailer Storage',
                    "url" => 'trailer-storage.php',
                    "seo-title" => 'Trailer Storage | Container Storage | Drop and Swap - Third Coast Terminals',
                    "keywords" => 'Trailer Storage,Container Storage,Drop and Swap',
                    "desc" => 'The ability and flexibility to store trailers of your drums, totes, or pails will expedite the packaging process and delivery of your products to the end user.'
                , "h1" => 'Trailer Storage',
                "banner" => 'header-logistics.jpg'
                ),
            )

    ),

    array(
        "title" => '<br> Lab Research & <br> Development',
        "url" => 'quality-assurance-testing.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-14',
        "sub" =>
            array(
                array(
                    "title" => 'Services',
                    "url" => 'quality-assurance-testing.php',
                    "seo-title" => 'Quality Assurance Testing | Quality Control Testing - Third Coast Terminals',
                    "keywords" => 'Quality Assurance Testing,Quality Control Testing',
                    "desc" => 'Third Coast Terminals has an extensive on-site laboratory, which can provide our customers with certified quality control and quality assurance testing for all manufactured products.'
                , "h1" => 'Quality Control Testing',
                "banner" => 'header-lab.jpg'
                ),
                array(
                    "title" => 'R&D',
                    "url" => 'third-coast-research-and-development.php',
                    "seo-title" => 'Third Coast Research & Development - Third Coast Terminals',
                    "keywords" => 'Third Coast Research & Development',
                    "desc" => 'Third Coast Terminals offers a variety of technical services.'
                , "h1" => 'Research & Development',
                "banner" => 'header-lab.jpg'
                )
            )

    ),

    array(
        "title" => '<br> <br> Quality',
        "url" => 'iso-9001-quality-system.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "h1" => '',
        "sub" =>
            array(
                    array(
                    "title" => 'Policies and Permits',
                    "url" => 'third-coast-terminals-permits.php',
                    "seo-title" => 'Third Coast Terminals Permits - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Permits',
                    "desc" => 'Permits currently held by Third Coast Terminals.',
                    "h1" => "Policies and Permits",
                    "banner" => 'header-quality.jpg'
                 ),

                array(
                    "title" => 'ISO Quality Management',
                    "url" => 'iso-9001-quality-system.php',
                    "seo-title" => 'Quality Management | Quality System | ISO 9001 | Certification - Third Coast Terminals',
                    "keywords" => 'Quality Management,Quality System,ISO 9001,Certification',
                    "desc" => 'The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries.'
                , 'h1' => 'ISO Quality Management',
                "banner" => 'header-quality.jpg'
                ),
                array(
                    "title" => 'Certifications',
                    "url" => 'third-coast-terminals-certifications.php',
                    "seo-title" => 'Certification - Third Coast Terminals',
                    "keywords" => 'Certification',
                    "desc" => 'Maintaining industry standard safety & compliance Certifications & Approvals.',
                    "h1" => 'Certifications',
                    "banner" => 'header-quality.jpg'
                ),
        
                array(
                    "title" => 'Responsible Care',
                    "url" => 'responsible-care-guiding-principles.php',
                    "seo-title" => 'Responsible Care Guiding Principles - Third Coast Terminals',
                    "keywords" => 'Responsible Care Guiding Principles',
                    "desc" => 'Third Coast Terminals is proud to participate in the American Chemistry Council Responsible Care initiative.',
                    'h1' => 'Responsible Care Guiding Principles',
                    "banner" => 'header-quality.jpg'

                ),
                array(
                    "title" => 'Responsible Distribution',
                    "url" => 'responsible-distribution.php',
                    "seo-title" => 'NACD | Responsible Distribution | Certification - Third Coast Terminals',
                    "keywords" => 'NACD,Responsible Distribution,Certification',
                    "desc" => 'Responsible Distribution is a third-party verification environmental, health, safety & security program through which members demonstrate their commitment to continuous performance improvement.',
                    "h1" => 'Responsible Distribution',
                    "banner" => 'header-quality.jpg'
                ),
            )

    ),
    array(
        "title" => '<br> <br> EHS&S',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-52',
        "sub" =>
            array(
                array(
                    "title" => 'Environmental',
                    "url" => 'environmental.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Environmental Services',
                    "banner" => 'environmental-banner.jpg'
                ),
                array(
                    "title" => 'Health',
                    "url" => 'health.php',
                    "seo-title" => 'Health - Third Coast Terminals',
                    "keywords" => 'Health',
                    "desc" => 'We protect our employee’s health by offering respiratory training, high quality equipment and medical evaluations including pulmonary function tests and fit test.',
                    "h1" => 'Health',
                    "banner" => 'environmental-banner.jpg'
                ),
                array(
                    "title" => 'Safety',
                    "url" => 'safety.php',
                    "seo-title" => 'Safety - Third Coast Terminals',
                    "keywords" => 'Safety',
                    "desc" => 'The safety of our employees is extremely important to us both at work and at home.',
                    "h1" => 'Safety',
                    "banner" => 'environmental-banner.jpg'
                ),

                array(
                    "title" => 'Security',
                    "url" => 'security.php',
                    "seo-title" => 'Security - Third Coast Terminals',
                    "keywords" => 'Security',
                    "desc" => ' Third Coast Terminals follows regulations set by the Department of Transportation (DOT) and Homeland Security.',
                    "h1" => 'Security',
                    "banner" => 'environmental-banner.jpg'

                ),
                array(
                    "title" => 'Emergency Preparedness',
                    "url" => 'emergency-preparedness.php',
                    "seo-title" => 'Emergency Preparedness - Third Coast Terminals',
                    "keywords" => 'Emergency Preparedness',
                    "desc" => 'Third Coast Terminals has prepared an Emergency Action Plan Manual.',
                    "h1" => 'Emergency Preparedness',
                    "banner" => 'environmental-banner.jpg'
                ),
                array(
                    "title" => 'Visitor Orientation',
                    "url" => 'visitor-orientation.php',
                    "seo-title" => 'Visitor Orientation - Third Coast Terminals',
                    "keywords" => 'Visitor Orientation',
                    "desc" => 'Please fill out our visitor orientation quiz.',
                    "h1" => 'Visitor Orientation',
                    "banner" => 'environmental-banner.jpg'
                ),

            )

    ),
    array( 
        "title" => '<br> <br> Human Resources',
        "url" => 'human-resources.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-25',
        "sub" =>
            array(

                array(
                    "title" => 'Human Resources',
                    "url" => 'human-resources.php',
                    "seo-title" => 'Human Resources - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => 'Human Resources information for Third Coast Terminals .',
                    "h1" => 'Human Resources',
                    "show" => 1,
                    "banner" => 'header-hr.jpg'
                ),
                array(
                    "title" => 'Apply Now',
                    "url" => 'apply.php',
                    "seo-title" => 'Apply Now - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => 'Apply Now at Third Coast Terminals.',
                    "h1" => 'Apply Now',
                    "banner" => 'header-hr.jpg'
                ),
                array(
                    "title" => 'TCT Cares',
                    "url" => 'tct-cares.php',
                    "seo-title" => 'TCT Cares - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => 'Third Coast Terminals events for the community .',
                    "h1" => 'Third Coast Terminals (TCT) Cares',
                    "banner" => 'header-hr.jpg'
                ),
                array(
                    "title" => 'Benefits',
                    "url" => 'benefits.php',
                    "seo-title" => 'Inside Sales - Third Coast Terminals',
                    "keywords" => 'Inside Sales',
                    "desc" => 'Inside Sales is the backbone of Third Coast Terminal’s Quality Initiative at the customer interface.',
                    "h1" => 'Benefits',
                    "banner" => 'header-hr.jpg'
                    
                ),
                array(
                    "title" => 'Equal Opportunities',
                    "url" => 'equal-opportunity.php',
                    "seo-title" => 'Human Resources - Third Coast Terminals',
                    "keywords" => 'Human Resources',
                    "desc" => 'Working together, we create the essential elements for life.',
                    "h1" => 'Equal Employment Opportunity',
                    "banner" => 'header-hr.jpg'
                ),
                array(
                    "title" => 'Photo Gallery',
                    "url" => 'photo-gallery.php',
                    "seo-title" => 'Third Coast Terminals Affiliations - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Affiliations',
                    "desc" => 'Meeting the quality needs and expectations of our customers is our highest priority.',
                    "h1" => 'Third Coast Terminals Photo Gallery',
                    "banner" => 'header-hr.jpg'
                ),
                array(
                    "title" => 'Contact Us',
                    "url" => 'contact-third-coast-human-resources.php',
                    "seo-title" => 'Third Coast Human Resources - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Human Resources',
                    "desc" => 'Meeting the quality needs and expectations of our customers is our highest priority.',
                    "h1" => 'Contact Third Coast Human Resources',
                    "banner" => 'header-hr.jpg'
                )
            )

    ),
    array(
        "title" => '<br> <br> About Us',
        "url" => 'gulf-coast-chemical-industry.php',
        "seo-title" => 'Gulf Coast Chemical Industry | Reactive Chemistry | Blending - Third Coast Terminals',
        "keywords" => 'Gulf Coast Chemical Industry,Reactive Chemistry,Blending',
        "desc" => 'Third Coast Terminals is a custom manufacturer and manager of petrochemicals.',
        "h1" => 'Petrochemical & Fine Chemicals Manufacturing and Handling Experts',
        'right' => '-30',
        "sub" => array(
            array(
                "title" => 'About Us',
                "url" => 'about-third-coast-terminals.php',
                "seo-title" => 'About Third Coast Terminals - Third Coast Terminals',
                "keywords" => 'About Third Coast Terminals',
                "desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
                "h1" => 'About Third Coast Terminals',
                "banner" => 'header-about.jpg'
                
            ),
            array(
                "title" => 'History',
                "url" => 'history.php',
                "seo-title" => 'History of Third Coast Terminals - Third Coast Terminals',
                "keywords" => 'Third Coast Terminals History',
                "desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
                "h1" => 'History',
                "show" => 1,
                "banner" => 'header-about.jpg'
                
            ),
            
			array(
                "title" => 'Service Overview',
                "url" => 'bulk-liquid-handling.php',
                "seo-title" => 'Bulk Liquid Handling | Contract Chemical Manufacturing | Toll Blending - Third Coast Terminals',
                "keywords" => 'Bulk Liquid Handling,Contract Chemical Manufacturing,Toll Blending',
                "desc" => 'Third Coast Terminals is a veteran provider of Bulk Liquid Handling Services for the companies that make up the worldwide Petrochemical, Fine Chemical, Food and Pharmaceutical Industries'
            , "h1" => 'Service Overview',
            "banner" => 'header-about.jpg'
            ),
            array(
                "title" => 'Press Releases',
                "url" => 'press-releases.php',
                "seo-title" => 'Press Releases - Third Coast Terminals',
                "keywords" => '',
                "desc" => ''
            , "h1" => 'Press Releases',
            "banner" => 'header-about.jpg' 
            ),
            array(
                "title" => 'Global Affiliates',
                "url" => 'thirdcoast.com',
                "url" => 'http://www.thirdcoast.com',
                "seo-title" => 'Third Coast Terminals Affiliates - Third Coast Terminals',
                "keywords" => 'Third Coast Terminals Affiliates',
                "desc" => 'A display of companies that are affiliated with Third Coast Terminals.',
                "h1" => 'Affiliates',
                "banner" => 'header-about.jpg'
            )
        )
    ),
    array( 
        "title" => '<br> <br> Contact Us',
        "url" => 'contact-third-coast-terminals.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-25',
        "sub" =>
            array(

                array(
                    "title" => 'Contact Info',
                    "url" => 'contact-third-coast-terminals.php',
                    "seo-title" => 'Contact Third Coast Terminals - Third Coast Terminals',
                    "keywords" => 'Contact Third Coast Terminals',
                    "desc" => 'Contact information for Third Coast Terminals .',
                    "h1" => 'Contact Third Coast Terminals',
                    "show" => 1,
                    "banner" => 'header-contact.jpg'
                ),
                array(
                    "title" => 'Inside Sales',
                    "url" => 'inside-sales.php',
                    "seo-title" => 'Inside Sales - Third Coast Terminals',
                    "keywords" => 'Inside Sales',
                    "desc" => 'Inside Sales is the backbone of Third Coast Terminal’s Quality Initiative at the customer interface.',
                    "h1" => 'Inside Sales',
                    "banner" => 'header-contact.jpg'
                    
                ),
                array(
                    "title" => 'Affiliations',
                    "url" => 'third-coast-terminals-affiliations.php',
                    "seo-title" => 'Third Coast Terminals Affiliations - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Affiliations',
                    "desc" => 'Meeting the quality needs and expectations of our customers is our highest priority.',
                    "h1" => 'Third Coast Terminals Affiliations',
                    "banner" => 'header-contact.jpg'
                )
            )

    )
    

);

// current page value
$currentPage = false;
$currentTopPage = false;
$currentInnerPage = false;
foreach ($pages as $page) {
    if ($page['url'] == $current) {
        $currentPage = $page;
    }
    if (isset($page['sub'])) {
        foreach ($page['sub'] as $sub) {
            if ($sub['url'] == $current) {
                $currentPage = $sub;
                $currentTopPage = $page;
                $currentInnerPage = $sub;
            }
        }
    }
}
?>
<?php


// stub creater

/*$mock = file_get_contents('/home/suresh/softs/hyper/oc/dev/mock.stub');

foreach ($pages as $page) {

    $d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $page['url'], $mock);
    if (isset($page['sub'])) {


        foreach ($page['sub'] as $sub) {
            $d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $sub['url'], $mock);
        }
    }
}
exit;*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php if (!$_SESSION['desktopmode']) { ?>

    <?php } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <?php if ($current == 'index.php') { ?>
        <title> Third Coast Terminals | Contract Manufacturing | Reactive Chemistry - Third Coast Terminals</title>
        <meta name="keywords" content="Third Coast Terminals,Contract Manufacturing,Reactive Chemistry">
        <meta name="description"
              content="Third Coast Terminals was established in 1998 and we have quickly grown to be recognized as one of the best supply chain partners.">
    <?php } else { ?>
        <title><?= $currentPage['seo-title'] ?></title>
        <meta name="keywords" content="<?= $currentPage['keywords'] ?>">
        <meta name="description" content="<?= $currentPage['desc'] ?>">
    <?php } ?>
    <meta name="author" content="www.petropages.com/creative/">
    <meta name="geo.region" content="US-TX"/>
    <meta name="geo.placename" content="Pearland"/>
    <meta name="geo.position" content="29.573049;-95.294782"/>
    <meta name="ICBM" content="29.573049, -95.294782"/>

    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">

	
    <link href="assets/style.css?v=0.1" rel="stylesheet">
    <link rel="canonical" href="http://thirdcoastterminals.com/">
    
    <?php if ($current !== 'index.php') { ?>
        <style>


            /* Sticky footer styles
    -------------------------------------------------- */
            html {
                position: relative;
                min-height: 100%;
            }

            body {
                /* Margin bottom by footer height */
                margin-bottom: 170px;
            }

            .footer {
                position: absolute;
                bottom: 0;
                width: 100%;
                /* Set the fixed height of the footer here */
                height: 170px;

            }

            /* Custom page CSS
            -------------------------------------------------- */
            /* Not required for template or sticky footer method. */

            body > .container {
                padding: 10px 0px 0;
                min-height: 600px;
            }

            .footer > .bg {
                background: #333132;
            }

        </style>
    <?php } ?>
	<style>
	@media (max-width: 992px){
		.navbar-theme .navbar-nav > li > a{
			    padding: 5px 10px 12px;
		}
	}
	@media (max-width: 1023px){
		.col-lg-6.bg-1{
			min-height:250px;width:100% !important;
		}
		.sprit {
			width: 100%;  margin-left: 0;  padding-left: 10px;  margin-top: 20px;
		}
		.learn-more[
			bottom: -100px !important;
		]
		.bg-2 .learn-more {
    bottom: -90px !important;
    right: 40px !important;
}
		.pull-right {
			float: left !important;
		}
		.bg-2 {
			padding-right: 0;min-height:250px;
		}
		.bg-1-width {
			width: 100%;
		}
	
	}
	</style>
<link href="images/favicon.png" rel="icon" type="image/x-icon" />
</head>
<body>
<!--<script type="text/javascript">
        if (screen.width <= 740) {
        window.location = "/mobile";
    }
</script>-->

<!--========================================================
                          HEADER
=========================================================-->
<header class="<?= $current == 'index.php' ? '' : 'inner'; ?>">
    <div class="top-bar  hidden-xs">
        <div class="container ">
            <div class="clearfix">
                <div class="leftside col-lg-4">
                    <div>
                      
                       
                      
                        <a href="https://www.google.co.in/maps/place/1871+Mykawa+Rd,+Pearland,+TX+77581,+USA/@29.5730486,-95.2969897,17z/data=!3m1!4b1!4m2!3m1!1s0x86409143e11e8a25:0x666fc47f0e49b31"
                           target="_blank"> 1871 Mykawa Rd, Pearland, TX 77581</a> 
                    </div>
                </div>

                <div class="rightside col-lg-8 ">
                    <div>

                        <ul class="list-inline list-unstyled pull-right">
                            <li><span>Toll Free </span> 877.412.0275</li>
                            <li><span> Fax  </span> 281.412.0245</li>
                            <li><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
<li>
                            	<a href="index.php">English</a> |  <a href="/chinese/">中文</a>
                            </li>
                            
                        </ul>
                    </div>

                </div>
                <div class="clearfix"></div>

            </div>
        </div>

    </div>

    <!--<div class="mobile-top-bar hidden-lg">
        <div class="container mobile-top">
            <div class="col-md-6 right-border col-xs-6 s">

                <span>877.412.0275</span>
            </div>
            <div class="col-md-6 col-xs-6 s">
                <span><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>-->

    <div class="menu">
        <div class="container">
            <?php include "nav.php" ?>
        </div>
    </div>

</header>


<?php if ($current == 'index.php') { ?>
    <section class="full" id="slider" style="width:100%;overflow:hidden;margin-bottom:0;">

       <!--<div
            id="header-video"
            class="full-height black-wrapper video2"
           data-vide-bg="mp4: assets/video/3CFrontpagecolor, webm: style/video/ocean, ogv: style/video/ocean, poster: style/video/ocean.jpg"
            data-vide-options="position: 0% 10%">
            

            <div class="text-center slider-text">
             

                <div class="title">A world-Class</div>
                <div class="title">Service Company</div>
                <div class="sub">Providing customized solutions</div>


                <a href="about-third-coast-terminals.php" class="btn btn-white btn-primary btn-white slider-btn"> Learn
                    More </a>


            </div>
            <a href="#content" class="down"> <i class="ion-chevron-down"></i></a>
           
        </div>-->
		
      <video  style="width:100%;" muted autoplay loop >
          <source src="assets/video/3CFrontpage2018-2.mp4" type="video/mp4">
             </video>
             
			<div class="full-height black-wrapper video2">
			
            <div class="text-center slider-text">
             

                <div class="title">A world-Class</div>
                <div class="title">Chemical Service Company</div>
                <div class="sub">Providing customized solutions</div>


                <a href="about-third-coast-terminals.php" class="btn btn-white btn-primary btn-white slider-btn"> Learn
                    More </a>


            </div>
            <a href="#content" class="down"> <i class="ion-chevron-down"></i></a>
           
      </div>
	  
    </section>
<?php } else {

    if ($currentInnerPage) {

        $img = !isset($currentInnerPage['banner']) ? "assets/images/banner2_02.jpg" : "assets/images/" . $currentInnerPage['banner'];
        ?>
        <section id="inner-slider" style="background: url(<?= $img ?>)">


            <div class="container" style="position: relative">
                <div class="inner-header">

                    <nav class="inner-nav hidden-md hidden-xs">
                        <ul class="nav nav-tabs">

                            <?php foreach ($currentTopPage['sub'] as $item) { ?>
                                <li class="<?= $current == $item['url'] ? 'active' : ''; ?>"><a
                                        href="<?= $item['url'] ?>"><?= $item['title'] ?></a>
                                </li>

                            <?php } ?>
                        </ul>
                    </nav>
                    <div class="page-title">
                        <h1><?= $currentInnerPage['h1'] ?></h1>
                    </div>

                </div>

            </div>

        </section>
    <?php } ?>
<?php } ?>
<div id="content" class="clearfix"></div>

 <?php if ($current == 'index1.php') { ?>
		<div class="container-fluid">
	<?php } else {?>
		<div class="container  shadow"  >
	<?php } ?>
	
	
	
	