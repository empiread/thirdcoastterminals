<?php include "header.php"
?>

<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

            <h3>
                Our people make the difference.
            </h3>
			<p>
				Our Human Resources Mission is to create a Highly Productive Team and a Great Place to Work! Find out more about joining the Third Coast Family:
			</p>
			<br>
			<br>
			<div class="col-lg-12">
				<div class="col-lg-4" style="text-align:center;">
					<a href="benefits.php"><img src="images/Benefits.png" title="Employee Benefits" alt="Employee Benefits"></a>
					<p class="icons">EMPLOYEE<br>BENEFITS</p>
				</div>
				<div class="col-lg-4" style="text-align:center;">
					<a href="equal-opportunity.php"><img src="images/Equal.png" title="Equal Opportunities" alt="Equal Opportunities"></a>
					<p class="icons">EQUAL<br>OPPORTUNITIES</p>
						
				</div>

				<div class="col-lg-4" style="text-align:center;">
                    <a href="apply.php"><img src="images/Application.png" title="Apply Now" alt="Apply Now"></a>
                    <p class="icons">APPLY NOW</p>
                </div>
				
			</div class="col-lg-12">
			<div class="col-lg-12"><br><br><br></div>
			<div class="col-lg-12">
				
				
				<div class="col-lg-4" style="text-align:center;">
					<a href="photo-gallery.php"><img src="images/PhotoGallery.png" title="Photo Gallery" alt="Photo Gallery"></a>
					<p class="icons">PHOTO<br></b>GALLERY</p>
				</div>

				<div class="col-lg-4" style="text-align:center;">
					<a href="contact-third-coast-human-resources.php"><img src="images/ContactUs.png" title="Contact Us" alt="Contact Us"></a>
					<p class="icons">CONTACT US</p>
				</div>
				
				
			</div class="col-lg-12">
			<div class="col-lg-12"><br><br><br></div>

			<p></p>
			<p></p>

            <p>
                Working together, we create the essential elements for life&mdash;polymers, chemicals and fuels
                that go into products and materials that you use every day. We operate with a lean staff, making the contributions of each individual critical to our
                success. We emphasize efficient, safe and reliable operations. Our shared purpose is to create
                exceptional value for our customers while providing world class service.
            </p>
		</div>
		<div class="col-lg-3 pl-10">
			 <br>
			 
		</div>
		
		
		<div class="col-lg-12">
            <p>
                <b>We seek talented, results-oriented team players who...</b>
            </p>
            <ul>
                <ul>
                    <li>
                        thrive on change
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        communicate effectively
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        challenge each other to improve
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        treat others with respect
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        operate safely and responsibly with a focus on integrity
                    </li>
                </ul>
                </li>
            </ul>

            <p>
               <b> In return, we
                nurture a culture where you can...</b>
            </p>

            <ul>
                <ul>
                    <li>
                        succeed professionally
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        grow personally
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        team with others to develop innovative solutions
                    </li>
                </ul>
                </li>
                <ul>
                    <li>
                        be rewarded well for your results
                    </li>
                </ul>
                </li>
            </ul>
            <p>
                Our
                opportunities for growth and achievement are strengthened by the innovation and creativity of our
                people. To succeed, we need individuals who believe they can&mdash;and do&mdash;make a
                difference....providing world class service.
            </p>

            
            <br>

            </p>
        </div>
    </div>
</div>

<!--========================================================        FOOTER
=========================================================-->
<?php include "footer.php" ?>
