<?php include_once('header.php'); ?> 

<div class='content_container col-xs-12  col-sm-8 col-md-6  col-lg-6 padding0 pull-right'>
	<div class='sub_container'>
		<div class='clearfix10'></div>
		<?php include_once('breadcrumb.php'); ?> 
		<h1 class='margin0'>EMPLOYEE DOCUMENTS</h1>
		<div class="clearfix10"></div>
		<div class='text-left scroll_container slide_panel'>
			<div class="panel-group" id="accordion">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Procedures
								<span class='drop_icon_1 glyphicon glyphicon-plus'></span>
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse"><!--<div id="collapseOne" class="panel-collapse collapse in">-->
						<div class="panel-body">
							<ul class='list_container'>
								<li><a href='void:javascript(0);'>BAMP-000-NDT-O&E</a></li>
								<li><a href='void:javascript(0);'>BAMP-001-NDT-WP</a></li>
								<li><a href='void:javascript(0);'>BAMP-002-NDT-ASME-RT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-003-NDT-ASME-UT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-004-NDT-ASME-MT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-005-NDT-ASME-PT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-006-NDT-API1104-RT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-007-NDT-API1104-UT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-008-NDT-API1104-MT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-009-NDT-API1104-PT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-010-NDT-ASME-UTPA-PIPING</a></li>
								<li><a href='void:javascript(0);'>BAMP-012-NDT-RADIOGRAPHIC TECHNIQUE</a></li>
								<li><a href='void:javascript(0);'>BAMP-013-NDT-ASMEB31.8-RT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-014-NDT-RS-10CFR37</a></li>
								<li><a href='void:javascript(0);'>BAMP-015-NDT-PA5L6450T</a></li>
								<li><a href='void:javascript(0);'>BAMP-016-NDT-PA5L6460T</a></li>
								<li><a href='void:javascript(0);'>BAMP-017-NDT-PA5L6430T</a></li>
								<li><a href='void:javascript(0);'>BAMP-018-NDT-PA5L6475T</a></li>
								<li><a href='void:javascript(0);'>BAMP-020-NDT-API6A-MT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-021-NDT-API6A-PT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-022-NDT-API6A-RT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-023-NDT-API6A-UT-GEN</a></li>
								<li><a href='void:javascript(0);'>BAMP-024-NDT-API6A-PAUT-GEN</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Documents
								<span class='drop_icon_2 glyphicon glyphicon-plus'></span>
							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<ul class="list_container">
								<li><a href='void:javascript(0);'>BAMD-000-NAMING CONVENTION D,F,P</a></li>
								<li><a href='void:javascript(0);'>BAMD-001-PURCHASING WORKFLOW</a></li>
								<li><a href='void:javascript(0);'>BAMD-009-LETTERHEAD</a></li>
								<li><a href='void:javascript(0);'>BAMD-011-PROPOSAL NAMING CONVENTION</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class='clearfix20'></div>
	</div>
</div>
<div class='clearfix20'></div>
<?php include_once('footer.php'); ?> 