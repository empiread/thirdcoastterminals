<?php include "header.php"?>
 
<div class="mtb-25 deliver" >
	
    
       <!-- <div class="col-lg-6 " style="margin-left: -10px;">
            <img src="images/optimized-solutions.jpg" class="img-responsive" alt="">
       </div>-->
        <div class="col-lg-6 sprit">
            <div class="headline">
                <h2><b>We Deliver Optimized Solutions</b></h2>
            </div>

            <p>
                <br>
                Third Coast Terminals was established in 1998 and we have quickly grown to be recognized as one of the
                best supply chain partners. Our reputation as a provider for liquid storage, toll manufacturing (which
                includes blending and reactive processes), contract packaging, and trans-load services, continues to
                grow as we match our business model to the needs of our customers serving the global petrochemical
                market. Our customers rely on Third Coast Terminals to quickly and cost effectively mobilize
                resources that capitalize on market opportunities as they arise.

                <div>
                    <br>
                    <a href="contact-third-coast-terminals.php" class="btn  btn-primary btn-blue" title="Contact Third Coast Terminals"> Contact Us </a>
                </div>

            </p>
            </div>
           <div class="col-lg-6 bg-1" style="width: 49.5%;">
           	<div class="pull-left bg-1-width">

            <h3><b><u>We Pursue Logistical Solutions</u> </b></h3>

            Third Coast has extensive experience in  improving supply chains, managing product flow, reducing
            inefficiencies in inventory management, and effectively delivering optimized solutions for any size
            organization.

            <div class="learn-more" style="bottom:-175px">
                <a href="tank-terminal-services.php" class="btn  btn-primary btn-blue" title="Tank terminal services"> Learn More </a>
            </div>

        </div>
           </div> 
   
</div>
<div class="mb-15 ">
   

    <div class="col-lg-6 bg-2 sprit">
        <div class="pull-left bg-1-width">
            <h3><b><u>World Class Safety</u></b></h3>

            <p>
                Third Coast Terminals is committed to the Environmental, Health, Safety and Security of our Team Members, Customers and our Surrounding Community. We have been awarded quality certifications including ISO 9001-2008, Responsible Distribution and a Responsible Care Partner. Additionally, in 2016 we were recognized by Texas Mutual Insurance for our dedication to Employee Health and Safety.
            </p>

            <div class="learn-more" style="right: 0px;bottom:-161px">
                <a href="environmental.php" class="btn  btn-primary btn-blue" title="Environmental"> Learn More </a>
            </div>
        </div>

    </div> 
     <div class="col-lg-6 " style="width: 50%; text-align: center;">
    <div id="fb-root"></div>
				<script>
				(function(d, s, id) {
				    var js, fjs = d.getElementsByTagName(s)[0];
				    if (d.getElementById(id))
				        return;
				    js = d.createElement(s);
				    js.id = id;
				    js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				    fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
				</script>
			<div class="fb-post" data-href="https://www.facebook.com/ThirdCoastTerminals/photos/a.1456161274445141.1073741828.131064056954876/1653179691409964/?type=3"></div>
       
        
    </div>
    <div class="clearfix"></div>
</div>

<div class="home-reaction ">
    <div class="container">
        

        
            

           

            <div class="col-lg-10">
                
                <a href="press-releases.php#anchor" class="btn btn-press btn-primary btn-press" title="Contract manufacturing and packaging."> Learn More </a>
            </div>

       
    </div>
	 
</div>
<div class="clearfix"><p>&nbsp;</p></div> 
 
<?php include "footer.php" ?>
