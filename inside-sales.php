<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Inside
                Sales is the backbone of Third Coast Terminal’s Quality Initiative
                at the customer interface. Across our Inside Sales Representative
                Team, we strive for service excellence by developing explicit and
                “customer-transparent” service standards, establish regular
                monitoring, accurate performance measurement and coach improvement by
                performance measurement. These efforts help us to continuously
                improve our service excellence.</p>

            <p>Our
                Inside Sales Representative team is committed to provide quick
                response time, effort and flexibility with our customers in handling
                orders, managing inventory and helping to resolve issues you
                occasionally encounter in normal business.</p>

            <p></p>

            <p>To
                our valued customers that we have been fortunate enough to have as
                client’s and friends, thank you for trusting us with your products
                and allowing us to handle your distribution over the years. Trust is
                key, and being able to lower your risks and liabilities with your
                product handling, allows you to focus on your company, and not stress
                about your supply chain. The optimal quality of operations you are
                looking for, and the convenience of not worrying about storage,
                handling, and logistic needs is our recipe for an efficient supply
                chain. Third Coast Terminals is looking forward to working together
                with you for many years still to come.</p>
                
             <p></p>
             
             <ul class="list-unstyled">
                    <li><span><strong>Email:</strong>  </span><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
                    <li><span> <strong>Phone:</strong></span> 281.412.0275</li>
                    <li><span> <strong>Toll Free:</strong></span> 877.412.0275</li>   
              </ul>  
                
                </div>
                
               
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/inside-sales1.jpg" title="Inside Sales" alt="Inside Sales">


                    </div>

                    <div class="item">
                        <img src="images/inside-sales2.jpg" title="Inside Sales" alt="Inside Sales">


                    </div>

                    <div class="item">
                        <img src="images/inside-sales3.jpg" title="Inside Sales" alt="Inside Sales">
                    </div>
                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



