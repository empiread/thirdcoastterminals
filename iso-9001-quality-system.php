<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">
            <h2>About ISO 9001</h2>
        <div class="col-lg-6">

            

            <p>The
                ISO 9001 Quality Management standard is implemented by over one
                million companies and organizations in over 170 countries. The
                standard provides a common self-improvement template for businesses
                and is continually evolving.</p> 
                <p><br></p>
                <p><a href="assets/pdf/2018-ISO-9001-Certificate.pdf" target="_blank"><img style="padding-right:5px;"src="assets/pdf/pdf.jpg" alt="pdf"></a><a href="assets/pdf/2019-ISO-9001-Certificate.pdf" target="_blank">ISO 9001:2015 Certification of Registration </a><br> </p>
				<p><br></p>
            <h4 dir="ltr"><span>Quality
                Management Principles
            </span></h4>

            <p>The
                standard is based on a process approach to business management.
                Using this approach, business processes are managed through
                monitoring of key performance indicators. Emphasis is given to
                customer focus. The quality of the business is measured according to
                how well it performs against the self-established indicators. A
                cycle of continual improvement is established to assure positive
                change. The cycle is monitored using internal and external
                surveillance audits and a minimum of one top management review per
                year.</p>



            <p>Third
                Coast Terminals is ISO 9001:2015 Certified by ABS Quality Evaluations, Inc.</p>
        </div>
        <div class="col-lg-6">
               <p><a href="http://www.abs-qe.com/" target="_blank"><img style="padding-left:25px;" src="assets/images/abs-anab-iso-9001-2015.jpg" title="ABS Quality Evaluations, Inc." alt="ABS Quality Evaluations, Inc."></a></p>

             <p><br></p>   
         </div>
         
     </div>
     <div class="col-lg-12">
        <p><br></p>
        <h3>About ISO 14001</h3> 
        
         <div class="col-lg-6">       
               
             <p>ISO 14001:2015 specifies the requirements for an environmental management system that an organization can use to enhance its environmental performance. ISO 14001:2015 is intended for use by an organization seeking to manage its environmental responsibilities in a systematic manner that contributes to the environmental pillar of sustainability.</p>
             <p><br></p>
             
                <p><a href="assets/pdf/2018-ISO-14001-Certificate.pdf" target="_blank"><img style="padding-right:5px;"src="assets/pdf/pdf.jpg" alt="pdf"></a><a href="assets/pdf/2019-ISO-14001-Certificate.pdf" target="_blank">ISO 14001:2015 Certification of Registration</a></p>
        </div>
        <div class="col-lg-6">
                <p><a href="http://www.abs-qe.com/" target="_blank"><img style="padding-left:25px;" src="assets/images/abs-anab-iso-14001-2015.jpg" title="ABS Quality Evaluations, Inc." alt="ABS Quality Evaluations, Inc."></a></p>
			<p><br></p>
        </div>
    </div>


    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



