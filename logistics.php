<?php include "header.php" ?>

<style type="text/css">
 /*Deliver Customized Container*/
.deliver_customized_container {
    background: #f6f7f7;
}
.deliver_customized_inner_container {
    text-align: center;
    padding: 3.5em 0;
}
.deliver_customized_inner_container p.deliver_customized_heading {
    font-family: 'Helvetica';
    font-size: 1.4em;
    font-weight: 900;
    color: #191919;
    text-align: center!important;
    text-decoration: none!important;
}
.deliver_customized_inner_container p.deliver_customized_subheading {
    font-family: 'Helvetica';
    margin: 1.5em 0;
    font-weight: 900;
    font-size: 0.8em;
    color: #034d70;
    text-align: center;
}
.deliver_customized_inner_container p.deliver_customized_subtext {
    color: #455560;
    font-size: 0.6em;
    line-height: 1.6em;
    margin-bottom: 4em;
    text-decoration: none;
}
.seperator {
    background: #034d70;
    width: 2.8em;
    height: 0.15em;
    margin: 0 auto;
    margin-top: 2em;
}
.deliver_customized_list_container {
    margin-top: 3.5em;
    text-align: center;

}

.deliver_customized_list_container p{
    margin-top: 1em;
    text-align: left;
    color: #455560;
    
}

.deliver_customized_list_container p a:hover{
    margin-top: 1em;
    text-align: left;
    color: #455560;
    text-decoration: none !important;
    
}
.deliver_customized_list_container ul li {
    width: 24%;
    display: inline-block;
    margin-left: 10px;
    vertical-align: top;
    padding: 0 0.5em;
}
.deliver_customized_list_container ul li:first-child {
    margin-left: 0px;
}
.deliver_customized_list_container ul li figure {
    background: #fff;
    border: 0.05em solid #e3e5e7;
    padding: 2.5em 1em;
}
.deliver_customized_list_container ul li figure  
.deliver_customized_list_icon {
    max-width: 8.5em;
    width: 100%;
    height: 8.5em;
}
.deliver_customized_list_container ul li figure figcaption {
    color: #000;
    margin-top: 2em;
    font-size: 1.4em;
    font-family: 'Helvetica'!important;
}
.deliver_customized_list_container ul li a:hover figure {
    background: #034d70;
}
.deliver_customized_list_container ul li a:hover figure figcaption {
    color: #fff;
    font-size: 1.4em;
    font-family: 'Helvetica'!important;
    text-align: center;
    text-decoration: none;
}
.deliver_customized_list_container ul li a:hover figure svg path {
    fill: #fff !important;
}
.deliver_customized_list_container ul li a:hover figure svg path#path2997, 
.deliver_customized_list_container ul li a:hover figure path#path3089, 
.deliver_customized_list_container ul li a:hover figure path#path2981, 
.deliver_customized_list_container ul li a:hover figure path#path2947 {
    fill: #256684 !important;
}
/*Deliver Customized Container*/

</style>



    <div class="mtb-25">
       
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 p-0 deliver_customized_list_container">
                    <ul>
                        <li>
                            <a href="https://thirdcoastterminals.com/tank-terminal-services.php">
                                <figure>
                                    <img src="assets/images/Tank.svg" class="svg deliver_customized_list_icon" alt="Tank Terminal Services" height="150px" width="150px" />
                                    <figcaption>Terminal</figcaption>
                                </figure></a>
                                <p>Third Coast has an extensive terminal operation with more than 170+ tanks in current service. Plans are in place to expand  our Pearland Operations with additional tanks exceeding 10 million gallons of total liquids storage.</p>
                            
                        </li>
                         <li>
                            <a href="https://thirdcoastterminals.com/railcar-loading-and-unloading.php">
                                <figure>
                                    <img src="assets/images/Rail.svg" class="svg deliver_customized_list_icon" alt="Railcar Transloading" height="150px" width="150px" />
                                    <figcaption>Rail Services</figcaption>
                                </figure></a>
                                <p>Third Coast Terminals continued to expand our rail storage to meet the aggressive growth of our customers with this need. Our latest expansion has increased on site storage to a new total of more than 80+ rail cars.</p>
                            
                        </li>
                        <li>
                            <a href="https://thirdcoastterminals.com/chemicals-warehousing.php">
                                <figure>
                                 <img src="assets/images/Warehouse.svg" class="svg deliver_customized_list_icon" alt="Chemical Warehousing and Distribution" height="150px" width="150px"/>
                                    <figcaption>Warehouse & Distribution</figcaption>
                                </figure></a>
                                <p>Third Coast Terminals offers 10 high-dock doors for daily receiving and shipping of packaged goods, including two LTL doors from 10 AM to 4 PM. Our warehouse complex manages over 30,000 drums in its 160,000 square feet of space.</p>
                            
                        </li>
                        <li>
                            <a href="https://thirdcoastterminals.com/trailer-storage.php">
                                <figure>
                                    <img src="assets/images/Trailer.svg" class="svg deliver_customized_list_icon" alt="Trailer Storage" height="150px" width="150px"/>
                                    <figcaption>Trailer Storage</figcaption>
                                </figure></a>
                                <p>Trailer storage is offered for our blending, reaction chemistry, trans-loading and packaging customers. You will find that  the ability and flexibility to store trailers of your drums, totes, or pails will expedite the packaging process and delivery  of your products to the end user.</p>
                            
                        </li>
                    </ul>
                </div>
            </div>


     


    </div>





<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
