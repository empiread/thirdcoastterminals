$.fn.isOnScreen = function(){
	var win = $(window);
	var viewport = {
		top : win.scrollTop(),
		left : win.scrollLeft()
	};
	viewport.right = viewport.left + win.width();
	viewport.bottom = viewport.top + win.height();
	var bounds = this.offset();
	bounds.right = bounds.left + this.outerWidth();
	bounds.bottom = bounds.top + this.outerHeight();
	return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};

$(document).ready(function(){ 

	$(".btn_submit").click(function(){
		var error="";
		var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
		if($(".txt_name").val()=="")
		{
			error ="\nName";
		}
		if($(".txt_email").val()=="")
		{
			error +="\nEmail\n";
		}
		else if(!pattern.test($(".txt_email").val()))
		{
			error += "\nEmail is not valid\n";
		}
		if(error!="")
		{
			alert("Please fill the below fields:"+  error);
			return false;
		}
	});



	$('body').flowtype({
		 minimum   : 640,
		 maximum   : 5000,
		 minFont   : 15,
		 maxFont   : 1000,
		 fontRatio : 60
	});

	/*SVG function*/
	$('img.svg').each(function(){
		//alert("1");
		var $img = $(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');
		$.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = $(data).find('svg');
			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}
			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');
			// Replace image with new SVG
			$img.replaceWith($svg);
		});
	});
	/*SVG function*/
	
	/*Mobile menu*/
	$(".mobile_arrow").click(function(){

		$(".submenu").slideUp("slow", function(){
			
			if($(this).parents("li").find("ul").is(":visible")){
				console.log($(this));
				$(this).parents("li").find(".up_arrow").show();
				$(this).parents("li").find(".down_arrow").hide();
			} else{
			   $(this).parents("li").find(".down_arrow").show();
			   $(this).parents("li").find(".up_arrow").hide();
			}
		});
		$(this).parents("li").find("ul").stop().slideToggle("slow", function(){
			
			if($(this).parents("li").find("ul").is(":visible")){
				console.log($(this));
				$(this).parents("li").find(".up_arrow").show();
				$(this).parents("li").find(".down_arrow").hide();
			} else{
			   $(this).parents("li").find(".down_arrow").show();
			   $(this).parents("li").find(".up_arrow").hide();
			}
		});

	});
		
		
	
	
	$(".menu_icon_inner_block .menu_open").click(function(){
		$(".menu_section").addClass("side_menu_open");
		$(".menu_open").hide();
		$(".menu_close").show();
		//$(".blue_header").hide();
		$(".section_padding").css("padding-top",$(".header_block").height());
		$('body').css('overflow', 'hidden');
		//$(".menu_section").css("height",$(window).height());
		
	});
	$(".menu_close").click(function(){
		$(".menu_section").removeClass("side_menu_open");
		$(".menu_open").show();
		$(".menu_close").hide();
		$(".blue_header").show();
		$('body').css('overflow', 'auto');
	});
	/*Mobile Menu*/

	/*Video Play*/
	var myVideo = document.getElementById("front-video");
	 
	 $('#playVideo').click(function(){
		 $(".inner_video video").css("max-width",$(".video_inner_box_container").width());
		 $(".inner_video video").css("max-height",$(".video_inner_box_container").height());
		$(".video_img_div").hide();
		$(".inner_video").show();
		
		 myVideo.play();
		
		
	 });

	 $('.video_close_btn').click(function(){
		$(".video_img_div").show();
		$(".inner_video").hide();
		 myVideo.pause();
	});

	$(".visitor_btn").click(function(){

		  if($("#myModal").modal({show: true})){
				$('body').css('overflow', 'hidden');
		  }else{
				$('body').css('overflow', 'auto');
		  }

	});
	$(".contractor_btn").click(function(){

		  if($("#myModal_2").modal({show: true})){
				$('body').css('overflow', 'hidden');
		  }else{
				$('body').css('overflow', 'auto');
		  }

	});



	$(".close_icon").click(function(){

		$('body').css('overflow', 'auto');
		  

	});

	

        
	/*Video Play*/
	
	/*Equal Height*/
	$.fn.extend({
		equalHeights: function() {
			var top=0;
			var row=[];
			var classname=('equalHeights'+Math.random()).replace('.','');
			$(this).each(function() {
				var thistop=$(this).offset().top;
				if (thistop>top) {
					$('.'+classname).removeClass(classname); 
					top=thistop;
				}
				$(this).addClass(classname);
				$(this).height('auto');
				var h=(Math.max.apply(null, $('.'+classname).map(function(){ return $(this).outerHeight(); }).get()));
				$('.'+classname).height(h);
			}).removeClass(classname); 
		}
	});
	//setTimeout(function(){$('.main-div').equalHeights();},400);
	setTimeout(function(){$(".deliver_customized_list_container ul li a figure figcaption").equalHeights();},400);
	/*Equal Height*/
	var animation_box_height=$(".box ul li").height();
	$(".box").css("height",animation_box_height);
	$(window).on("resize load scroll",function() {
	
	});

	$(window).on('scroll resize', function (){
		/*$(".header_menu_block").css("top","0px");
		//$(".mobile_menu").css("top","0px");
		var header_menu_height= $(".header_menu_block").height();
		$(".mobile_menu").css({"position":"fixed","width":"100%","top":"0px"});
		$(".section_padding").css("padding-top",$(".mobile_menu").height());
		$(".menu_section").css("top",$(".mobile_menu").height());
		var header_addsress_height= $(".blue_header").height();
		if($(this).scrollTop() <= header_addsress_height ){
			$(".mobile_menu").css({"position":"unset","width":"auto","top":"unset"});
			$(".header_menu_block").css("top",header_addsress_height);
		}*/
		if($(window).width() <= '1200'){
			//$('.header_block').css({'position':'unset'});
			//$(".section_padding").css("padding-top",'0px');
			$(".section_padding").css("padding-top",$(".header_block").height());
			if($(".blue_header").height() <= $(window).scrollTop()){
				$(".blue_header").hide();
				$('.header_inner_block').css({'position':'fixed', 'top':'0', 'width':'100%'});
			}else{
				$(".blue_header").show();
				$('.header_inner_block').css({'position':'unset'});
				//alert($(".side_menu_open").length);
				if($(".side_menu_open").length){
					$(".blue_header").hide();
					$('.header_inner_block').css({'position':'fixed', 'top':'0', 'width':'100%'});
				}
			}
		}
	});
	
	
	
});

$(window).on("orientationchange resize load ",function() {
	var header_height= $(".header_inner_block").height();
	var header_addsress_height= $(".blue_header").height();
	var header_menu_height= $(".header_menu_block").height();
	var total_height= header_menu_height+header_addsress_height;
	$(".menu_section").css("top",$(".header_block").height());
	$(".section_padding").css("padding-top",header_height);
	if($(window).width() <= '1200'){
		$(".section_padding").css("padding-top",$(".header_block").height());
	}
	console.log( header_addsress_height);
	//$(".header_menu_block").css("top",header_addsress_height);
	
	/*Equal Height*/
	$.fn.extend({
		equalHeights: function() {
			var top=0;
			var row=[];
			var classname=('equalHeights'+Math.random()).replace('.','');
			$(this).each(function() {
				var thistop=$(this).offset().top;
				if (thistop>top) {
					$('.'+classname).removeClass(classname); 
					top=thistop;
				}
				$(this).addClass(classname);
				$(this).height('auto');
				var h=(Math.max.apply(null, $('.'+classname).map(function(){ return $(this).outerHeight(); }).get()));
				$('.'+classname).height(h);
			}).removeClass(classname); 
		}
	});
	
	setTimeout(function(){$(".deliver_customized_list_container ul li a figure figcaption").equalHeights();},400);
	/*Equal Height*/
	
});
