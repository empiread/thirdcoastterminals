<?php include "header.php" ?>


<div class="container mtb-25">
	
        <div class="col-xs-12">
        	<h3>Challenging work can be very rewarding.</h3>
        	<p>Our competitive benefits program is meant to enhance and preserve your work/life balance and help you plan and prepare for tomorrow.</p>
          </div>
           
           <div class="col-xs-12">
           <div class="row">
	            <div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/HealthInsurance.png"><br>
				<strong>MEDICAL</strong>
	        	</div>
	        	
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/Dental.png"><br>
				<strong>DENTAL</strong>
	        	</div>
        	</div>
        	<br />
        	<div class="row">
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/Pharmacy.png"><br>
				<strong>PHARMACY COVERAGE</strong>
	        	</div>
	        	
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/Vision.png"><br>
				<strong>VISION</strong>
	        	</div>
        	</div>
        	<br />
        	<div class="row">
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/LifeInsurance.png"><br>
				<strong>LIFE INSURANCE</strong>
	        	</div>
	        	
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/PaidHolidays.png"><br>
				<strong>11 COMPANY PAID HOLIDAYS</strong>
	        	</div>
        	</div>
        	<br />
        	<div class="row">
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/401K.png"><br>
				<strong>4% 401K MATCH</strong>
	        	</div>
        	
	        	<div class="col-xs-6" style="text-align:center;">
				<img src="../mobile/images/DirectDeposit.png"><br>
				<strong>DIRECT DEPOSIT</strong>
	        	</div>
    		</div>
    		<br />
</div>

		<div class="col-lg-6">
			<h3>In addition to the benefits offered above, Third Coast Terminals also offers:</h3>
			
			
			
			<div class="col-xs-6">
				<strong>Company Paid:</strong>
				<ul>
					<li>Short Term Disability</li>
					<li>Onsite Annual Physicals</li>
					<li>3 Day Paternity Leave</li>
					<li>2 Weeks (Minimum) Paid Vacation</li>
				</ul>
			</div>
			<div class="col-xs-6">
				<strong>Additional Benefits:</strong>
				<ul>
					<li>Company Assisted Medical<br>Deductible Reimbursement</li>
					<li>Employee Assistance Program</li>
					<li>Education Reimbursement</li>
				</ul>
			</div>
		</div>
        
    </div>



<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
