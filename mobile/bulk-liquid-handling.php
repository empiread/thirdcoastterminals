<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>Third Coast Terminals is a veteran provider of Bulk Liquid Handling
                Services for the companies that make up the worldwide Petrochemical,
                Fine Chemical, Food and Pharmaceutical Industries. These industries
                form the foundation of our global economy. In order to meet the
                needs of these complex and highly regulated industries, we rely on
                highly developed systems that operate on the foremost level of
                quality, safety and efficiency.</p>

            <p>Third
                Coast Terminals services are ISO 9001 quality system proven, with the
                capability to manage strategically important products and provide
                global distribution solutions. Our resources are based in Texas, but
                we have affiliated Sales offices and facilities in Singapore, Hong
                Kong, London and the newest terminal facilities in Doha, Qatar. Since
                all of these resources and affiliates have their accompanying quality
                systems in place, we confidently serve all our customers with the
                highest standards, and aim to exceed your expectations.</p>

            <p>Our
                family of Third Coast Terminals Services include:</p>
            <ul>
                <li><p><a href="contract-manufacturing-and-packaging.php">Contract
                        Chemical Manufacturing</a>
                    </p>
                </li>
                <li><p><a href="toll-blenders.php">Toll
                        Blending</a> and <a href="reaction-chemistry.php">Reactive Chemistry Processes</a></p>
                </li>
                <li><p><a href="filtration-services.php">Filtration</a></p>
                </li>
                <li><p><a href="trans-loading-services.php">Transloading</a>
                        to and from <a href="railcar-loading-and-unloading.php">Railcars</a>, <a href="iso-9001-quality-system.php">ISO tanks</a>, Flexitanks, and Bulk Trucks</p>
                </li>
                <li><p>Blend
                        capabilities from one drum up to 90,000 gallons.
                    </p>
                </li>
                <li><p>High
                        purity product handling and packaging that meets USP, <a href="cgmp-haccp.php">FDA</a>, <a href="food-product-packaging.php">Kosher,
                        and Halal requirements</a></p>
                </li>
                <li><p><a href="industrial-packaging.php">Industrial
                        Product Packaging</a>
                    </p>
                </li>
                <li><p><a href="hazardous-materials-packaging.php">Flammable
                        and Combustible Material Handling and Packaging</a></p>
                </li>
                <li><p><a href="railcar-loading-and-unloading.php">Rail
                        storage</a> to manage your inbound and outbound railcar inventory</p>
                </li>
                <li><p>Packaged
                        Goods <a href="chemicals-warehousing.php">Warehousing and Distribution</a></p>
                </li>
                <li><p>Containerization</p>
                </li>
                <li><p>Accredited
                        <a href="quality-assurance-testing.php">Laboratory and Analytical Services</a></p>
                </li>
                <li><p>Product
                        Development and Modeling</p>
                </li>
                <li><p>Flexible
                        operating hours</p>
                </li>
            </ul>
        </div>

        <div class="col-lg-6">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/service-overview-1.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-2.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-3.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-4.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-5.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-6.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-7.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-8.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-9.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-10.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-11.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-12.jpg" title="Services Overview" alt="Services Overview">
                </div>

                <div class="item">
                  <img src="images/service-overview-13.jpg" title="Services Overview" alt="Services Overview">
                </div>




            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
