<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third Coast is ready to handle a wide range of food-grade products. We have a vast array of experience in packaging products while adhering to strict “Good Manufacturing Practices” and FDA standards.</p>

            <p>Our team is intimately familiar with HACCP requirements and we stand ready to partner with you regarding your packaging needs. </p>

            <p>You can easily reach us here, <a href="contact-third-coast-terminals.php">Contact Us</a> for an introductory inquiry.</p>

           

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                	
                	<div class="item active">
                        <img src="images/cgmp-haccp.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>
                    
                    <div class="item">
                        <img src="images/cgmp.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/haccp.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>

                    
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div><!--/col-lg-3-->


</div>


<!--========================================================
FOOTER
=========================================================-->
<?php include "footer.php" ?>



