<?php include "header.php" ?>


<div class="container contact">

    <div class="mtb-25">
        <div class="col-lg-6">

            <div class="headline">
                <h2>
                    Third Coast Terminals
                </h2>
            </div>
            <br>

            <p>
                When you call Third Coast Terminals, you'll find a friendly and helpful person who can answer your
                questions
                quickly or direct you to the best person on our staff to help you further.

            </p>

            <div style="float: left;"><p><strong>Main Location: </strong></p>
            
                        <p>1871 Mykawa Rd. <br> Pearland, TX 77581</p></div>
            


            <div style="float: right;"><p><strong>Support Location: </strong></p>
                        <p>18410 Dace Rd. <br> Alvin, Texas 77511</p></div>

            <!-- Google Map -->
            <div id="map" class="map map-box map-box-space1 margin-bottom-40">

                <iframe
                    src="https://www.google.com/maps/d/embed?mid=1tHQz4Z3oMCsouihXUhx3kaJsFCu7G1W8&hl=en"
                    width="600" height="450" frameborder="0" style="border:0;width: 100%;height: 260px"
                    allowfullscreen></iframe>
            </div><!---/map-->

            <div class="clearfix mt-5"></div>


            <div class="col-lg-6 pl-0">
                <ul class="list-unstyled">
                    <li><span><strong>Phone:</strong>  </span>(281) 412-0275</li>
                    <li><span> <strong>Toll Free:</strong></span> (877) 412-0275</li>
                    <li><span> <strong>Fax:</strong></span> 281.412.0275
                    </li>

                </ul>

            </div>


        </div><!--/col-lg-3-->

        <div class="col-lg-6 contact-form">
            <div class="headline">
                <h2>
                    Send Us a Message
                </h2>
            </div>
            <br>

            <form action="" method="post"
                  class=" contact-style">
                <fieldset class="no-padding">

                    <input type="text" name="full_name" id="name" class="form-control" placeholder="Full Name *">
                    <input type="text" name="phone" id="name" class="form-control" placeholder="Phone *"> <input
                        type="text" name="email" id="email" class="form-control" placeholder="Email *">
                    <input type="text" name="company" id="email" class="form-control" placeholder="Company Name *">
                    <textarea class="form-control" name="message" placeholder="Message..."></textarea>

                    <p>
                        <button type="submit" class="btn  btn-primary btn-blue btn-contact">Send Message</button>
                    </p>
                </fieldset>

                <div class="message hidden">
                    <i class="rounded-x fa fa-check"></i>

                    <p>Your message was successfully sent!</p>
                </div>
            </form>
        </div>
        <div class="clearfix mt-5"></div>
    </div>
    <div class="clearfix mt-5"></div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



