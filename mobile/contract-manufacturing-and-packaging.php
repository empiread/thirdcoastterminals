<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
            <p>
                Contract Manufacturing is the dedicated, custom production of products using a customer's trade
                knowledge and intellectual property to manufacture products
                for them and their end customers. We work with you every step of the way to select raw materials and
                tailor manufacturing to meet your exact specifications
                on time, every time. Our dedicated team of commercial, technical, process, operations, design, maintenance and supply
                chain personnel work hand in hand with your experts
                to ensure that products arrive at your end customer's site safely, reliably, on time, profitably and as
                seamlessly as if they had been produced in your own
                plant.
            </p>

            <p align="justify">
                <strong>Step 1 : Expression of Interest</strong>
            </p>

            <p align="justify">
                The journey towards your success story begins with an expression of interest in manufacturing your product.
                You can easily reach us here for an introductory
                inquiry. <a href="contact-third-coast-terminals.php">Contact Us</a>.

            <p align="justify">
                <strong>Step 2 : Information Exchange</strong>
            </p>

            <p align="justify">
                After the initial contact, a meeting between you and the team can be arranged. In the meeting, a broad
                overview of the potential toll manufacturing project
                can be shared with our operational, technical and commercial functions. We will also share with you a
                general overview of the plant's successful history and
                diverse set of manufacturing capabilities.
            </p>

            <p align="justify">
                <strong>Step 3 : Joint Non Disclosure Execution</strong>
            </p>

            <p align="justify">
                We are committed to ensuring your IP remains exclusively yours. In this respect, a joint non disclosure
                agreement will be executed between you and TCT
                before any IP is exchanged. This ensures the protection of your proprietary information, and ours.
            </p>

            <p align="justify">
                <strong>Step 4 : Detailed Discussions</strong>
            </p>

            <p align="justify">
                Once the non disclosure agreement has been executed, detailed discussion on the toll manufacturing
                project will commence. The discussions will center
                around manufacturing procedures, engineering designs and modifications, cost and contractual terms, logistical arrangements,
                quality assurance, and safety evaluations and manufacturing
                procedures.
            </p>

            <p align="justify">
                <strong>Step 5 : Project Execution</strong>
            </p>

            <p align="justify">
                The culmination of the journey lies in the final project execution. At this last stage, you will witness
                your raw materials being transformed into on
                specification product in the most safe, timely and efficient manner.
            </p>

        </div>
        <div class="col-lg-6">

            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                       <img src="images/chemical-blending.jpg" title="Chemical Blending" alt="Chemical Blending">
                    </div>

                    <div class="item">
                        <img src="images/chemical-blending2.jpg" title="Chemical Blending" alt="Chemical Blending">
                    </div>

                    <div class="item">
                        <img src="images/contract-manufacturing-and-packaging.jpg" title="Contract Manufacturing and
                             Packaging"
                             alt="Contract Manufacturing and Packaging">
                    </div>

                    <div class="item">
                        <img src="assets/images/complete-reaction-services.jpg" title="Contract Manufacturing"
                             alt="Contract Manufacturing">
                    </div>

                    <div class="item">
                        <img src="assets/images/complete-reaction-services-2.jpg" title="Contract Manufacturing"
                             alt="Contract Manufacturing">
                    </div>

                    <div class="item">
                        <img src="assets/images/reaction-expertise.jpg" title="Contract Manufacturing"
                             alt="Contract Manufacturing">
                    </div>

                    <div class="item">
                        <img src="assets/images/reaction-operation-area.jpg" title="Contract Manufacturing"
                             alt="Contract Manufacturing">
                    </div>


                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>


            </div>

        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
