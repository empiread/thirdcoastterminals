<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

			<h3 dir="ltr"><span>Environmental</span></h3>
			
            <p dir="ltr"><span>Third Coast Terminals is deeply committed to the prevention of environmental pollution, conservation of resources and to observe all applicable legal and moral requirements that apply to all our work operations. </span>
            </p>

            <p dir="ltr"><span>We work closely with both our suppliers and customers to eliminate product waste streams. All line flushes are accumulated and returned to customers or sold at customer request to salvagers for refinement and re-use.</span></p>

            <p dir="ltr"><span>We are actively recycling and utilizing recycled materials (where available) across our business processes.</span>
            </p>
			<h3 dir="ltr"><span>Health</span></h3>
            <p dir="ltr"><span>Our primary concern is for the well-being of our team members. This has led us to introduce a voluntary Wellness Program, at no cost to our team members, with annual healthy physical screening and annual flu inoculations.</span>
            </p>

            <p dir="ltr"><span>We are a non-smoking facility, believing that more deterrents to smoking at work aids team members in their efforts to quit while improving the environment for non-smokers and reductions in fire related accidents.</span>
            </p>
			<h3 dir="ltr"><span>Safety</span></h3>
            <p dir="ltr"><span>The safety of our team members is extremely important to us both at work and at home. Toolbox safety topics are not limited to at-work issues as there are hazards in all aspects of life. Safety training does not stop for us when our team members leave our gates. Safety education and the resulting heightened awareness are principles we train and encourage our team members to follow every day.</span>
            </p>
			<h3 dir="ltr"><span>Emergency Preparedness</span></h3>
            <p dir="ltr"><span>Third Coast Terminals recognizes that our local area is periodically exposed to violent storms and other events. TCT has prepared our organization for rapid response, recuperation, community assistance and business continuity.</span>
            </p>

            

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <div class="item active">
                        <img src="images/environmental1.jpg" title="Environmental" alt="Environmental">

                    </div>

                    <div class="item">
                        <img src="images/environmental2.jpg" title="Environmental" alt="Environmental">

                    </div>

                    <div class="item">

                        <img src="images/environmental3.jpg" title="Environmental" alt="Environmental">

                    </div>


                    <div class="item">
                        <img src="images/environmental4.jpg" title="Environmental" alt="Environmental">
                    </div>


                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>

    </div>


</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



