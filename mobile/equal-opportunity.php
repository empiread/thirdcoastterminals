<?php include "header.php" ?>


<div class="container ">
	
	
	
	<div class="mtb-25">
        <div class="col-xs-12">
        	
        	
        		<p>Third Coast is an equal employment opportunity corporation and will adhere to all federal, state, and local employment rules, regulations, and laws including the following:</p>
        		<p>Third Coast prohibits job discrimination because of religious opinions or affiliations or on the basis of race, color, age, sex, national origin and/or disability.</p>
        	
            <img src="images/equal-opportunity.jpg" title="Equal Opportunity Employer" alt="Equal Opportunity Employer">
            <p></p>
        </div>

        
    </div>



</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
