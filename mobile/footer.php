<footer>
    <div class="container">
        <div class="col-lg-9 pl-0">
            <div class="col-lg-4 pl-0">
                <div class="title"><h4>Address</h4></div>
                <div class="desc">
                    <p><a href="https://www.google.com/maps?ll=29.573049,-95.294801&z=15&t=m&hl=en-US&gl=IN&mapclient=embed&q=1871+Mykawa+Rd+Pearland,+TX+77581+USA">1871 Mykawa Rd. Pearland, TX 77581</a></p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="title"><h4>Toll Free</h4></div>
                <div class="desc">
                    <p><a href="tel://877.412.0275" class="adesc">877.412.0275</a></p>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="title"><h4>Fax</h4></div>
                <div class="desc">
                    <p>281.412.0275 </p>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="title"><h4>Email</h4></div>
                <div class="desc">
                    <p><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></p>
                </div>
            </div>

            <div class="col-lg-3 mobile-social hidden-lg">
                <div class="clearfix"></div>
                <div class="title"><h4>Follow us</h4></div>
                <div class="">
                   <a href="https://www.facebook.com/ThirdCoastTerminals/" target="_blank"><i class="fa fa-facebook"></i></a>
                   <a href="https://twitter.com/TCTerminals" target="_blank"><i class="ion-social-twitter"></i></a>
                   <a href="https://www.youtube.com/user/thirdcoastint" target="_blank"><i class="fa fa-youtube"></i></a>

                </div>
            </div>
        </div>
        <div class="col-lg-2 pull-right hidden-xs hidden-md">
            <div class="social-last"><h4>Follow us</h4></div>
            <div>
                <ul class="list-inline list-unstyled social pull-right">

                    <li><a href="https://twitter.com/ThirdCoastint"><i class="ion-social-twitter"></i></a></li>

                    <li><a href="https://plus.google.com/111648529809799884969"><i
                                class="ion-social-googleplus"></i></a></li>
                    <li><a href="https://www.youtube.com/user/thirdcoastint"><i class="fa fa-youtube"></i></a></li>

                </ul>
            </div>
        </div>

        <div class="clearfix"></div>
        <hr>

        <div class="col-lg-6 pl-0" id="desktop">
            <div class="copy-right">
                Copyright © <script>document.write(new Date().getFullYear())</script> Third Coast Terminals. All Rights Reserved.
            </div>

            <div class="copy-right  hidden-lg">
                <span class="switch switch-mobile"><a href="http://thirdcoastterminals.com/?viewFullSite=true">Show Full site</a></span> <a target="_blank"
                                                                            href="http://petropages.com/">
                   | Site design by
                    PetroPages</a>
            </div>

        </div>


    </div>

</footer>
<script src="assets/jquery/dist/jquery.js"></script>
<script src="assets/script.js"></script>
<script src="assets/jquery.mobile.custom.min.js"></script>
 <script>
$(document).ready(function(){
	
$('#myCarousel').carousel({
  interval: 2000
});

$("#myCarousel").swiperight(function() {
      $(this).carousel('prev');
    });
   $("#myCarousel").swipeleft(function() {
      $(this).carousel('next');
   });
});
</script>


<?php include "analyticstracking.php" ?>
</body>
</html>
