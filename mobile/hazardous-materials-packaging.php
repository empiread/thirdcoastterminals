<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>
                In
                addition to traditional fill lines, Third Coast has added a “Feige”
                Robotic Filling line. This is a “State-of-the-art” fully
                automated filling station. This filler is enclosed in a stainless
                steel housing with automatic sliding doors (right and left), for in
                and outbound pallet movements. Attached to this new filling station,
                is a negative pressure thermal oxidizer and scrubbing system.</p>

             <h3 dir="ltr"><span>Technology Offers:</span></h3>
			<ul>
            <li>Drum
                or tote filling for hazardous materials</li>

            <li>Consistently
                accurate fill weights</li>

            <li>No
                human exposure / intervention</li>
			</ul>
            <p></p>

            </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                	
                	<div class="item active">
                        <img src="images/hazardous-materials-packaging.png" title="Toll Blenders" alt="Toll Blenders">
                    </div>
                    
                    <div class="item">
                        <img src="images/automated-filling-station.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/vapor-controlled-materials.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>

                    
                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



