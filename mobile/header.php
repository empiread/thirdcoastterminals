<?php
$current = basename($_SERVER['PHP_SELF']);
$pages = array(
    /*array(
        "title" => 'About',
        "url" => 'gulf-coast-chemical-industry.php',
        "seo-title" => 'Gulf Coast Chemical Industry | Reactive Chemistry | Blending - Third Coast Terminals',
        "keywords" => 'Gulf Coast Chemical Industry,Reactive Chemistry,Blending',
        "desc" => 'Third Coast Terminals is a custom manufacturer and manager of petrochemicals.',
        "h1" => 'Petrochemical & Fine Chemicals Manufacturing and Handling Experts',
        'right' => '-30',
        "sub" => array()),*/
    
    array(
        "title" => 'Contract  Manufacturing',
        "url" => 'contract-manufacturing-and-packaging.php',
        "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
        "keywords" => 'Contract Manufacturing and Packaging,Chem',
        "desc" => '',
        "mobileBg" => 'assets/images/header-contract-manufacturing-m.jpg',
        'right' => '-20',
        "sub" => array(
        
            array(
                "title" => 'Contract Manufacturing',
                "url" => 'contract-manufacturing-and-packaging.php',
                "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
                "keywords" => 'Contract Manufacturing and Packaging,Chem',
                "desc" => '',
                "h1" => 'Contract Manufacturing and Packaging',
                "mobileBg" => 'assets/images/header-contract-manufacturing-m.jpg',
            ),
            array(
                "title" => 'Toll Blenders',
                "url" => 'toll-blenders.php',
                "seo-title" => 'Chem Toll Blenders | Toll Manufacturing - Third Coast Terminals',
                "keywords" => 'Toll Blenders,Toll Manufacturing',
                "desc" => 'Our toll manufacturing process enables us to blend our customers basic to complex chemical formulations into their final product.',
                "h1" => 'Toll Blending',
                "mobileBg" => 'assets/images/header-contract-manufacturing-m.jpg',
            ),
            array(
                "title" => 'Reaction Chemistry',
                "url" => 'reaction-chemistry.php',
                "seo-title" => 'Reaction Chemistry | Urethane | Prepolymers | Toll - Third Coast Terminals',
                "keywords" => 'Reaction Chemistry,Urethane,Prepolymers,Toll',
                "desc" => 'Third Coast offers reaction chemistry currently majoring on polyurethane prepolymers.',
                "h1" => 'Reaction Chemistry',
                "mobileBg" => 'assets/images/header-contract-manufacturing-m.jpg',
            ),
            array(
                "title" => 'Filtration',
                "url" => 'filtration-services.php',
                "seo-title" => 'Filtration Services - Third Coast Terminals',
                "keywords" => 'Filtration Services',
                "desc" => 'Third Coast Terminals offers a variety of filtration capabilities.',
                "h1" => 'Filtration Services',
                "mobileBg" => 'assets/images/header-contract-manufacturing-m.jpg',
            )
        )
    ),
    array(
        "title" => 'Packaging Services',
        "url" => 'industrial-packaging.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "h1" => '',
        "mobileBg" => 'assets/images/packaging-banner.jpg',
        "sub" =>
            array(

                array(
                    "title" => 'Industrial Packaging',
                    "url" => 'industrial-packaging.php',
                    "seo-title" => 'Quality Management | Quality System | ISO 9001 | Certification - Third Coast Terminals',
                    "keywords" => 'Quality Management,Quality System,ISO 9001,Certification',
                    "desc" => 'The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries.',
                    'h1' => 'Industrial Packaging',
                    "mobileBg" => 'assets/images/packaging-banner.jpg',
                ),
                array(
                    "title" => 'CGMP-HAACP',
                    "url" => 'cgmp-haccp.php',
                    "seo-title" => 'Quality Management | Quality System | ISO 9001 | Certification - Third Coast Terminals',
                    "keywords" => 'Quality Management,Quality System,ISO 9001,Certification',
                    "desc" => 'The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries.',
                    'h1' => 'CGMP-HAACP',
                    "mobileBg" => 'assets/images/packaging-banner.jpg',
                ),
                array(
                    "title" => 'Halal/Kosher Packaging',
                    "url" => 'food-product-packaging.php',
                    "seo-title" => 'Food Product Packaging | Kosher Packaging - Third Coast Terminals',
                    "keywords" => 'Certification',
                    "desc" => 'Maintaining industry standard safety & compliance Certifications & Approvals.',
                    "h1" => 'Food Product Packaging',
                    "mobileBg" => 'assets/images/packaging-banner.jpg',
                ),
                array(
                    "title" => 'Hazardous Packaging',
                    "url" => 'hazardous-materials-packaging.php',
                    "seo-title" => 'Responsible Care Guiding Principles - Third Coast Terminals',
                    "keywords" => 'Responsible Care Guiding Principles',
                    "desc" => 'Third Coast Terminals is proud to participate in the American Chemistry Council Responsible Care initiative.',
                    'h1' => 'Hazardous Packaging', 
                    "mobileBg" => 'assets/images/packaging-banner.jpg',

                ),
                
            )

    ),
    array(
        "title" => 'Logistics',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-32',
        //'divmenu'=>'1',
        "mobileBg" => 'assets/images/header-logistics-m.jpg',
        "sub" =>
            array(
                array(
                    "title" => 'Logistics',
                    "url" => 'logistics.php',
                    "seo-title" => 'logistics - Third Coast Terminals',
                    "keywords" => 'Logistics Services',
                    "desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.',
                    "h1" => 'Logistics Services Services',
                    "mobileBg" => 'assets/images/header-logistics-m.jpg',
                ),
                array(
                    "title" => 'Terminal',
                    "url" => 'tank-terminal-services.php',
                    "seo-title" => 'Tank Terminal Services - Third Coast Terminals',
                    "keywords" => 'Tank Terminal Services',
                    "desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.',
                    "h1" => 'Tank Terminal Services',
                    "mobileBg" => 'assets/images/header-logistics-m.jpg',
                ),
                array(
                    "title" => 'Rail Services',
                    "url" => 'railcar-loading-and-unloading.php',
                    "seo-title" => 'Railcar Loading | Railcar Unloading - Third Coast Terminals',
                    "keywords" => 'Railcar Loading,Railcar Unloading',
                    "desc" => '',
                    "h1" => 'Rail Services',
                    "mobileBg" => 'assets/images/header-logistics-m.jpg',
                ),
               array(
                    "title" => 'Warehousing & Distribution',
                    "url" => 'chemicals-warehousing.php',
                    "seo-title" => 'Chemicals Warehousing - Third Coast Terminals',
                    "keywords" => 'Chemicals Warehousing',
                    "desc" => 'One half of our warehouse is equipped to handle flammable and other hazardous materials.'
                , "h1" => 'Chemicals Warehousing',
                    "mobileBg" => 'assets/images/header-logistics-m.jpg',
                ),
                array(
                    "title" => 'Trailer Storage',
                    "url" => 'trailer-storage.php',
                    "seo-title" => 'Trailer storage | Container Storage | Drop and Swap - Third Coast Terminals',
                    "keywords" => 'Trailer Storage,Container Storage,Drop and Swap',
                    "desc" => 'Trailer storage is offered for our blending, reaction chemistry, trans-loading and packaging customers.'
                , "h1" => 'Trailer Storage',
                    "mobileBg" => 'assets/images/header-logistics-m.jpg',
                ),
            )

    ),
    array(
        "title" => ' Lab Research &  Development',
        "url" => 'laboratory-services.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'h1' => 'Laboratory Services',
        'right' => '-14',
        "mobileBg" => 'assets/images/header-lab-m.jpg',
        "sub" => array(
		 array(
                    "title" => 'Services',
                    "url" => 'quality-assurance-testing.php',
                    "seo-title" => 'Quality Control Testing - Third Coast Terminals',
                    "keywords" => 'Quality Control Testing',
                    "desc" => 'Third Coast Terminals has an extensive on-site laboratory, which can provide our customers with certified quality control and quality assurance testing for all manufactured products.'
                , "h1" => 'Quality Control Testing',
                    "mobileBg" => 'assets/images/header-lab-m.jpg',
                ),
                array(
                    "title" => 'R&D',
                    "url" => 'third-coast-research-and-development.php',
                    "seo-title" => 'Third Coast Research and Development - Third Coast Terminals',
                    "keywords" => 'Third Coast Research and Development',
                    "desc" => 'Third Coast Terminals offers a variety of technical services.'
                , "h1" => 'Research & Development',
                    "mobileBg" => 'assets/images/header-lab-m.jpg',
                ),
		 
		)

    ),
    array(
        "title" => 'Quality',
        "url" => 'iso-9001-quality-system.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "h1" => '',
        "mobileBg" => 'assets/images/header-quality-m.jpg',
        "sub" =>
            array(

                array(
                    "title" => 'ISO Quality Management',
                    "url" => 'iso-9001-quality-system.php',
                    "seo-title" => 'Quality Management | Quality System | ISO 9001 | Certification - Third Coast Terminals',
                    "keywords" => 'Quality Management,Quality System,ISO 9001,Certification',
                    "desc" => 'The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries.',
                    'h1' => 'ISO Quality Management',
                    "mobileBg" => 'assets/images/header-quality-m.jpg',
                ),
                array(
                    "title" => 'Certifications',
                    "url" => 'third-coast-terminals-certifications.php',
                    "seo-title" => 'Certification - Third Coast Terminals',
                    "keywords" => 'Certification',
                    "desc" => 'Maintaining industry standard safety & compliance Certifications & Approvals.',
                    "h1" => 'Certifications',
                    "mobileBg" => 'assets/images/header-quality-m.jpg',
                ),
                array(
                    "title" => 'Permits',
                    "url" => 'third-coast-terminals-permits.php',
                    "seo-title" => 'Third Coast Terminals Permits - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Permits',
                    "desc" => 'Permits currently held by Third Coast Terminals.',
                    "h1" => "Permits",
                    "mobileBg" => 'assets/images/header-quality-m.jpg',
                ),
                array(
                    "title" => 'Responsible Care',
                    "url" => 'responsible-care-guiding-principles.php',
                    "seo-title" => 'Responsible Care Guiding Principles - Third Coast Terminals',
                    "keywords" => 'Responsible Care Guiding Principles',
                    "desc" => 'Third Coast Terminals is proud to participate in the American Chemistry Council Responsible Care initiative.',
                    'h1' => 'Responsible Care Guiding Principles',
                    "mobileBg" => 'assets/images/header-quality-m.jpg',

                ),
                array(
                    "title" => 'Responsible Distribution',
                    "url" => 'responsible-distribution.php',
                    "seo-title" => 'NACD | Responsible Distribution | Certification - Third Coast Terminals',
                    "keywords" => 'NACD,Responsible Distribution,Certification',
                    "desc" => 'Responsible Distribution is a third-party verification environmental, health, safety & security program through which members demonstrate their commitment to continuous performance improvement.',
                    "h1" => 'Responsible Distribution',
                    "mobileBg" => 'assets/images/header-quality-m.jpg',
                ),
            )

    ),
    array(
        "title" => '  EHS&S',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-52',
        "mobileBg" => 'assets/images/environmental-banner.jpg',
        "sub" =>
            array(
                array(
                    "title" => 'Environmental',
                    "url" => 'environmental.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Environmental Services',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
                array(
                    "title" => 'Health',
                    "url" => 'health.php',
                    "seo-title" => 'Health - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => '',
                    "h1" => 'Health',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
                array(
                    "title" => 'Safety',
                    "url" => 'safety.php',
                    "seo-title" => 'Safety - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => '',
                    "h1" => 'Safety',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
                array(
                    "title" => 'Security',
                    "url" => 'security.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Security',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
                array(
                    "title" => 'Emergency Preparedness',
                    "url" => 'emergency-preparedness.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Emergency Preparedness',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
                array(
                    "title" => 'Visitor Orientation',
                    "url" => 'visitor-orientation.php',
                    "seo-title" => 'Visitor Orientation - Third Coast Terminals',
                    "keywords" => 'Visitor Orientation',
                    "desc" => 'Please download our visitor orientation quiz.',
                    "h1" => 'Visitor Orientation',
                    "mobileBg" => 'assets/images/environmental-banner.jpg',
                ),
            )
    ),
    array(
        "title" => '  Human Resources',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-52',
        "mobileBg" => 'assets/images/header-hr-m.jpg',
        "sub" =>
            array(
                array(
                    "title" => 'Human Resources',
                    "url" => 'human-resources.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Human Resources',
                    "mobileBg" => 'assets/images/header-hr-m.jpg',
                ),
                array(
                    "title" => 'Apply Now',
                    "url" => 'apply.php',
                    "seo-title" => 'Apply Now - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => 'Current Opportunities for employment at Third Coast Terminals.',
                    "h1" => 'Apply Now',
                    "mobileBg" => 'assets/images/header-hr-m.jpg',
                ),
                array(
                    "title" => 'TCT Cares',
                    "url" => 'tct-cares.php',
                    "seo-title" => 'TCT Cares - Third Coast Terminals',
                    "keywords" => 'TCT Cares',
                    "desc" => 'An overview of Third Coast Terminals activity in the community.',
                    "h1" => 'TCT Cares',
                    "mobileBg" => 'assets/images/header-hr-m.jpg',
                ),
                array(
                    "title" => 'Benefits',
                    "url" => 'benefits.php',
                    "seo-title" => 'Health - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => '',
                    "h1" => 'Benefits',
                    "mobileBg" => 'assets/images/header-hr-m.jpg',
                ),
                array(
                    "title" => 'Equal Opportunities',
                    "url" => 'equal-opportunity.php',
                    "seo-title" => 'Safety - Third Coast Terminals',
                    "keywords" => '',
                    "desc" => '',
                    "h1" => 'Equal Opportunty',
                    "mobileBg" => 'assets/images/header-hr-m.jpg',
                ),
                array(
                    "title" => 'Photo Gallery',
                    "url" => 'photo-gallery.php',
                    "seo-title" => 'Photo Gallery - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Photo Gallery highlighting the events from Third Coast Terminals in the community.',
                    "h1" => 'Photo Gallery', 
                    "mobileBg" => 'assets/images/header-hr-m.jpg',   
                ),
            )
    ),
    array(
        "title" => 'About Us',
        "url" => 'history.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-25',
        'h1' => '',
        "mobileBg" => 'assets/images/header-about-m.jpg',
        "sub" => array(
        
			array(
        			"title" => 'About Us',
        			"url" => 'about-third-coast-terminals.php',
        			"seo-title" => 'About Third Coast Terminals - Third Coast Terminals',
        			"keywords" => 'About Third Coast Terminals',
        			"desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
        			"h1" => 'About Third Coast Terminals',
        			"mobileBg" => 'assets/images/header-about-m.jpg',
        			"sub" => array()
    ),
        	array(
                    "title" => 'History',
                    "url" => 'history.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'History',
                    "mobileBg" => 'assets/images/header-about-m.jpg',
                ),
                
             array(
        			"title" => 'Service Overview',
        			"url" => 'bulk-liquid-handling.php',
        			"seo-title" => 'Bulk Liquid Handling - Third Coast Terminals',
        			"keywords" => 'Bulk Liquid Handling',
        			"desc" => 'Third Coast Terminals is a veteran provider of Bulk Liquid Handling Services for the companies that make up the worldwide Petrochemical, Fine Chemical, Food and Pharmaceutical Industries.',
        			"h1" => 'Service Overview',
        			"mobileBg" => 'assets/images/header-about-m.jpg',
        			"sub" => array()
    ),
    		array(
        			"title" => 'Press Releases',
        			"url" => 'press-releases.php',
        			"seo-title" => 'Press Releases - Third Coast Terminals',
        			"keywords" => '',
        			"desc" => '',
        			"h1" => 'Press Releases',
        			"mobileBg" => 'assets/images/header-about-m.jpg',
        			"sub" => array()
    ),
         )
    ),
    
 
    array(
        "title" => '  Contact Us', 
        "url" => 'contact-third-coast-terminals.php',
        "seo-title" => 'Contact TCT - Third Coast Terminals',
        "keywords" => '',
        "desc" => '',
        'right' => '-25',
        'h1' => 'Contact Us',
        "mobileBg" => 'assets/images/header-contact-m.jpg',
        "sub" => array()
    )
);

// current page value
$currentPage = false;
$currentTopPage = false;
$currentInnerPage = false;
foreach ($pages as $page) {
    if ($page['url'] == $current) {
        $currentPage = $page;
        $img = $page["mobileBg"];
    }
    if (isset($page['sub'])) {
        foreach ($page['sub'] as $sub) {
            if ($sub['url'] == $current) {
                $currentPage = $sub;
                $currentTopPage = $page;
                $currentInnerPage = $sub;
                $img = $page["mobileBg"];
            }
        }
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <?php if (!$_SESSION['desktopmode']) { ?>

    <?php } ?>

    <meta name="viewport" content="width=device-width, initial-scale=1"/>


    <?php if ($current == 'index.php') { ?>
        <title> Third Coast Terminals | Contract Manufacturing | Reactive Chemistry - Third Coast Terminals</title>
        <meta name="keywords" content="Third Coast Terminals,Contract Manufacturing,Reactive Chemistry">
        <meta name="description"
              content="Third Coast Terminals was established in 1998 and we have quickly grown to be recognized as one of the best supply chain partners.">
    <?php } else { ?>
        <title><?= $currentPage['seo-title'] ?></title>
        <meta name="keywords" content="<?= $currentPage['keywords'] ?>">
        <meta name="description" content="<?= $currentPage['desc'] ?>">
    <?php } ?>
    <meta name="author" content="www.petropages.com/creative/">
    <meta name="geo.region" content="US-TX"/>
    <meta name="geo.placename" content="Pearland"/>
    <meta name="geo.position" content="29.573049;-95.294782"/>
    <meta name="ICBM" content="29.573049, -95.294782"/>

    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">


    <link href="assets/style.css" rel="stylesheet">
    <style>
    	.ui-loader{
    		 display:none;
    		  }
    </style>


</head>
<body>
<div id="wrapper">
    <!--========================================================
                              HEADER
    =========================================================-->
    <header class="<?= $current == 'index.php' ? '' : 'inner'; ?>">
        <div class="top-bar hidden-md hidden-xs">
            <div class="container ">
                <div class="clearfix">
                    <div class="leftside col-lg-4">
                        <div>1871 Mykawa Rd, Pearland, TX 77581</div>
                    </div>

                    <div class="rightside col-lg-8 ">
                        <div>

                            <ul class="list-inline list-unstyled pull-right">
                                <li><span>Toll Free </span><a href="tel://877.412.0275">877.412.0275</a></li>
                                <li><span> Fax  </span> 281.412.0245</li>
                                <li><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></li>
                            </ul>
                        </div>

                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>

        </div>

        <div class="mobile-top-bar hidden-lg">
            <div class="container mobile-top">
                <div class="col-md-6 right-border col-xs-6 s">
                    <span><a href="tel://877.412.0275">877.412.0275</a></span>
                </div>
                <div class="col-md-6 col-xs-6 s">
                    <span><a href="mailto:sales@3cterminals.com">sales@3cterminals.com</a></span>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="menu">
            <div class="container">
                <?php include "nav.php" ?>
            </div>
        </div>

    </header>


    <?php if ($current == 'index.php') { ?>
        <div class="bg-black">
            <section class="full" id="slider">

                <div
                    id="header-video"
                    class="full-height black-wrapper video"
                    data-vide-bg="poster: images/mobile-header.jpg"
                    data-vide-options="position: 0% 10%">
                    <!-- replace the above options with your own -->

                    <div class="text-center slider-text">
                        <!-- multiple h1's are perfectly acceptable on a page in valid HTML5, when wrapped in individual sections, they're 100% semantically correct -->

                        <div class="title">A world-Class</div>
                        <div class="title">Chemical Service Company</div>
                        <div class="sub">Providing customized <br> solutions</div>


                        <a href="about-third-coast-terminals.php"
                           class="btn btn-white btn-primary btn-white slider-btn">
                            Learn More </a>


                    </div>

                </div>

            </section>
        </div>
    <?php } else {

        if ($currentInnerPage) {
            // $img = "assets/images/banner2_02.jpg";
			if ($current == 'photo-gallery.php') {
				echo '<section id="inner-slider">';
			}else{
			 echo '<section id="inner-slider" style="background: url('.$img.')">';	
			}
            ?>
            


                <div class="">
                    <div class="inner-header">

                        <nav class="inner-nav hidden-md hidden-xs">
                            <ul class="nav nav-tabs">

                                <?php foreach ($currentTopPage['sub'] as $item) { ?>
                                    <li class="<?= $current == $item['url'] ? 'active' : ''; ?>"><a
                                            href="<?= $item['url'] ?>"><?= $item['title'] ?></a>
                                    </li>

                                <?php } ?>
                            </ul>
                        </nav>
                        <div class="page-title">
                            <h1><?= $currentInnerPage['h1'] ?></h1>
                        </div>

                    </div>

                </div>

            </section>
        <?php } else {
			
			?>
			<section id="inner-slider" style="background: url(<?= $img ?>)">
                <div class="">
                    <div class="inner-header">
                        <nav class="inner-nav hidden-md hidden-xs">
                            <ul class="nav nav-tabs">
                            </ul>
                        </nav>
                        <div class="page-title">
                            <h1><?= $currentPage['h1'] ?></h1>
                        </div>
                    </div>
                </div>
            </section>
        <?php } ?>
    <?php } ?>
    <div id="content" class="clearfix"></div>
