<?php

$current = basename($_SERVER['PHP_SELF']);
$pages = array(
    array(
        "title" => 'Contract <br> Manufacturing<br>  Services',
        "url" => 'contract-manufacturing-and-packaging.php',
        "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
        "keywords" => 'Contract Manufacturing and Packaging,Chem',
        "desc" => '',
        'right' => '-20',
        "sub" => array(
            array(
                "title" => 'Contract Manufacturing',
                "url" => 'contract-manufacturing-and-packaging.php',
                "seo-title" => 'Contract Manufacturing and Packaging | Chemical Blending - Third Coast Terminals',
                "keywords" => 'Contract Manufacturing and Packaging,Chem',
                "desc" => '',
                "h1" => 'Contract Manufacturing and Packaging',
            ),
            array(
                "title" => 'Toll Blenders',
                "url" => 'toll-blenders.php',
                "seo-title" => ' Toll Blenders | Toll Manufacturing - Third Coast Terminals',
                "keywords" => 'Toll Blenders,Toll Manufacturing',
                "desc" => 'It’s a great idea to use our expertise, specialized equipment, and resources to complete your Toll Manufacturing.',
                "h1" => 'Toll Blenders',
            ),
            array(
                "title" => 'Reaction Chemistry',
                "url" => 'reaction-chemistry.php',
                "seo-title" => 'Reaction Chemistry | Urethane | Prepolymers | Toll - Third Coast Terminals',
                "keywords" => 'Reaction Chemistry,Urethane,Prepolymers,Toll',
                "desc" => 'Third Coast offers reaction chemistry currently majoring on polyurethane prepolymers.',
                "h1" => 'Reaction Chemistry',
            ),
            array(
                "title" => 'Filtration',
                "url" => 'filtration-services.php',
                "seo-title" => 'Filtration Services - Third Coast Terminals',
                "keywords" => 'Filtration Services',
                "desc" => 'Third Coast Terminals offers a variety of filtration capabilities.',
                "h1" => 'Filtration Services',
            ),
            array(
                "title" => 'Services',
                "url" => 'bulk-liquid-handling.php',
                "seo-title" => 'Bulk Liquid Handling | Contract Chemical Manufacturing | Toll Blending - Third Coast Terminals',
                "keywords" => 'Bulk Liquid Handling,Contract Chemical Manufacturing,Toll Blending',
                "desc" => 'Third Coast Terminals is a veteran provider of Bulk Liquid Handling Services for the companies that make up the worldwide Petrochemical, Fine Chemical, Food and Pharmaceutical Industries'
            , "h1" => 'Bulk Liquid Handling',
            )
        )

    ),


    array(
        "title" => '<br>  Packaging<br> Services',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "sub" =>
            array(
                array(
                    "title" => 'Industrial Packaging',
                    "url" => 'industrial-chemicals-packaging.php',
                    "seo-title" => 'Chemicals Packaging - Third Coast Terminals',
                    "keywords" => 'Chemicals Packaging',
                    "desc" => 'Third Coast Terminals has six industrial chemical packaging lines for products to accommodate either drums or IBC’s.'
                , "h1" => 'Industrial Chemicals Packaging',
                ),
                array(
                    "title" => 'cGMP/HACCP',
                    "url" => 'cgmp-haccp.php',
                    "seo-title" => 'CGMP | HACCP - Third Coast Terminals',
                    "keywords" => 'CGMP,HACCP',
                    "desc" => 'Our team is intimately familiar with HACCP requirements and we stand ready to partner with you regarding your packaging needs.'
                , "h1" => 'cGMP/HAACP',
                ),
                array(
                    "title" => 'Religious Certified Packaging',
                    "url" => 'food-product-packaging.php',
                    "seo-title" => 'Food Product Packaging | Kosher Packaging | Halal Packaging - Third Coast Terminals',
                    "keywords" => 'Food Product Packaging,Kosher Packaging,Halal Packaging',
                    "desc" => 'Third Coast is equipped to handle a wide range of food-grade products from vegetable oil to pharmaceutical grade propylene glycols.'
                , "h1" => 'Food Product Packaging',
                ),
                array(
                    "title" => 'Hazardous',
                    "url" => 'hazardous-materials-packaging.php',
                    "seo-title" => 'Hazardous Materials Packaging - Third Coast Terminals',
                    "keywords" => 'Hazardous Materials Packaging',
                    "desc" => 'Drum or tote filling for hazardous materials.'
                , "h1" => 'Hazardous Materials Packaging',
                )
            )

    ),
    array(
        "title" => '<br>Supply  Chain<br>  Services',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-32',
        //'divmenu'=>'1',
        "sub" =>
            array(
                array(
                    "title" => 'Terminal',
                    "url" => 'tank-terminal-services.php',
                    "seo-title" => 'Tank Terminal Services - Third Coast Terminals',
                    "keywords" => 'Tank Terminal Services',
                    "desc" => 'Third Coast has an extensive terminal operation with more than 170+ tanks in current service.'
                , "h1" => 'Tank Terminal Services',
                ),
                array(
                    "title" => 'Rail Services',
                    "url" => 'railcar-loading-and-unloading.php',
                    "seo-title" => 'Railcar Loading | Railcar Unloading - Third Coast Terminals',
                    "keywords" => 'Railcar Loading,Railcar Unloading',
                    "desc" => ''
                , "h1" => '',
                ),
                array(
                    "title" => 'Transloads',
                    "url" => 'trans-loading-services.php',
                    "seo-title" => 'Trans-Loading Services - Third Coast Terminals',
                    "keywords" => 'Trans-Loading Services',
                    "desc" => 'Trans-loading with Third Coast will allow your company to gain the economic benefits of a rail service and the on-time, flexibility of a tank truck delivery.'
                , "h1" => 'Transloading Services',
                ),
                array(
                    "title" => 'Warehousing & Distribution',
                    "url" => 'chemicals-warehousing.php',
                    "seo-title" => 'Chemicals Warehousing - Third Coast Terminals',
                    "keywords" => 'Chemicals Warehousing',
                    "desc" => 'One half of our warehouse is equipped to handle flammable and other hazardous materials.'
                , "h1" => 'Chemicals Warehousing',
                ),
                array(
                    "title" => 'Trailer Storage',
                    "url" => 'trailer-storage.php',
                    "seo-title" => 'Trailer storage | Container Storage | Drop and Swap - Third Coast Terminals',
                    "keywords" => 'Trailer Storage,Container Storage,Drop and Swap',
                    "desc" => 'Trailer storage is offered for our blending, reaction chemistry, trans-loading and packaging customers.'
                , "h1" => 'Trailer Storage',
                ),
            )

    ),

    array(
        "title" => '<br> Lab Research & <br> Development',
        "url" => 'quality-assurance-testing.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-14',
        "sub" =>
            array(
                array(
                    "title" => 'Services',
                    "url" => 'quality-assurance-testing.php',
                    "seo-title" => 'Quality Assurance Testing | Quality Control Testing - Third Coast Terminals',
                    "keywords" => 'Quality Assurance Testing,Quality Control Testing',
                    "desc" => 'Third Coast Terminals has an extensive on-site laboratory, which can provide our customers with certified quality control and quality assurance testing for all manufactured products.'
                , "h1" => 'Quality Control Testing',
                ),
                array(
                    "title" => 'R&D',
                    "url" => 'third-coast-research-and-development.php',
                    "seo-title" => 'Third Coast Research & Development - Third Coast Terminals',
                    "keywords" => 'Third Coast Research & Development',
                    "desc" => 'Third Coast Terminals offers a variety of technical services.'
                , "h1" => 'Third Coast Research & Development',
                )
            )

    ),

    array(
        "title" => '<br> <br> Quality',
        "url" => 'iso-9001-quality-system.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-45',
        "h1" => '',
        "sub" =>
            array(

                array(
                    "title" => 'ISO 9001',
                    "url" => 'iso-9001-quality-system.php',
                    "seo-title" => 'Quality Management | Quality System | ISO 9001 | Certification - Third Coast Terminals',
                    "keywords" => 'Quality Management,Quality System,ISO 9001,Certification',
                    "desc" => 'The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries.'
                , 'h1' => 'ISO 9001 Quality Management'
                ),
                array(
                    "title" => 'Certifications',
                    "url" => 'third-coast-terminals-certifications.php',
                    "seo-title" => 'Certification - Third Coast Terminals',
                    "keywords" => 'Certification',
                    "desc" => 'Maintaining industry standard safety & compliance Certifications & Approvals.',
                    "h1" => 'Third Coast Terminals Certifications'
                ),
                array(
                    "title" => 'Permits',
                    "url" => 'third-coast-terminals-permits.php',
                    "seo-title" => 'Third Coast Terminals Permits - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Permits',
                    "desc" => 'Permits currently held by Third Coast Terminals.',
                    "h1" => "Third Coast Terminals Permits"
                ),
                array(
                    "title" => 'Responsible Care',
                    "url" => 'responsible-care-guiding-principles.php',
                    "seo-title" => 'Responsible Care Guiding Principles - Third Coast Terminals',
                    "keywords" => 'Responsible Care Guiding Principles',
                    "desc" => 'Third Coast Terminals is proud to participate in the American Chemistry Council Responsible Care initiative.',
                    'h1' => 'Responsible Care Guiding Principles',

                ),
                array(
                    "title" => 'Responsible Distribution',
                    "url" => 'responsible-distribution.php',
                    "seo-title" => 'NACD | Responsible Distribution | Certification - Third Coast Terminals',
                    "keywords" => 'NACD,Responsible Distribution,Certification',
                    "desc" => 'Responsible Distribution is a third-party verification environmental, health, safety & security program through which members demonstrate their commitment to continuous performance improvement.',
                    "h1" => 'Responsible Distribution'
                ),
            )

    ),
    array(
        "title" => '<br> <br> EH&S',
        "url" => '',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-52',
        "sub" =>
            array(
                array(
                    "title" => 'Environmental',
                    "url" => 'environmental.php',
                    "seo-title" => 'Environmental - Third Coast Terminals',
                    "keywords" => 'Third Coast Environmental Services',
                    "desc" => 'Third Coast Terminals is deeply committed to the prevention of environmental pollution.',
                    "h1" => 'Third Coast Terminals Environmental Services'
                ),
                array(
                    "title" => 'Health',
                    "url" => 'health.php',
                    "seo-title" => 'Health - Third Coast Terminals',
                    "keywords" => 'Health',
                    "desc" => 'We protect our employee’s health by offering respiratory training, high quality equipment and medical evaluations including pulmonary function tests and fit test.',
                    "h1" => 'Health'
                ),
                array(
                    "title" => 'Safety',
                    "url" => ' safety.php',
                    "seo-title" => 'Safety - Third Coast Terminals',
                    "keywords" => 'Safety',
                    "desc" => 'The safety of our employees is extremely important to us both at work and at home.',
                    "h1" => 'Safety'
                ),

                array(
                    "title" => 'Security',
                    "url" => 'security.php',
                    "seo-title" => 'Security - Third Coast Terminals',
                    "keywords" => 'Security',
                    "desc" => ' Third Coast Terminals follows regulations set by the Department of Transportation (DOT) and Homeland Security.',
                    "h1" => 'Security'

                ),
                array(
                    "title" => 'Emergency Preparedness',
                    "url" => 'emergency-preparedness.php',
                    "seo-title" => 'Emergency Preparedness - Third Coast Terminals',
                    "keywords" => 'Emergency Preparedness',
                    "desc" => 'Third Coast Terminals has prepared an Emergency Action Plan Manual.',
                    "h1" => 'Emergency Preparedness'
                ),

            )

    ),
    array(
        "title" => '<br> <br> Company',
        "url" => 'gulf-coast-chemical-industry.php',
        "seo-title" => 'Gulf Coast Chemical Industry | Reactive Chemistry | Blending - Third Coast Terminals',
        "keywords" => 'Gulf Coast Chemical Industry,Reactive Chemistry,Blending',
        "desc" => 'Third Coast Terminals is a custom manufacturer and manager of petrochemicals.',
        "h1" => 'Petrochemical & Fine Chemicals Manufacturing and Handling Experts',
        'right' => '-30',
        "sub" => array(
            array(
                "title" => 'About Us',
                "url" => 'about-third-coast-terminals.php',
                "seo-title" => 'About Third Coast Terminals - Third Coast Terminals',
                "keywords" => 'About Third Coast Terminals',
                "desc" => 'Third Coast Terminals is a specialized Storage, Toll Processing/Reaction Chemistry, Contract Terminaling, Blending and Drumming Operation serving the Gulf Coast Petrochemical Industry.',
                "h1" => 'About Third Coast Terminals',
                "show" => 1
            ),
            array(
                "title" => 'Affiliates',
                "url" => 'third-coast-terminals-affiliates.php',
                "seo-title" => 'Third Coast Terminals Affiliates - Third Coast Terminals',
                "keywords" => 'Third Coast Terminals Affiliates',
                "desc" => 'A display of companies that are affiliated with Third Coast Terminals.',
                "h1" => '',
            )
        )
    ),
    array(
        "title" => '<br> <br> Contact Us',
        "url" => 'contact-third-coast-terminals.php',
        "seo-title" => '',
        "keywords" => '',
        "desc" => '',
        'right' => '-25',
        "sub" =>
            array(

                array(
                    "title" => 'Contact Info',
                    "url" => 'contact-third-coast-terminals.php',
                    "seo-title" => 'Contact Third Coast Terminals - Third Coast Terminals',
                    "keywords" => 'Contact Third Coast Terminals',
                    "desc" => 'Contact information for Third Coast Terminals .',
                    "h1" => 'Contact Third Coast Terminals',
                    "show" => 1,
                    "banner" => 'contct-banner.jpg'
                ),
                array(
                    "title" => 'Inside Sales',
                    "url" => 'inside-sales.php',
                    "seo-title" => 'Inside Sales - Third Coast Terminals',
                    "keywords" => 'Inside Sales',
                    "desc" => 'Inside Sales is the backbone of Third Coast Terminal’s Quality Initiative at the customer interface.',
                    "h1" => 'Inside Sales'
                ),
                array(
                    "title" => 'Human Resources',
                    "url" => 'human-resources.php',
                    "seo-title" => 'Human Resources - Third Coast Terminals',
                    "keywords" => 'Human Resources',
                    "desc" => 'Working together, we create the essential elements for life.',
                    "h1" => 'Working at Third Coast Terminals'
                ),
                array(
                    "title" => 'Community ',
                    "url" => 'community.php',
                    "seo-title" => 'Our Community - Third Coast Terminals',
                    "keywords" => 'Our Community ',
                    "desc" => 'Third Coast has representatives in local civic and governmental institutions.',
                    "h1" => 'OUR COMMUNITY'
                ),
                array(
                    "title" => 'Affiliations',
                    "url" => 'third-coast-terminals-affiliations.php',
                    "seo-title" => 'Third Coast Terminals Affiliations - Third Coast Terminals',
                    "keywords" => 'Third Coast Terminals Affiliations',
                    "desc" => 'Meeting the quality needs and expectations of our customers is our highest priority.',
                    "h1" => 'Third Coast Terminals Affiliations'
                )
            )

    )

);

// current page value
$currentPage = false;
$currentTopPage = false;
$currentInnerPage = false;
foreach ($pages as $page) {
    if ($page['url'] == $current) {
        $currentPage = $page;
    }
    if (isset($page['sub'])) {
        foreach ($page['sub'] as $sub) {
            if ($sub['url'] == $current) {
                $currentPage = $sub;
                $currentTopPage = $page;
                $currentInnerPage = $sub;
            }
        }
    }
}
?>
<?php

// stub creater

/*$mock = file_get_contents('/home/suresh/softs/hyper/oc/dev/mock.stub');

foreach ($pages as $page) {

    $d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $page['url'], $mock);
    if (isset($page['sub'])) {


        foreach ($page['sub'] as $sub) {
            $d = file_put_contents('/home/suresh/softs/hyper/oc/dev/' . $sub['url'], $mock);
        }
    }
}
exit;*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->

    <title><?= $currentPage['seo-title'] ?></title>

    <meta name="keywords" content="<?= $currentPage['keywords'] ?>">
    <meta name="description" content="<?= $currentPage['desc'] ?>">
    <meta name="author" content="www.petropages.com/creative/">
    <meta name="geo.region" content="US-TX"/>
    <meta name="geo.placename" content="Pearland"/>
    <meta name="geo.position" content="29.573049;-95.294782"/>
    <meta name="ICBM" content="29.573049, -95.294782"/>


    <link href="assets/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">


    <link href="assets/style.css" rel="stylesheet">

    <script src="assets/jquery/dist/jquery.js"></script>
</head>
<body>

<!--========================================================
                          HEADER
=========================================================-->
<header class="<?= $current == 'index.php' ? '' : 'inner'; ?>">
    <div class="top-bar">
        <div class="container">
            <div class="clearfix">
                <div class="leftside col-lg-4">
                    <div> 1871 Mykawa Rd, Pearland, TX 77581</div>
                </div>

                <div class="rightside col-lg-8 ">
                    <div>

                        <ul class="list-inline list-unstyled pull-right">
                            <li><span>Toll Free </span> 877.412.0275</li>
                            <li><span> Fax  </span> 281.412.0245</li>
                            <li>sales@3cterminals.com</li>
                        </ul>
                    </div>

                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">
            <?php include "nav.php" ?>
        </div>
    </div>

</header>


<?php if ($current == 'index.php') { ?>
    <section class="full" id="slider">

        <div
            id="header-video"
            class="full-height black-wrapper video"
            data-vide-bg="mp4: assets/video/3C-cut-for-home-page, webm: style/video/ocean, ogv: style/video/ocean, poster: style/video/ocean.jpg"
            data-vide-options="position: 0% 10%">
            <!-- replace the above options with your own -->

            <div class="text-center slider-text">
                <!-- multiple h1's are perfectly acceptable on a page in valid HTML5, when wrapped in individual sections, they're 100% semantically correct -->

                <div class="title">A world-Class</div>
                <div class="title">Services Company</div>
                <div class="sub">Providing customized solutions</div>


                <a href="about-third-coast-terminals.php" class="btn btn-white btn-primary btn-white slider-btn"> Learn More </a>


            </div>
            <a href="#content" class="down"> <i class="ion-chevron-down"></i></a>
            <!-- seriously -->
        </div>

    </section>
<?php } else {

    if ($currentInnerPage) {
        ?>
        <section id="inner-slider" >
            <img
                src="<?= !isset($currentInnerPage['banner']) ? "assets/images/banner2_02.jpg" : "assets/images/" . $currentInnerPage['banner'] ?>"
                class="banner img-responsive" alt="">

            <div class="container" style="position: relative">
                <div class="inner-header">

                    <nav class="inner-nav">
                        <ul class="nav nav-tabs">

                            <?php foreach ($currentTopPage['sub'] as $item) { ?>
                                <li class="<?= $current == $item['url'] ? 'active' : ''; ?>"><a
                                        href="<?= !isset($item['show']) ? "#" : $item['url'] ?>"><?= $item['title'] ?></a>
                                </li>

                            <?php } ?>
                        </ul>
                    </nav>
                    <div class="page-title">
                        <h1><?= $currentInnerPage['h1'] ?></h1>
                    </div>

                </div>

            </div>

        </section>
    <?php } ?>
<?php } ?>
<div id="content" class="clearfix"></div>