<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Our primary concern is for the well-being of our team members. This has led us to introduce a voluntary Wellness Program, at no cost to our team members, with annual healthy physical screening and annual flu inoculations.</p>

            <p>We are a non-smoking facility, believing that more deterrents to smoking at work aids team members in their efforts to quit while improving the environment for non-smokers and reductions in fire related accidents.</p>

            <p>We protect our team members' health by offering respiratory training, high quality equipment and medical evaluations including pulmonary function tests and fit test.</p>

            <p>Other forms of personal protective equipment (PPE) are supplied and used to protect our team members from chemical exposure which could result in a contamination by contact.</p>

            <p>To further protect our team members' health, eating and drinking are not allowed in the facility except in designated areas. Keeping a clean non-contaminated eating area for team members greatly reduces the potential of contamination by contact with the products we handle.</p>

            <p>Team members are trained in how to handle blood-borne pathogens in the event of possible exposure including clean-up and disposal.</p>

            <p></p>

            <p>A facility-wide, Industrial Hygiene (IH Testing) monitoring program was conducted to determine hearing protecting requirements. Noise levels were determined to be below the OSHA TWA standard. Therefore, team members are not mandated to wear hearing protection, even though it is available for anyone’s use.</p></div>

            <div class="col-lg-6">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">


                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="images/health-1.jpg" title="Health" alt="Health">
                        </div>

                        <div class="item">
                            <img src="images/health-2.jpg" title="Health" alt="Health">
                        </div>


                    </div>

                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                </div>


            </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
