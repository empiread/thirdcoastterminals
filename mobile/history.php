<?php include "header.php" ?>
    <br>
<!--<style type="text/css">
	.inner-header {
    /* position: absolute; */
    /* bottom: 0; */
    background: rgba(255, 255, 255, 0.6);
    width: 100%;
    margin-top: 230px;
    bottom: 0;
}

#inner-slider {
    position: relative;
    height: 350px;
}-->
<style type="text/css" media="screen">
	.no-padding {
		padding-left:0;
		padding-right:0;
	}
	.carousel-control{ 
		top:200px; 
		height:25px;
		 }
		 
	/*Carosel history page*/

#history .carousel-indicators {
    margin-left: 0;
    padding-left: 0;
    width: auto;
    margin-top: 20px;
}

#history .carousel-indicators > li,
#history .carousel-indicators > li.active {
    width: 113px;
    height: auto;
    padding: 5px 32px;
    border-radius: 0;
    border: solid 2px #0C5E86;
    color: #0C5E86;
    background: #FFFFFF;
    text-indent: 0;
}

#history .carousel-indicators > li.active {
    background: #0C5E86;
    color: #ffffff;
}
#history .carousel-indicators > li:hover {
    background: #0C5E86;
    color: #ffffff;
}
#history .carousel-control {
    /* width: auto;*/
    opacity: 1;
}
.carousel-control.left, .carousel-control.right{
	background-image:none;
}
.headline h2{ 
margin-top: 10px;
}
@media (max-width:325px){
	.carousel-control{
		top:250px;
	}
}
</style>

   <div class="container" id="history">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
		 <div>
                <ol class="carousel-indicators" style="position: static">
                    <li data-target="#myCarousel" data-slide-to="0" class="active">1998</li>
                    
                    <li data-target="#myCarousel" data-slide-to="1" class="">2002</li>
                    
                    <li data-target="#myCarousel" data-slide-to="2" class="">2005</li>
                    <li data-target="#myCarousel" data-slide-to="3" class="">2008</li>
                    <li data-target="#myCarousel" data-slide-to="4" class="">2010</li>
                    <li data-target="#myCarousel" data-slide-to="5" class="">2012</li>
                    <li data-target="#myCarousel" data-slide-to="6" class="">2014</li>
                    <li data-target="#myCarousel" data-slide-to="7" class="">2016</li>
                    <li data-target="#myCarousel" data-slide-to="8" class="">2018</li>
                </ol>
            </div>
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-1998.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                1998
                            </h2>
                        </div>
                        <br>
                        <p>Establishment of Third Coast Terminals Pearland Location.</p>
                        <p>Property started with 8 storage tanks and 50,000 sq. ft. of warehouse on 13 acres of property.</p>
                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2002.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2002
                            </h2>
                        </div>
                        <br>
                        <p>Purchase of additional 3 acres of Third Coast property.</p>
                        <p>Added our first truck scale.</p>
                        <p>Constructed first white room for high purity/food product packaging.</p>
                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2005.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2005
                            </h2>
                        </div>
                        <br>
                        <p>Construction of Front Office, additional storage tanks and foundation of new tank farm.</p>
                        <p>Purchase of 6 acres of land for the construction of a corporate office.</p>
                        <p>Facility has approximately 48 storage tanks.</p>
                        <p>Construction of a raw material product storage warehouse.</p>
                        <p>Clearing of land for the rail expansion.</p>
                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2008.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2008
                            </h2>
                        </div>
                        <br>
                        <p> Double the number of storage tanks and the construction of a free-standing onsite lab.</p>
                        <p>Purchase of 13 acres of land for Third Coast Terminals expansion.</p>
                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2010.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2010
                            </h2>
                        </div>
                        <br> 
                        <p>Completed the original tank farm and started the new “North” tank farm.</p>
                        <p>Added 20,000 sq. ft. of warehouse by connecting the original warehouse to the raw material product storage warehouse.</p>
                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2012.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2012
                            </h2>
                        </div>
                        <br>
                        <p>Purchase of additional land, additional construction of second entrance and warehouses.</p>
                        <p>Construction of a new 20,000 sq. ft. warehouse for flammable and combustible product storage.</p>
                        <p>Added an enclosed filler for flammable and combustible products.</p>
                         <p>Construction of the Fire Prevention Facilities for tank farm and future warehouse expansion.</p>
                    </div>
                </div>
                
                
                <div class="item">

                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2014.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2014
                            </h2>
                        </div>
                        <br>
                        <p>Construction completed on 40,000 sq. ft. of additional warehouse.</p>
                        <p>Construction completed on rail to accommodate more than 90 railcars within the facility.</p>
                        <p>Expanded the onsite lab.</p>
                        <p>Purchase of an additional 25 acres - making our total site for this location more than 60 acres.</p>

                    </div>
                </div>
                
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2016.jpg" style="width:100%" title="Third Coast Terminals 2015" alt="Third Coast Terminals 2015">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2016
                            </h2>
                        </div>
                        <br>
                        <p>Construction of a new 40,000 sq. ft. warehouse.</p>
                        
                        <p> Construction of a manufacturing area  for reaction chemistry.</p>

                    </div>
                </div>
                <div class="item">
                    <div class="col-lg-9 no-padding" style="padding-left:0">
                        <img src="images/h/third-coast-terminals-2019.jpg" style="width:100%" title="Third Coast Terminals 2018" alt="Third Coast Terminals 2018">
                    </div>
                    <div class="col-lg-3" style="padding-right:0">
                        <div class="headline">
                            <h2>
                                2018
                            </h2>
                        </div>
                        <br>
                        <p>Construction of an Emergency Response Building</p>
                        <p>Additional Reactor Capacity</p>
                        <p>Upgraded Utility Sector</p>
                        <p>Construction of a Steam Rack which will accommodate 15 ISO’s/Trailers</p>
                        <p>25 additional Storage Tanks</p>
                        <p>Additional Administrative Space</p>

                    </div>
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="ion-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="ion-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
           
<div class="clearfix"></div>
            <div class="clearfix"></div>
            <div class="clearfix"></div>

<h1>Watch Our History</h1>

            <div class="col-lg-6" style="margin-left: 1px; width: 100% !important;">
               <video width="98%" poster="/assets/images/video-poster.png" controls>
                  <source src="/assets/video/ThirdCoastHistoryWeb.mp4" type="video/mp4" >

                  </video>
<br><br>
              </div>

        </div>
    </div>

    <!--========================================================
                            FOOTER
    =========================================================-->
<?php include "footer.php" ?>