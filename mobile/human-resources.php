<?php include "header.php" ?>


<div class="container ">
	
	
	

        <div class="col-xs-12 mtb-25">
        	<h2>Our people make the difference.</h2>
        	<p>Our Human Resources Mission is to create a Highly Productive Team and a Great Place to Work! Find out more about joining the Third Coast Family:</p>
        </div>
        
        <div class="col-xs-12">
        <div class="row">
           <div class="col-xs-6" style="text-align:center;">
			<a href="benefits.php"><img src="../mobile/images/Benefits.png"></a><br>
			<strong>BENEFITS</strong>
        	</div>
        	
        	<div class="col-xs-6" style="text-align:center;">
			<a href="equal-opportunity.php"><img src="../mobile/images/Equal.png"></a><br>
			<strong>EQUAL <br>OPPORTUNITIES</strong>
        	</div>
        </div>
        	<br>
        <div class="row">
        	
        	
        	<div class="col-xs-6" style="text-align:center;">
			<a href="apply.php"><img src="../mobile/images/Application.png"></a><br>
			<strong>APPLY <br>NOW</strong>
        	</div>

            <div class="col-xs-6" style="text-align:center;">
            <a href="photo-gallery.php"><img src="../mobile/images/PhotoGallery.png"></a><br>
            <strong>PHOTO <br>GALLERY</strong>
            </div>
        	
        	
        </div>
        	<br>
        <div class="row">
        	
        	
        	
        	<div class="col-xs-6" style="text-align:center;">
			<a href="contact-third-coast-terminals.php"><img src="../mobile/images/ContactUs.png"></a><br>
			<strong>CONTACT US</strong>
        	</div>
        </div>
        	<br>
       
        </div>
        <br>
        <div class="col-xs-12">	
        	<p>Working together, we create the essential elements for life—polymers, chemicals and fuels that go into products and materials that you use every day. We operate with a lean staff, making the contributions of each individual critical to our success. We emphasize efficient, safe and reliable operations. Our shared purpose is to create exceptional value for our customers while providing world class service.</p>
 		</div>
 		
 		<div class="col-xs-12">
 			<strong>We seek talented, results-oriented team players who...</strong>
 				<ul>
 					<li>thrive on change</li>
 					<li>communicate effectively</li>
 					<li>challenge each other to improve</li>
 					<li>treat others with respect</li>
 					<li>operate safely and responsibly with a focus on integrity</li>
 				</ul>
 		</div>
 		
 		<div class="col-xs-12">
 			<strong>In return, we nurture a culture where you can...</strong>
 				<ul>
 					<li>succeed professionally</li>
 					<li>grow personally</li>
 					<li>team with others to develop innovative solutions</li>
 					<li>be rewarded well for your results</li>
 				</ul>
 		</div>
 		
 		<div class="col-xs-12">	
        	<p>Our opportunities for growth and achievement are strengthened by the innovation and creativity of our people. To succeed, we need individuals who believe they can—and do—make a difference....providing world class service.</p>
 		</div>
 </div>




<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
