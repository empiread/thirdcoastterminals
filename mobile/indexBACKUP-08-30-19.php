<?php include "header.php" ?>

<div class="mtb-25 deliver" >
    <div class="container">
        <div class="col-lg-6 hidden-md hidden-xs ">
            <img src="assets/images/bg1.gif" class="img-responsive" alt="">
        </div>
        <div class="col-lg-6">
            <div class="headline">
                <h2>
                    <b>We Deliver Optimized Solutions</b>
                </h2>
            </div>

            <p dir="ltr"><span>Third Coast Terminals was established in 1998 and has quickly grown to be recognized as one of the best supply chain partners. &nbsp;Our reputation as a provider for liquid storage, toll manufacturing (which includes blending and reactive processes), contract packaging, and trans-load services, continues to grow as we match our business model to the needs of our customers serving the global petrochemical market. </span>
            </p>

            <p><span id="docs-internal-guid-6ddfe654-a0d7-6ef4-a48b-e87809e0d453"><span>Third Coast Terminals is committed to the Environmental, Health, Safety and Security of our Team Members, Customers and our Surrounding Community. We have been awarded quality certifications including ISO 9001-2008, Responsible Distribution and a Responsible Care Partner. Additionally, in 2016 we were recognized by Texas Mutual Insurance for our dedication to Employee Health and Safety.  </span></span>
            </p>


        </div>
    </div>
</div>

<?php include "footer.php" ?>
