<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <h2><span>Food:</span></h2>

            <p><span>Third Coast is equipped to handle a wide range of food-grade products from vegetable oil to pharmaceutical grade propylene glycols (Better known as PG USP).</span>
            </p>

            <p><span>We offer high purity packaging lines for drums, totes, IBC&rsquo;s (up to 330 gallons), and 5 gallon pails in three select white rooms (ISO 14644-1 Class &lt; ISO 9). </span>
            </p>

            <p><span>Our facility and staff are prepared and experienced in following all required procedures to meet Kosher and Halal packaging standards. </span>
            </p>

            <p><span>All of our white-room packaging lines have access to truck, railcar and terminal operations with combined filling capacities in excess of 50,000 gallons/ 8 hour shift and utilizing dedicated transfer pumps, process piping lines and transfer hoses all based upon customer request. All filling is conducted by weight and all packaging containers are visually inspected inside and out prior to filling; packaging containers can be nitrogen purged and blanketed upon customer request.</span>
            </p>

            <h3><span>Industrial:</span></h3>

            <p><span>Third Coast Terminals has six industrial chemical packaging lines for products to accommodate either drums or IBC&rsquo;s. Four fill lines are designed to handle light to medium viscosity products while one is a flexible fill, high viscosity and high temperature packaging line.</span>
            </p>

            <p><span>In addition to traditional fill lines, Third Coast has added a &ldquo;Feige" Robotic Filling line. &nbsp;This is a &ldquo;State-of-the-art" fully automated filling station. This filler is enclosed in a stainless steel housing with automatic sliding doors (right and left), for in and outbound pallet movements. Attached to this new filling station, is a negative pressure thermal oxidizer and scrubbing system.</span>
            </p>

            <div><span><br/></span></div>

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/chemicals-packaging.jpg" title="cGMP" alt="HACCP">
                    </div>

                    <div class="item">
                    	<img src="images/chemicals-packaging-2.jpg" title="Chemicals Packaging" alt="Chemicals Packaging">
                    </div>

                    <div class="item">
                    	<img src="images/chemicals-packaging3.jpg" title="Chemicals Packaging" alt="Chemicals Packaging">
                    </div>


                </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>


                </div>
            </div>


        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
