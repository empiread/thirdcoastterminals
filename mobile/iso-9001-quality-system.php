<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

            <h2 dir="ltr"><span>About ISO 9001</span></h2>

            <p dir="ltr"><span>The ISO 9001 Quality Management standard is implemented by over one million companies and organizations in over 170 countries. &nbsp;The standard provides a common self-improvement template for businesses and is continually evolving.</span>
            </p>
            <p><a href="assets/pdf/2018-ISO-9001-Certificate.pdf" target="_blank"><img style="padding-right:5px;"src="assets/pdf/pdf.jpg"></a><a href="assets/pdf/2018-ISO-9001-Certificate.pdf" target="_blank">ISO 9001:2015 Certification of Registration </a><br> </p>
            <p><br></p>
            <h2 dir="ltr"><span>Quality Management Principles</span></h2>
            
            <p>The standard is based on a process approach to business management. Using this approach, business processes are managed through monitoring of key performance indicators. Emphasis is given to customer focus. The quality of the business is measured according to how well it performs against the self-established indicators. A cycle of continual improvement is established to assure positive change. The cycle is monitored using internal and external surveillance audits and a minimum of one top management review per year.</p>

            <p><span id="docs-internal-guid-dd2ac03d-a0ed-e2ae-95ea-42542ca156f6"><span>Third Coast Terminals is ISO 9001:2015 Certified by ABS Quality Evaluations, Inc.</span></span>
            </p>
            <p><a href="http://www.abs-qe.com/" target="_blank"><img src="../mobile/assets/images/abs-anab-iso-9001-2015.jpg"></a></p>
            <p><br></p>
            <h2 dir="ltr"><span>About ISO 14001</span></h2>
            
            <p>ISO 14001:2015 specifies the requirements for an environmental management system that an organization can use to enhance its environmental performance. ISO 14001:2015 is intended for use by an organization seeking to manage its environmental responsibilities in a systematic manner that contributes to the environmental pillar of sustainability.</p>
          
             
                <p><a href="assets/pdf/2018-ISO-14001-Certificate.pdf" target="_blank"><img style="padding-right:5px;"src="assets/pdf/pdf.jpg"></a><a href="assets/pdf/2018-ISO-14001-Certificate.pdf" target="_blank">ISO 14001:2015 Certification of Registration</a></p>
                <p><a href="http://www.abs-qe.com/" target="_blank"><img src="../mobile/assets/images/abs-anab-iso-14001-2015.jpg"></a></p>
			<p><br></p>

        </div>


    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



