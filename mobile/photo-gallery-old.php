<?php include "header.php" ?>


<div class="container ">
	<div class="mtb-25">
		<img src="/mobile/images/ThirdCoast_Fundraiser_Infographic-02.png" title="Third Coast Fundraiser" alt="Third Coast Fundraiser" style="width:300px;">
	</div>
	
	
	<div class="mtb-25">
        <div class="col-lg-6">
        	<h2>Third Coast Terminals Makes Generous Donation</h2>
        	<h4>Students in Pearland Benefit from Third Coast Terminals Donation</h4>
            <p>Teachers and staff at EA Lawhon Elementary School in Pearland ISD were amazed at the initiative and generosity of Third Coast Terminals' employees who decided to support a local oranization that has a positive impact in their community. They chose Communities In Schools at Lawhon Elementary School.</p>

              <p>
                The employees' original idea of purchasing sweatshirts for one hundred students identified by the school and the CIS site coordinator grew to the great surprise of the executive staff at Third Coast Terminals. They were able to collect nearly 250 sweatshirts! At that point, they decided to raise money and purchase clothing, under garments, shoes, etc. for students at Lawhon.  
              </p>
              
              <p>The departments in the company completed and used various techniques to raise money including a Las Vegas trip raffle, taco sales, and barbecue plates. Through their efforts, the company was able to raise over $15,000. The owner of Third Coast Terminals, Jim Clawson, Jr., generously agreed to match any amount of money that his employees raised, bringing the total donation to over $30,000. Approximately $10,000 was used to purchase clothing, games, and shoes for the students at Lawhon Elementary School, the remaining dollars will be distributed evenly among the other five CIS schools in Pearland, Texas: Carleston Elementary, Cockrell Elementary, Jamison and Sablatura Middle School and Pearland Junior High South.</p>

            <p>&nbsp;</p>
        </div>

        <div class="col-lg-6">
            <video style="text-align:center" width="280" height="220" controls Autoplay="autoplay">
  			<source src="/assets/video/Third-Coast-Terminals-Makes-Donation-To-Benefit-Elementary-Students.mp4" type="video/mp4">
</video>

        </div>
    </div>

<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

         <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
              <p>
                Texas Mutual Insurance Company announced that Third Coast Packaging, Inc. in Galveston County has been awarded the company’s top honor for workplace safety.  
              </p>

            <p>To download the entire press release, <a href="assets/assets/images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<br>
            <a href="assets/assets/images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank"><img class="img-responsive" src="assets/images/TXM-logo-2014-ol.png" title="Texas Mutual: Workers' Compensation Insurance"
                 alt="Texas Mutual: Workers' Compensation Insurance logo"></a>


        </div>
    </div>

<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

    <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
            <p>Third Coast Terminals has deployed M-Files in a hybrid cloud environment for disaster recovery to ensure the company's content is secured, protected and available in case the company's on-premises systems are compromised by a hurricane or another catastrophic event.</p>

            <p>
              “Being located in the hurricane prone Gulf Coast region, we required an information management solution that could provide
               disaster recovery capabilities and ensure our vital information assets were protected in the event of a natural disaster
               situation or hardware failure. Implementing M-Files in a HYBRID CLOUD environment provides us with a business continuity
               plan and disaster recovery system that will keep us up and running at all times”
            </p>
            
            <p>To download the entire press release, <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<p><br></p>
            <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank"><img class="img-responsive" src="images/press-release-third-coast-hybrid-cloud-disaster-recovery.jpg" title="Press Releases"
                 alt="Press Release"></a>


        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
