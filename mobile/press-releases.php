speci<?php include "header.php" ?>


<div class="container ">
	<div class="mtb-25">
        <div class="col-lg-12">
        	<p>PRESS RELEASE</p>
			<p>July 6, 2017 (Featured in August/September 2017 issue of PU Magazine.)</p>
        	<h2>Successful first year for Third Coast's Prepolymer Toll Manufacturing Unit</h2>
        	<p>Third Coast Terminals LLC– Pearland, Texas USA</p>
        	<p>Prepolymer contract manufacturer Third Coast Terminals LLC announced a full year of successful prepolymer toll manufacturing at its state-of-the-art prepolymer facility in Pearland, TX, USA. The unit was completed as scheduled in 2016 and commissioned flawlessly, the company said in a statement issued on 6 July 2017.</p>

        </div>
		
			
		
		
		<div class="col-lg-12">
              <p>
                <b>Third Coast</b> CEO Jim Clawson noted, “When we installed this unit at our Pearland, Texas facility, we planned to be a significant player in prepolymer tolling. The addition of this capability is directly in line with our strategy to be a full service provider to our customers. We have been running the unit flawlessly for over a year now and look to increase our throughput with more tolling”.  
              </p>
              <p>The unit is comprised of two matched pairs of reactors, two for full scale production. In addition there is a pilot plant unit. Both production pairs can produce between 6,000–32,000 lbs. (~ 2.7–14.5 t) of prepolymer per batch and are designed to handle very high viscosities. The new system is of stainless steel construction, fully automated, and is complemented by the site’s ample storage and blending capabilities. The facility is capable of handling MDI, TDI and aliphatic di-isocyanates, as well as producing a full range of polyether and polyester prepolymers. It has been carefully designed to allow the addition of small quantities of additives, thereby allowing customers to produce very specialized products.</p>
        </div>
        
        <div class="col-lg-12">      
              
			  
			  <p>The company said that it has a wealth of experience in polyurethanes, and, in addition to prepolymer tolling, offers in house engineering, process design, extensive laboratory facilities, and developmental scale reactors. The Pearland facility has over 170 storage tanks and is rail connected with the ability to load into railcars, tank trucks, ISO containers, flexi-bags, totes, drums and pails. Third Coast has drum and tote packaging lines for white room (food grade, halal and kosher certified), industrial and vapour controlled products.</p>
            <p>&nbsp;</p>
        </div>
       
        <div class="col-lg-12">
        	<p style="font-style:italic;">Third Coast’s new system has been designed to allow the addition of small quantities of additives, thereby allowing
customers to produce specialized products.</p>
            <img class="img-responsive" src="images/Prepolymer-Tolling.jpg" title="" alt="" style="width:300px;>

        </div>
        <div class="col-lg-2"></div> 
    </div>

	
	
	
	
	<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>
	
	<div class="mtb-25">
		<p>&nbsp;</p>
		<img src="/mobile/images/ThirdCoast_Fundraiser_Infographic-02.png" title="Third Coast Fundraiser" alt="Third Coast Fundraiser" style="width:300px;">
	</div>
	
	
	<div class="mtb-25">
        <div class="col-lg-6">
        	<h2>Third Coast Terminals Makes Generous Donation</h2>
        	<h4>Students in Pearland Benefit from Third Coast Terminals Donation</h4>
            <p>Teachers and staff at EA Lawhon Elementary School in Pearland ISD were amazed at the initiative and generosity of Third Coast Terminals' employees who decided to support a local oranization that has a positive impact in their community. They chose Communities In Schools at Lawhon Elementary School.</p>

              <p>
                The employees' original idea of purchasing sweatshirts for one hundred students identified by the school and the CIS site coordinator grew to the great surprise of the executive staff at Third Coast Terminals. They were able to collect nearly 250 sweatshirts! At that point, they decided to raise money and purchase clothing, under garments, shoes, etc. for students at Lawhon.  
              </p>
              
              <p>The departments in the company completed and used various techniques to raise money including a Las Vegas trip raffle, taco sales, and barbecue plates. Through their efforts, the company was able to raise over $15,000. The owner of Third Coast Terminals, Jim Clawson, Jr., generously agreed to match any amount of money that his employees raised, bringing the total donation to over $30,000. Approximately $10,000 was used to purchase clothing, games, and shoes for the students at Lawhon Elementary School, the remaining dollars will be distributed evenly among the other five CIS schools in Pearland, Texas: Carleston Elementary, Cockrell Elementary, Jamison and Sablatura Middle School and Pearland Junior High South.</p>

            <p>&nbsp;</p>
        </div>

        <div class="col-lg-6">
            <video style="text-align:center" width="280" height="220" controls Autoplay="autoplay">
  			<source src="/assets/video/Third-Coast-Terminals-Makes-Donation-To-Benefit-Elementary-Students.mp4" type="video/mp4">
</video>

        </div>
    </div>

<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

         <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
              <p>
                Texas Mutual Insurance Company announced that Third Coast Packaging, Inc. in Galveston County has been awarded the company’s top honor for workplace safety.  
              </p>

            <p>To download the entire press release, <a href="assets/assets/images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<br>
            <a href="assets/assets/images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank"><img class="img-responsive" src="assets/images/TXM-logo-2014-ol.png" title="Texas Mutual: Workers' Compensation Insurance"
                 alt="Texas Mutual: Workers' Compensation Insurance logo"></a>


        </div>
    </div>

<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

    <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
            <p>Third Coast Terminals has deployed M-Files in a hybrid cloud environment for disaster recovery to ensure the company's content is secured, protected and available in case the company's on-premises systems are compromised by a hurricane or another catastrophic event.</p>

            <p>
              “Being located in the hurricane prone Gulf Coast region, we required an information management solution that could provide
               disaster recovery capabilities and ensure our vital information assets were protected in the event of a natural disaster
               situation or hardware failure. Implementing M-Files in a HYBRID CLOUD environment provides us with a business continuity
               plan and disaster recovery system that will keep us up and running at all times.”
            </p>
            
            <p>To download the entire press release, <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<p><br></p>
            <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank"><img class="img-responsive" src="images/press-release-third-coast-hybrid-cloud-disaster-recovery.jpg" title="Press Releases"
                 alt="Press Release"></a>


        </div>
    </div>

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
