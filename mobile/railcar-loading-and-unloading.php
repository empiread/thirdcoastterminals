<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

			<p>Third Coast has many years of experience in rail car loading.</p>
            <h3 dir="ltr"><span>Rail &amp; Transportation</span></h3>

            <p dir="ltr"><span>Third Coast Terminals continues to expand our rail storage to meet the aggressive growth of our customers. Our latest expansion has increased on site storage to a new total of more than 80 rail cars. We have also increased the number of process storage positions for railcars up to a total of 12. We are served by the BNSF main line which provides excellent service/logistics.</span>
            </p>

			 <h3 dir="ltr"><span>Rail &amp; Transportation Transloading</span></h3>

            <p><span id="docs-internal-guid-b49028f7-a0e6-6d1c-5a30-d16181967d64"><span>Third Coast Terminals has the capability to perform all of the transloading services related to rail such as rail to truck transfers, rail access to our drumming facility and rail to terminal transfers.</span></span>
            </p>

            <h3 dir="ltr"><span>Transloading</span></h3>

            <p>From loading an ISO container into a storage tank, or loading from a rail car into a tank truck, Third Coast
            can handle all of your trans-loading needs. Trans-loading with Third Coast will allow your company to gain
            the economic benefits of a rail service and the on-time, flexibility of a tank truck delivery.</p>

            <p>With tank truck weight scales throughout the facility, a highly trained staff, and room for 80+ railcars on
            our property – Third Coast offers your company trans-loading which is another safe, reliable alternative to
            achieve all of your logistical needs.
            
            <p>"We have added new ISO and Tank Truck Steaming Racks to our original capacity. This new capability allows us to steam up to 15 ISO’s at a time for more efficient unloading of product. This capability allows our customers a faster turnaround time on ISO’s as they can go directly on steam when they arrive at the plant. Based on our initial success, we will be expanding this capability to 30 spots in the near future."</p>

             <h3 dir="ltr"><span>Railcar Loading</span></h3>

             <p>One of our most popular services is railcar loading directly from a storage tank. Customers who store products with us have the ability to quickly and easily send product from their storage tank via rail to any number of their customers. Our experience, tank storage, and access to the BNSF rail line directly from our property makes this process efficient and very cost effective for your transportation requirements.
             	</p>
              <h3 dir="ltr"><span>Railcar Unloading</span></h3>

              <p>One of our most popular services is railcar loading directly from a storage tank. Customers who store products with us have the ability to quickly and easily send product from their storage tank via rail to any number of their customers. Our experience, tank storage, and access to the BNSF rail line directly from our property makes this process efficient and very cost effective for your transportation requirements.</p>
        </div>

        <div class="col-lg-6">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">


              <!-- Wrapper for slides -->
              <div class="carousel-inner" role="listbox">
                  <div class="item active">
                      <img src="images/railcar-loading.jpg" title="Railcar Loading" alt="Railcar Loading">
                  </div>
                  <div class="item">
                        <img src="assets/images/transloading.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading-3.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading-2.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>

                    <div class="item">
                      <img src="images/railcar.jpg" title="Railcar" alt="Railcar">
                    </div>

                  <div class="item">
                      <img src="images/railcar-unloading.jpg" title="Railcar Unloading" alt="Railcar Unloading">
                  </div>

                  <div class="item">
                      <img src="images/railcar-pumping.jpg" title="Railcar Pumping" alt="Railcar Pumping">
                  </div>
                  
                  <div class="item">
                      <img src="images/railcar-transloading.jpg" title="Railcar Transloading" alt="Railcar Transloading">
                  </div>
                  <div class="item">
                      <img src="images/transloading-services7.jpg" title="Transloading Services"
                           alt="Transloading Services">
                  </div>
                  
                  <div class="item">
                      <img src="images/transloading-services8.jpg" title="Transloading Services"
                           alt="Transloading Services">
                  </div>
                  
                  <div class="item">
                      <img src="images/transloading-services.jpg" title="Transloading Services"
                           alt="Transloading Services">
                  </div>

                  <div class="item">
                      <img src="images/transloading-services2.jpg" title="Trans-Loading Services"
                           alt="Trans-Loading Services">

                  </div>


                  <div class="item">
                      <img src="images/transloading-services5.jpg" title="Transloading Services"
                           alt="Transloading Services">

                  </div>

                  <div class="item">
                      <img src="images/transloading-services6.jpg" title="Transloading Services"
                           alt="Transloading Services">

                  </div>


              </div>

              <!-- Left and right controls -->
              <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
              </a>
              <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
              </a>

          </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
