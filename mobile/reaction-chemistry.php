<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

          <p>
                Third Coast Terminals has over 20 years of chemical production experience between the location in Pearland, Texas and affiliate companies in Singapore and Qatar.
            </p>

            <p>
                <h2>Complete Reaction Services:</h2> Third Coast provides a full range of services to facilitate commercialization of our customer’s products.
            </p>
            
            <p>
            	<span style="text-decoration:underline;">Process Development</span> – Our technical team can produce at lab scale (glassware) and pilot scale (300 gal) to assist in process development and refinement. We can then transition the learnings from these processes into full scale commercial production.
            </p>
            
            <p>
            	<span style="text-decoration:underline;">Market Development</span> – Our capable production group can economically provide products for your customers in small quantities as is often required to generate market demand for new products.
            </p>
            
            <p>
            	<span style="text-decoration:underline;">Commercial Production</span> – Third Coast will manufacture your product at full scale to help you meet the needs of your customers. Whether you require additional capacity to supplement a growing marketplace, or you don’t have the capability to manufacture your product internally, Third Coast can fill your manufacturing gap.
            </p>
            
            
            <h2>Reaction Expertise</h2>
            
            <p>
            	Our team is focused on execution in each of our services. We have skilled and knowledgeable technical staff capable of ensuring effective knowledge transfer and technological fidelity. We work in concert with our customers to evaluate and understand the process, and process sensitivities.
            </p>
            
            <p>
            	Our operations group has a strong record of quality and timely production. We accomplish this through our commitment to operating discipline, in depth hands on operator development, and a close working relationship between the technical and operations groups.
            </p>
            
            <p>
            	All processes in our reaction department will undergo rigorous PHA evaluation before being implemented in our facility.
            </p>
            
            <h2>
            	Efficient Commercialization
            </h2>
            
            <p>
            	Third Coast can begin producing in a matter of weeks or months in many cases, depending on the needs of your process. We can bring commercial scale to your technology in such a tight time frame thanks to our cooperative environment and flat close organizational structure.
            </p>
            
            <p>
            	Should your process require capital investment, we can leverage this same infrastructure, as well as our equipment supplier relationships to provide the necessary functional operating equipment at a surprising speed and economical cost.
            </p>

            <p>
            	Related services – storage, packaging, analytical, filtration
            </p>

            <p>
                <a href="contact-third-coast-terminals.php" title="Contact Third Coast Terminals">Contact Us</a>
            </p>
        </div>

        <div class="col-lg-6">
          <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/reaction-chemistry-1.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="images/reaction-chemistry-2.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="images/reaction-chemistry-3.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="images/reaction-chemistry-4.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="images/reaction-chemistry-5.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="images/rc-tank-farm.jpg" title="Tank Farm" alt="Tank Farm">
                    </div>                  
                    <div class="item">
                        <img src="images/reaction-chemistry-12.jpg" title="Tank Farm" alt="Tank Farm">
                    </div>
                    <div class="item">
                        <img src="assets/images/reaction-operation-area-2.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="assets/images/reaction-operation-area-3.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="assets/images/reaction-operation-area-4.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="assets/images/reaction-operation-area-5.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>
                    <div class="item">
                        <img src="assets/images/reaction-operation-expertise.jpg" title="Reaction Chemistry" alt="Reaction Chemistry">
                    </div>


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
