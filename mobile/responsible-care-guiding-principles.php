<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">


            <p>Third
                Coast Terminals is proud to participate in the American Chemistry
                Council Responsible Care initiative. View our Responsible Care Certificate below.</p>

            <p><br></p>
 <p><a href="assets/pdf/2018-RC-ISO-14001-Certificate.pdf" target="_blank"><img style="padding-right:5px;"src="assets/pdf/pdf.jpg"></a><a href="assets/pdf/2018-RC-ISO-14001-Certificate.pdf" target="_blank">Responsible Care 14001 2015 Certificate of Registration</a></p>
           <p><br></p>
            <p>Chemistry
                is essential to the products and services that help make our lives
                safer, healthier and better. Through the Responsible Care initiative
                and the Responsible Care Global Charter our industry has made a
                worldwide commitment to improve our environmental, health, safety and
                security performance. Accordingly, we believe and subscribe to the
                following principles:</p>

            <p></p>
            <ul>
                <li><p>To
                        lead our companies in ethical ways that increasingly benefit
                        society, the economy and the environment.</p>
                </li>
                <li><p>To
                        design and develop products that can be manufactures, transported,
                        used and disposed of or recycled safely.</p>
                </li>
                <li><p>To
                        work with customers, carriers, suppliers, distributors and
                        contractors to foster the safe and secure use, transport and
                        disposal of chemicals and provide hazard and risk information that
                        can be accessed and applied in their operations and products.
                    </p>
                </li>
                <li><p>To
                        design and operate our facilities in a safe, secure and
                        environmentally sound manner.</p>
                </li>
                <li><p>To
                        instill a culture throughout all levels of our organizations to
                        continually identify, reduce and manage process safety risks.</p>
                </li>
                <li><p>To
                        promote pollution prevention, minimization of waste and conservation
                        of energy and other critical resources at every stage of the life
                        cycle of our products.</p>
                </li>
                <li><p>To
                        cooperate with governments at all levels and organizations in the
                        development of effective and efficient safety, health, environmental
                        and security laws, regulations and standards.
                    </p>
                </li>
                <li><p>To
                        support education and research on the health, safety, environmental
                        effects and security of our products and processes.
                    </p>
                </li>
                <li><p>To
                        communicate product, service and process risks to our stakeholders
                        and listen to and consider their perspectives.</p>
                </li>
                <li><p>To
                        make continual improvement in our integrated Responsible Care
                        Management System to address environmental, health, safety and
                        security performance.
                    </p>
                </li>
                <li><p>To
                        promote Responsible Care by encouraging and assisting others to
                        adhere to these Guiding Principles.&nbsp;
                    </p>
                </li>
            </ul>
        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/responsible-care.jpg" title="Responsible Care" alt="Responsible Care">
                    </div>
                </div>
            </div>


        </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



