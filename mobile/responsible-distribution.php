<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p dir="ltr"><span>Responsible Distribution is a third-party verification environmental, health, safety &amp; security program through which members demonstrate their commitment to continuous performance improvement in every phase of chemical storage, handling, transportation, and disposal. NACD members play a leadership role in their communities as information resources and provide the same assistance and guidance to local, state, and federal legislators on technical issues relating to the safe handling, storage, transportation, use, and disposal of chemical products.</span>
            </p>
            <h4 dir="ltr"><span>Key Benefits of Responsible Distribution</span></h4>
            <ul>
                <li dir="ltr">
                    <p dir="ltr"><span>Lower occurrences of safety and environmental incidents</span></p>
                </li>
                <li dir="ltr">
                    <p dir="ltr"><span>Enjoy better documentation of company policies</span></p>
                </li>
                <li dir="ltr">
                    <p dir="ltr">
                        <span>Employ practices of excellence and quality systems throughout your operations</span></p>
                </li>
                <li dir="ltr">
                    <p dir="ltr"><span>Engage in better communication with local communities</span></p>
                </li>
                <li dir="ltr">
                    <p dir="ltr"><span>Reduce audit costs and time spent on audits</span></p>
                </li>
                <li dir="ltr">
                    <p dir="ltr"><span>Save on insurance expenses</span></p>
                </li>
                <li dir="ltr"><span>Improve marketplace visibility and earn credibility through performance</span></li>
            </ul>
        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/responsible-distribution.jpg" title="Responsible Distribution"
                             alt="Responsible Distribution">
                    </div>


                </div>
            </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



