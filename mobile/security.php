<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>Third
                Coast Terminals follows regulations set by the Department of
                Transportation (DOT) and Homeland Security.</p>

            <p>Internal
                security audits are conducted, documented and acted on with due
                diligence. Combined training for railcar and bulk tanker Transcaer
                Training for first responders, team members and others.</p>

            <p>Facility
                property is completely fenced.</p>

            <p>Smoking
                is not permitted anywhere within the facility. Smoking products,
                including lighters, are not allowed into the plant.</p>

            <p>Warning
                signs are posted around the facility’s entire perimeter.</p>

            <p>Continuous
                surveillance cameras are strategically located throughout the campus.</p>

            <p>The
                plant gates are operated by “Key Card” and monitored.</p>

            <p>Visitors
                are never left unattended, they are always escorted while in the
                plant.</p>

            <p>The
                facility is well illuminated from dusk to dawn.</p>

            <p>Storage
                tanks, containers and buildings are all secured when unattended.</p>

            <p>Discharge
                valves are padlocked when not in use. This minimizes the chance of
                product leakage, cross-contamination and vandalism.</p>

            <p>Team members
                are trained and re-trained annually. Before beginning work at Third
                Coast Terminals team members are given awareness training. Job specific
                training for DOT, IMDG, IATA and OSHA programs is incorporated into
                early training schedules and no one is allowed to operate a piece of
                equipment without the proper training/certification.</p>

            <p>Random
                drug screening and criminal background checks are performed for all
                team members.</p>

            <p></p>

            <p>Third
                Coast Terminals prohibits taking weapons of any type into the plant.
                Personal vehicles are not allowed in the terminal area except when
                authorized by senior management.</p></div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/security.jpg" title="Security" alt="Security">
                    </div>


                </div>
            </div>


        </div>


    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



