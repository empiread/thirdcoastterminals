<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">



            <p dir="ltr"><span>Third Coast has an extensive terminal operation with more than 170+ tanks in current service. Plans are in place to expand our Pearland Operations with additional tanks exceeding 10 million gallons of total liquids storage. Our tanks range in size from 1,000 to 90,000 gallons and materials of construction include carbon steel (lined and unlined), and stainless steel. Bulk product tanks typically have auto gauging, steam coils, nitrogen purge and dedicated transfer lines and pumps. Our facility has grown to 60+ acres, which has allowed us to add additional railcar storage inside our fence line. This additional railcar storage gives our customers a logistical advantage in both flexible short and long-term product storage availability.</span>
            </p>

            <p><span id="docs-internal-guid-e14d5cfe-a0e4-eee7-49d2-36bce2011774"><span>Trans-loads may be made from any bulk liquid container to the tank terminals; these include railcar, bulk truck, flexitanks, and ISO containers. Retain samples are taken and tested in our on-site laboratory from each load assuring the incoming and outbound products meet our customer’s specifications. Each operation in the terminal is witnessed by at least two individuals independently. This is just a part of our internal quality assurance documentation and process. </span></span>
            </p>
        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/tank-terminal-services.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services2.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services3.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">
                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services4.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>

                    <div class="item">
                        <img src="images/tank-terminal-services5.jpg" title="Tank Terminal Services"
                             alt="Tank Terminal Services">

                    </div>
                    


                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>


    </div>


</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
