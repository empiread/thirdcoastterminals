<?php include "header.php"
?>
<style>
.ion-chevron-left{
  background: #fff;
    padding: 10px;    
    z-index: 9999999;
    position: absolute;
    left: 20px;
}
.ion-chevron-right{
  background: #fff;
    padding: 10px;    
    z-index: 9999999;
    position: absolute;
    right: 20px;
}
.pagi{
  float: left;
    width: 100%;
    text-align: center;
}
.pagi ol{
  padding: 0;
    margin: 0 auto;
    position: absolute;
    bottom: 4px;    
    left: 0;
    right: 0;
  margin-top: 10px !important;
}
.pagi ol li{
  display: inline-block;
    background: #0C5E86;
    width: 4% !important;
    height: 5px !important;
    cursor: pointer;
    margin: 0 5px;
  padding: 1px 14px !important;
}
.carousel-indicators li.active a{
  color:#fff;
}
.carousel-indicators li a:hover,
.carousel-indicators li a:focus{
  color:#fff;
  text-decoration:none;
}

#photo .carousel-indicators > li , #photo .carousel-indicators > li.active {
  padding:0 5px;
}
#photo .carousel-indicators > li a, #photo .carousel-indicators > li.active a{
     width: 100%; 
    float:left;
padding:5px;  
}
</style>

<?php include("../filereader.php"); 
$tctcares = ''; 
   if(isset($_GET['gallery']) && $_GET['gallery'] != ''){
   $gallery = $_GET['gallery'];
   switch($gallery){
    case 1:
         $gallery_template = $tct_cares;
       $tctcares = 'class="active"';
         break;
         default:
         $gallery_template = $tct_cares;
       $tctcares = 'class="active"';
       break;
   }
   }else{
     $gallery_template = $tct_cares;
     $tctcares = 'class="active"';
   }

?>

<div class="container ">

    <div class="mtb-25">

  <div class="col-lg-12" style="text-align:center;">

    <!--<img src="images/tct-family-day.jpg" class="img-responsive" style="width: 75%; margin: auto;" />-->
    <!--<iframe src="https://calendar.google.com/calendar/embed?title=TCT%20Cares%20Upcoming%20Events&amp;showTitle=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;height=600&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=tctcares%40gmail.com&amp;color=%231B887A&amp;ctz=America%2FChicago" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe>-->
    <!--<br>
    <br />
    <br />-->
  </div>  
        <div class="col-lg-6">

            <h3>Help us raise money to support Communities in Schools and
Pearland Neighborhood Center.</h3>
      <p>Both organizations provide
basic clothes and food needs for Pearland residents.</p>
<ul>
  <li>TCT's Goal is to raise $16,000 in 17 weeks</li>
  <li>TCT Fundraiser will end on Nov 22nd.</li>
  <li>TCT's owner, Jim Clawson will match the money raised.</li>
</ul>

<p>
Last year Third Coast Terminals raised $15,171 in 6 weeks.
Our owner, Jim Clawson, matched that to total $30,342.16
that we gave to Communities in School, Pearland.
      </p>
      
      
    </div>    
    <div class="col-lg-6" style="text-align:center;">
        <img src="images/tct-cares-check.jpg" title="TCT Cares Check" alt="TCT Cares Check">
    </div>
        
    <div class="col-lg-6>">
      <h3>Tradition of Giving</h3>
      <p>Third Coast Terminals understands the importance and responsibility of giving back to our community. The tradition of giving is one of our leadership's core beliefs and as such has been embraced and encouraged by our team members.</p>
    <br>
    <h3>Our Team Members Make The Difference</h3>
    <p>Each year our team members work together to create fundraising events throughout the year. Every dollar from bbq plates, raffle tickets, candy bars, and bake sales all add up, and when matched by our TCT Cares Team Member Donation Matching Program we are able to make a real difference in our communities. In just the past five years, we were able to donate over $95,000 to two outstanding local charities.</p>
    </div>

    <br>
    <div class="col-lg-6">

            <h3>Local Charities Benefiting Local Families</h3>
      <p><a href="https://cistxjv.org" target="_blank">Communities In Schools</a> Southeast Harris and Brazoria County is dedicated to providing exemplary services that empower students to successfully learn, stay in school and achieve in life. Their primary focus is to provide students with the necessary resources, tools, and real-world skills that give them the opportunity to be successful in school and beyond the classroom.</p>



      
      
    </div>    
    <div class="col-lg-6" style="text-align:center;">
        <a href="https://cistxjv.org" target="_blank"><img src="images/communities-in-schools.jpg" title="Communities In Schools" alt="Communities In Schools"></a>
    </div>

    <div class="col-lg-6">

            <h3>Pearland Neighborhood Center</h3>
      <p>The <a href="https://pnctexas.com/" target="_blank">Pearland Neighborhood Center's</a> mission is to provide effective health and human services and responsive programs to facilitate the development of individuals and families in the northern end of Brazoria County. Their multi-service center concept helps those in need of a variety of social services including food and baby pantry, medical and prescription assistance, school supplies and clothing, continuing education scholarships, senior employment program, job training courses, disaster relief and more.</p>



      
      
    </div>    
    <div class="col-lg-6" style="text-align:center;">
        <a href="https://pnctexas.com/" target="_blank"><img src="images/pearland-neighborhood-center.jpg" title="Pearland Neighborhood Center" alt="Pearland Neighborhood Center"></a>
    </div>  

<p>&nbsp;</p>
<p>&nbsp;</p>
<!--- Beginning of Photo Gallery -->

<div class="container" id="photo">
       <div id="myCarousel" class="carousel slide" >
            <!-- Wrapper for slides -->
          <?php echo $gallery_template; ?>
            

            <!-- Left and right controls -->
            
          <!--   <a class="previous carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="ion-minus-round" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <a class="next carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="ion-minus-round" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>-->
      
            
        </div>
    
      

  <div class="col-lg-12"> 
    <div class="col-lg-12" style="text-align:center;">
      <br>
      <h3>Looking to get involved?</h3>
        <p>If you would like more information about TCT Cares and how you can help please contact the Human Resources department at <a href="mailto:HR@3cterminals.com">HR@3cterminals.com</a></p>
      <br>
    </div>
    
    
  </div>
  <div class="col-lg-12"></div>
  <div class="col-lg-12"><p>&nbsp;</p></div>  
    
    </div>
    </div>
</div>

<!--========================================================        FOOTER
=========================================================-->
<script>
var $tct_cares   = '<?php echo $tct_cares; ?>';
</script>
<?php include "footer.php" ?>
 