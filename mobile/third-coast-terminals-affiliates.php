<?php include "header.php" ?>


<div class="container ">
    <div class="mtb-25">
        <!-- <div class="col-lg-12">
             <p>Meeting
                 the quality needs and expectations of our customers is our highest
                 priority. Third Coast Terminals is completely committed to ensuring
                 customer satisfaction is achieved at all times.
             </p>

             <p>We
                 are currently proud members of the follow organizations:</p>

             <p><b>Scrolling
                     Logos for the following affiliations: These logos are on the current
                     website http://www.thirdcoastterminals.com/affiliations/</b></p>

             <p>National
                 Association of Chemical Producers (NACP)</p>

             <p>National
                 Association of Chemical Distributors (NACD)
             </p>

             <p>An
                 international association of chemical distributor companies that
                 purchase and take title of chemical products from manufacturers. Its
                 members follow a strict code of Responsible Distribution®. It
                 provides a guideline for companies to protect the environment,
                 promote health and safety of employees and community members, enhance
                 product stewardship and ensure the security of its facilities and
                 products. Compliance is enforced through third-party verification.
             </p>

             <p>http://www.nacd.com</p>

             <p>American
                 Fuel &amp; Petrochemical Manufacturers AFPM | American Fuel &amp;
                 Petrochemical Manufacturers
             </p>

             <p>Members
                 serve you and America by manufacturing vital products for your life
                 every day, while strengthening America's economic and national
                 security</p>

             <p>http://www.afpm.org</p>

             <p>Petroleum
                 Packaging Council</p>

             <p>PPC
                 | Petroleum Packaging Council</p>

             <p>Petroleum
                 Packaging Council, an association providing technical leadership and
                 education to the petroleum packaging industry.</p>

             <p>http://www.ppcouncil.org</p>

             <p>The
                 European Petrochemical Association</p>

             <p>EPCA
                 | European Petrochemical Association</p>

             <p>Global
                 Network and Communication Platform for the Chemical Industry and its
                 Logistics Service Providers.</p>

             <p>http://www.epca.be</p>

             <p>The
                 Association of Chemical Industry of Texas</p>

             <p>ACIT
                 | Association of Chemical Industry of Texas</p>

             <p>ACIT
                 is composed of businesses that share a symbiotic relationship with
                 the Texas chemical industry.</p>

             <p>http://www.acit.org</p>

             <p>Independent
                 Lubricant Manufacturers Association ILMA | Independent Lubricant
                 Manufacturers Association</p>

             <p>A
                 Trade organization for manufacturers of industrial lubricants,
                 focused on advocacy, networking and business promotion.</p>

             <p>International
                 Liquid Terminals Association</p>

             <p>ILTA
                 | Independent Liquid Terminals Association</p>

             <p>Independent
                 Liquid Terminals Association (ILTA) represents bulk liquid terminals
                 and above ground storage tank operators - bulk liquid storage tanks -
                 for-hire and throughput services, marketing and pipeline terminals.</p>

             <p>http://www.ilta.org</p>

             <p>Houston
                 Chemical Association Houston Chemical Association</p>

             <p>The
                 Association shall have for its purposes: Exists to foster and promote
                 the education of its members and the public by gathering and
                 distributing information of general interest within the field of
                 chemical manufacture and distribution.</p>

             <p>http://www.houstonchemical.org</p>

             <p>Pearland
                 Chamber of Commerce Pearland Chamber of Commerce</p>

             <p>The
                 Pearland Chamber of Commerce is a not-for-profit organization and is
                 owned and operated by local business leaders who work to promote
                 business growth in the community. The goal of the chamber is to
                 conceive, inspire and influence public policy decisions that will
                 benefit the local community and businesses.</p>

             <p>http://www.pearlandchamber.com</p>

             <p>
                 American
                 Chemistry Council American Chemistry Council
             </p>

             <p>The
                 American Chemistry Council's (ACC's) mission is to deliver business
                 value using exceptional advocacy using best-in-class performance,
                 political engagement, communications and scientific research. We are
                 committed to sustainable development by fostering progress in our
                 economy, environment and society.</p>

             <p>http://www.americanchemistry.com</p>

             <p>TRANSCAER TRANSCAER®</p>

             <p>TRANSCAER
                 is a voluntary national outreach effort that: Promotes safe
                 transportation and handling of hazardous materials, educates and
                 assists communities near major transportation routes about hazardous
                 materials and aids community emergency response planning for
                 hazardous material transportation incidents.
             </p>

             <p>http://www.transcaer.com</p>

             <p>Chemtrec®</p>

             <p>The
                 CHEMTREC vision is to continue to be recognized by emergency
                 responders, industry, government, and others as the world's foremost
                 emergency call center for information on hazardous materials and
                 dangerous goods.</p>


             <p>http://www.chemtrec.com</p></div>-->


        <a href="http://www.thirdcoast.com">Affiliates</a>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>



