<?php include "header.php" ?>
    <div class="container ">
        <div class="mtb-25">
            <div class="col-lg-12">

                <h3><span>Certifications and Approvals</span></h3>

                <p><span>Meeting the quality needs and expectations of our customers is our highest priority. Third Coast Terminals is completely committed to ensuring customer satisfaction is achieved at all times.</span>
                </p>
                <p>This will be accomplished by providing the highest quality service and by continually improving our quality management system, including maintaining industry standard safety & compliance Certifications & Approvals such as:</p>
                <ul>
                    <li>

                        <span>Federal Guaranty under section 303(C)(2) of the Food, Drug, and Cosmetic Act</span>

                    </li>
                    <li>
                        <span>Texas Department of Health, Food, and Drug License #0041062</span>
                    </li>
                    <li>
                        <span>FDA Labeler Code Number 67073</span>
                    </li>
                    <li>
                        <span>ISO 9001:2015 Certified</span>
                    </li>



                    <p></p>
                    <li>
                        <span>Federal Spill Prevention Control and Counter Measures (SPCC) Plan</span>
                    </li>
                    <li>
                        <span>Federal Storm Water Pollution Prevention Plan (SWP)</span>
                    </li>
                    <li>

                        <span>Texas Commission on Environmental Quality Notice of Intent (NOI) permit No. TX R05N876</span>

                    </li>
                    <li>
                        <span>Texas Natural Resource Conservation Commission Notice of Registration </span>

                    </li>
                    <li>
                        <span>EPA No. TX R000026807</span>
                    </li>
                    <li>
                        <span>We are a member of the American Chemistry Council (ACC)</span>
                    </li>
                </ul>

                <p>
                    Kosher (Kashruth) certificate with:
                </p>
                <ul style="list-style:none; font-size: 10.5pt; color: #333333;">
                <li>
                    <span>Orthodox Union</span>
                </li>
                <li>
                    <span>KOF-K Kosher Overseers Associates of America</span>
                </li>
                <li>
                    <span>Organized Kashruth Laboratories</span>
                </li>
                </ul>

                <p>Third Coast is committed to the conservation of our resources &amp; environment observing all rules
                    &amp; regulations.</span><span><br
                            class="kix-line-break"/></p>

            </div>
        </div>
    </div><!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
