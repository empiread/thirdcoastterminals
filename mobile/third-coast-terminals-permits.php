<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-12">

          <ul style="font-size: 10.5pt; color: #333333">
		<h3>Quality Policy</h3>

            <p>
                <em>“Meeting the quality needs and expectations of our customers is our highest priority. Third Coast Terminals is completely committed to ensuring their satisfaction is achieved continuously. This will be obtained by providing the highest quality service and by continually improving our management system.”</em>
            </p>

            <p>
                Objectives:

                <ul>
                    <li>
                        Consistently meeting or exceed our customers’ expectations of quality and performance.
                    </li>
                    <li>
                        Ensuring our personnel are properly trained so they are better able to serve our customer.
                    </li>
                    <li>
                        Timely delivery of products and services to meet customer requirements.
                    </li>
                    <li>
                    	Continuous improvement of our processes and systems.
                    </li>
                    <li>
                    	Continually improving our management system by monitoring indicators, nonconformance’s, communication and objectives and targets.
                    </li>
                </ul>
            </p>

            <p>
                As Vice President, I personally affirm my commitment to this policy.
                <br />
                Grif Carnes, Vice President
            </p>
            <br>
            <br>
            <h3>Environmental Health and Safety Policy</h3>
        
        	<p>Third Coast Terminals is committed to environmental leadership in all its Operations. We will provide a safe and healthy workplace for our employees as well as minimize our potential impact on the environment. We will operate in compliance with all relevant environmental legislation and strive to use pollution prevention and environmental best practices in all we do. Security Code is SC-01.</p>
        	
        	<p>We are committed to do and will:</p>
        	
        	<ul>
        		<li>Integrate the consideration of environmental concerns and impacts into all our decision making and activities.</li>
        		
        		<li>Train, educate and inform our employees and stake holders about environmental issues that may affect their work.</li>
        		
        		<li>Develop and improve operations to minimize waste, and other pollution, minimze health and safety risks, and dispose of waste safely and responsibly.</li>
        		
        		<li>Where significant health, safety or environmental hazards exist, develop and maintain appropriate emergency and spill response programs.</li>
        		
        		<li>Strive to continually improve our environmental performance and minimize the social impact and damage of activities by periodically reviewing our environmental policy considering our current and planned future activities.</li>
        		
        		<li>Promptly report all noncompliance issues in accordance with applicable governmental reporting requirements, evaluate causes of noncompliance, and implement corrective actions.</li>
        		
        	</ul>
        	<br>
        	<b>Objectives</b>
        	
        	<ul>
        		<li>Continuously improve our EH&S management system to reduce the number of conconformance's found during internal and external audits.</li>
        		
        		<li>Reduce employee injuries.</li>
        		
        		<li>Educating our stakeholders on the safe use of chemicals and environmental conservations.</li>
        		
        		<li>Comply with applicable local, state and federal legal requirements and permits.</li>
        		
        		<li>Reduce number of spills.</li>
        	</ul>
        	<br>
        	<b>Endorsement</b>
        	
        	<p>As Vice President, I affirm my commitment to this policy in the name of Third Coast Terminals. This policy is communicated to all persons working for or on behalf of the organization, stakeholders and available to the public. This policy is documented, implemented and maintained following ISO and RC 14001&reg; guidelines and guiding principles.
        		<br />
                Grif Carnes, Vice President</p>

        	<br>
        	<br>
        	<h3>Security Policy Statement</h3>
        	
        	<p>Third Coast Terminals has an obligation to protect the privacy of all employees and ensure their personal security on the job. The Security Policy's goal is to protect the organization's assets against internal, external, deliberate or accidental threats.</p>
        	
        	<ul>
        		<li>Confidentiality of information will be assured;</li>
        		
        		<li>Integrity of information/assets maintained;</li>
        		
        		<li>Legislative and regulatory requirements will be met;</li>
        		
        		<li>Business continuity plans will be continually maintained.</li>
        	</ul>
        	
        	<p>The objective is to ensure that Third Coast Terminal's minimizes the risk of damage by preventing security incidents and reducing their potential impact.</p>
        	
        	<p>Paul Glenn
        		<br>
        		VP Human Resources</p>
        	<br>
        	<br>
            <h3>Permits</h3>
            <ul style="font-size: 10.5pt; color: #333333">

                <li>
                    Texas Department of Agriculture (TDA) - Weights and Measures
                </li>

                <li>

                    Department of Transportation (DOT) - Hazmat Registration
                </li>

                <li>
                    Food and Drug Administration (FDA) - Drug Facility Registration
                </li>

                <li>
                    Texas Commission on Environmental Quality (TCEQ) - Waste Water
                </li>

                <li>
                    Food and Drug Administration (FDA) - Food Facility Registration
                    Department of State Health Services - Wholesale Distributor of
                    Drugs
                </li>

                <li>
                    Texas Department of State Health Services - Wholesale Distributor of
                    Drugs
                </li>

                <li>
                    State of Texas (Commissioner of Health) - Hazardous Substances
                    Distributor
                </li>

                <li>
                    Environmental Protection Agency (EPA) Pesticide Establishment
                </li>

                <li>
                    State of Texas Dept. of Public Safety permit - Precursor Chemicals
                    and Laboratory Apparatus
                </li>

                <li>
                    Department of Treasury Alcohol and Tobacco Operating permit -
                    Warehousing and processing of Spirits for Industrial Use
                </li>

            </ul>

        </div>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
