<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <h3 dir="ltr"><span>Take the Quiz</span></h3>
            <p>Click here to download our <a href="assets/pdf/Third-Coast-Visitor-Orientation-Quiz.pdf" target="_blank">visitor orientation quiz</a>.</p>

            <p><br><br></p>

            <h3 dir="ltr"><span>Planning on visiting our facility?</span></h3>

            <p>Watch the appropriate safety orientation video.</p>

            <p></p>

            <p></p>

            <p></p>

            <p></p>

            <p></p>

            <p></p>

            <p></p></div>

        <div class="col-lg-6">
          <iframe width="320" height="205" src="https://www.youtube.com/embed/jFC67rMjMj0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

          <iframe width="320" height="205" src="https://www.youtube.com/embed/Hv-KVmrXxXA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


        </div>

    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
