<?php include "header.php"
?>
<style>
.ion-chevron-left{
	background: #fff;
    padding: 10px;    
    z-index: 9999999;
    position: absolute;
    left: 20px;
}
.ion-chevron-right{
	background: #fff;
    padding: 10px;    
    z-index: 9999999;
    position: absolute;
    right: 20px;
}
.pagi{
	float: left;
    width: 100%;
    text-align: center;
}
.pagi ol{
	padding: 0;
    margin: 0 auto;
    position: absolute;
    bottom: 4px;    
    left: 0;
    right: 0;
	margin-top: 10px !important;
}
.pagi ol li{
	display: inline-block;
    background: #0C5E86;
    width: 4% !important;
    height: 5px !important;
    cursor: pointer;
    margin: 0 5px;
	padding: 1px 14px !important;
}
.carousel-indicators li.active a{
	color:#fff;
}
.carousel-indicators li a:hover,
.carousel-indicators li a:focus{
	color:#fff;
	text-decoration:none;
}

#photo .carousel-indicators > li , #photo .carousel-indicators > li.active {
	padding:0 5px;
}
#photo .carousel-indicators > li a, #photo .carousel-indicators > li.active a{
     width: 100%; 
    float:left;
padding:5px;	
}
</style>
<?php include("filereader.php"); 
$dynamo = ''; $craw =''; $astros = ''; $break = ''; $summer = ''; $christmas = '';
$thanks = ''; $special = '';
   if(isset($_GET['gallery']) && $_GET['gallery'] != ''){
   $gallery = $_GET['gallery'];
   switch($gallery){
	   case 1:
	       $gallery_template = $dynamo_game;
		   $dynamo = 'class="active"';
		   break;
	   case 2:
	       $gallery_template = $crawfish_boil;
		   $craw = 'class="active"';
		   break;
       case 3:
	       $gallery_template = $astros_game;
		   $astros = 'class="active"';
		   break;
       case 4:
	       $gallery_template = $breakfast;
		   $break = 'class="active"';
		   break;
       case 5:
	       $gallery_template = $summer_picnic;
		   $summer = 'class="active';
		   break;
	   case 6:
	       $gallery_template = $christmas_party;
		   $christmas = 'class="active"';
		   break;
	   case 7:
	       $gallery_template = $thanksgiving_lunch;
		   $thanks = 'class="active"';
		   break;		   
	   case 8:
	       $gallery_template = $special_events;
		   $special = 'class="active"';
		   break;
       default:
	       $gallery_template = $dynamo_game;
		   $dynamo = 'class="active"';
		   break;
   }
   }else{
	   $gallery_template = $dynamo_game;
	   $dynamo = 'class="active"';
   }

?>
 <div class="container" id="photo">
       <div id="myCarousel" class="carousel slide" >
            <!-- Wrapper for slides -->
        	<?php echo $gallery_template; ?>
            

            <!-- Left and right controls -->
            
          <!--   <a class="previous carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="ion-minus-round" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            <a class="next carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="ion-minus-round" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>-->
			
            
        </div>
		<div>
                <ol class="carousel-indicators album-buttons" style="position: static">
                   <!-- <li class="load_carousel active" id="dynamo_game" data-template="dynamo_game">Dynamo Game</li>
                    <li class="load_carousel" data-template="crawfish_boil">Crawfish Boil</li>
                    <li class="load_carousel" data-template="astros_game">Astros Games</li>
                    <li class="load_carousel" data-template="breakfast">Breakfast @ The Gate</li>
                    <li class="load_carousel" data-template="summer_picnic">Summer Picnic</li>
                    <li class="load_carousel" data-template="christmas_party">Christmas Party</li>
                    <li class="load_carousel" data-template="thanksgiving_lunch">Thanksgiving Lunch</li>
                    <li class="load_carousel" data-template="speical_events">Special Events</li> -->
					
					 <li <?php echo $dynamo; ?> id="dynamo_game" data-template="dynamo_game"><a href="photo-gallery.php">Dynamo Game</a></li>
                    <li <?php echo $craw; ?> data-template="crawfish_boil"><a href="photo-gallery.php?gallery=2">Crawfish Boil</a></li>
                    <li <?php echo $astros; ?> data-template="astros_game"><a href="photo-gallery.php?gallery=3">Astros Games</a></li>
                    <li <?php echo $break; ?> data-template="breakfast"><a href="photo-gallery.php?gallery=4">Breakfast @ The Gate</a></li>
                    <li <?php echo $summer; ?> data-template="summer_picnic"><a href="photo-gallery.php?gallery=5">Summer Picnic</a></li>
                    <li <?php echo $christmas; ?> data-template="christmas_party"><a href="photo-gallery.php?gallery=6">Christmas Party</a></li>
                    <li <?php echo $thanks; ?> data-template="thanksgiving_lunch"><a href="photo-gallery.php?gallery=7">Thanksgiving Lunch</a></li>
                    <li <?php echo $special; ?> data-template="speical_events"><a href="photo-gallery.php?gallery=8">Special Events</a></li>
                </ol>
            </div>
    </div>

<!--========================================================        FOOTER
=========================================================-->

<script>
var $dynamo_game   = '<?php echo $dynamo_game; ?>';
var $crawfish_boil = '<?php echo $crawfish_boil; ?>';
var $astros_game   = '<?php echo $astros_game; ?>';
var $breakfast     = '<?php echo $breakfast; ?>';
var $summer_picnic = '<?php echo $summer_picnic; ?>';
var $christmas_party = '<?php echo $christmas_party; ?>';
var $thanksgiving_lunch = '<?php echo $thanksgiving_lunch; ?>';
var $special_events     = '<?php echo $special_events; ?>';
</script>
<?php include "footer.php" ?>
