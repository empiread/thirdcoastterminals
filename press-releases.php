<?php include "header.php" ?>


<div class="container ">

 <div class="mtb-25">
        <div class="col-lg-6">
          <p>October 14, 2020</p>
   <h2>NSR Permit Renewal and Amendment Application</h2>
<p>On behalf of Third Coast Packaging, Inc. (Third Coast), TRC Environmental Corporation (TRC) hereby submits
the following minor New Source Review (NSR) air quality permit application to renew and amend NSR
Permit No. 92403 pursuant to 30 TAC Chapter 116, Subchapter D and Subchapter B requirements,
respectively. The subject NSR permit authorizes a drum and tote packaging facility located in Pearland,
Brazoria County, Texas.</p>
<p>An electronic version of the application, including the Excel workbook Form PI‐1 General Application
(version 4.0), Excel Electronic Modeling Evaluation Workbook (EMEW) for non‐SCREEN3 (version 2.3), and
Appendix B ‐ Excel Emission Calculations workbook has been submitted to TCEQ Air Permits Division via
STEERS e‐Permits, and the appropriate permit application fee was paid electronically during the online
submission.</p>

              

            <p>To download the entire press release, <a href="Permit_92403_Public_Notice.pdf" target="_blank" title="Download press releases.">Click Here</a></p>
        </div>

        <div class="col-lg-6">
          <p><br></p>
            <a href="Permit_92403_Public_Notice.pdf" target="_blank"><img class="img-responsive" src="assets/images/press-release-10-2020.jpg" title="NSR Permit Renewal and Amendment Application"
                 alt="NSR Permit Renewal and Amendment Application"></a>


        </div>
    </div>
<p>&nbsp;&nbsp;</p><p>&nbsp;&nbsp;</p>
  

 <div class="mtb-25">
        <div class="col-lg-6">
          <p>July 9, 2019</p>
   <h2>         Third Coast Expands Executive Team and Appoints
   Jim Thompson as Chief Operating Officer</h2>
<p>COO appointment will help power Third Coast’s continued growth
Pearland, TX – Third Coast, a leading chemical contract manufacturer,
announces the appointment of Jim Thompson to the role of Chief Operating Officer.
“[Jim] Thompson understands domestic and global chemical sales, plant operations, and
customer satisfaction in a way that few executives can hope to, and he has a proven record of
maximizing the growth potential of Third Coast facilities,” stated Jim Clawson, founder and CEO
of Third Coast. </p>
<p>“As Chief Operating Officer, Mr. Thompson will be responsible for the day-to-day operations of
each of the Third Coast companies, and we are thrilled to add his valuable leadership to our
executive team in this period of rapid growth,” added Jett Rominger, Chief Financial Officer</p>

              

            <p>To download the entire press release, <a href="assets/pdf/Third-Coast-Terminals-Appoints-Jim-Thompson-COO.pdf" target="_blank" title="Download press releases.">Click Here</a></p>
        </div>

        <div class="col-lg-6">
          <p><br></p>
            <a href="assets/pdf/Third-Coast-Terminals-Appoints-Jim-Thompson-COO.pdf" target="_blank"><img class="img-responsive" src="images/press-release-jim-thompson-coo.png" title="Online security software press release"
                 alt="Online security software press release"></a>

<p>&nbsp;&nbsp;</p>
        </div>
    </div>


	<div class="mtb-25">
        <div class="col-lg-12">
        	<p>&nbsp;</p>
			<p>July 6, 2017 (Featured in August/September 2017 issue of PU Magazine.)</p>
        	<h2>Successful first year for Third Coast's Prepolymer Toll Manufacturing Unit</h2>
        	<p>Third Coast Terminals LLC– Pearland, Texas USA</p>
        	
            		</div>
		<div class="col-lg-12">
			<p>Prepolymer contract manufacturer Third Coast Terminals LLC announced a full year of successful prepolymer toll manufacturing at its state-of-the-art prepolymer facility in Pearland, TX, USA. The unit was completed as scheduled in 2016 and commissioned flawlessly, the company said in a statement issued on 6 July 2017.</p>

		</div>
		
		<div class="col-lg-6">
              <p>
                <b>Third Coast</b> CEO Jim Clawson noted, “When we installed this unit at our Pearland, Texas facility, we planned to be a significant player in prepolymer tolling. The addition of this capability is directly in line with our strategy to be a full service provider to our customers. We have been running the unit flawlessly for over a year now and look to increase our throughput with more tolling”.  
              </p>
              <p>The unit is comprised of two matched pairs of reactors, two for full scale production. In addition there is a pilot plant unit. Both production pairs can produce between 6,000–32,000 lbs. (~ 2.7–14.5 t) of prepolymer per batch and are designed to handle very high viscosities. The new system is of stainless steel construction, fully automated, and is complemented by the site’s ample storage and blending capabilities.</p>
        </div>
        
        <div class="col-lg-6">      
              
			  <p>The facility is capable of handling MDI, TDI and aliphatic di-isocyanates, as well as producing a full range of polyether and polyester prepolymers. It has been carefully designed to allow the addition of small quantities of additives, thereby allowing customers to produce very specialized products.</p>
			  <p>The company said that it has a wealth of experience in polyurethanes, and, in addition to prepolymer tolling, offers in house engineering, process design, extensive laboratory facilities, and developmental scale reactors. The Pearland facility has over 170 storage tanks and is rail connected with the ability to load into railcars, tank trucks, ISO containers, flexi-bags, totes, drums and pails. Third Coast has drum and tote packaging lines for white room (food grade, halal and kosher certified), industrial and vapour controlled products.</p>
            <p>&nbsp;</p>
        </div>
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
        	<p style="text-align:center; font-style:italic;">Third Coast’s new system has been designed to allow the addition of small quantities of additives, thereby allowing
customers to produce specialized products.</p>
            <img class="img-responsive" src="images/Prepolymer-Tolling.jpg" title="" alt="">

        </div>
        <div class="col-lg-2"></div>
    </div>
    
    <div style="width: 100%; padding: 0 0 10px 0; float: left;  border-bottom: 1px #dfdfdf solid"><br></div>
	
	
	
	<div class="mtb-25">
		<p>&nbsp;</p>
		<img src="images/ThirdCoast_Fundraiser_Infographic-01.png" title="Third Coast Fundraiser" alt="Third Coast Fundraiser">
	</div>
	
	<div class="mtb-25">
        <div class="col-lg-6">
        	<a name="anchor" id="anchor"></a><h2>Third Coast Terminals Makes Generous Donation</h2>
        	<h4>Students in Pearland Benefit from Third Coast Terminals Donation</h4>
            <p>Teachers and staff at EA Lawhon Elementary School in Pearland ISD were amazed at the initiative and generosity of Third Coast Terminals' employees who decided to support a local oranization that has a positive impact in their community. They chose Communities In Schools at Lawhon Elementary School.</p>

              <p>
                The employees' original idea of purchasing sweatshirts for one hundred students identified by the school and the CIS site coordinator grew to the great surprise of the executive staff at Third Coast Terminals. They were able to collect nearly 250 sweatshirts! At that point, they decided to raise money and purchase clothing, under garments, shoes, etc. for students at Lawhon.  
              </p>
              
              <p>The departments in the company completed and used various techniques to raise money including a Las Vegas trip raffle, taco sales, and barbecue plates. Through their efforts, the company was able to raise over $15,000. The owner of Third Coast Terminals, Jim Clawson, Jr., generously agreed to match any amount of money that his employees raised, bringing the total donation to over $30,000. Approximately $10,000 was used to purchase clothing, games, and shoes for the students at Lawhon Elementary School, the remaining dollars will be distributed evenly among the other five CIS schools in Pearland, Texas: Carleston Elementary, Cockrell Elementary, Jamison and Sablatura Middle School and Pearland Junior High South.</p>

            <p>&nbsp;</p>
        </div>

        <div class="col-lg-6">
            <video style="padding-left: 55px;" width="450" height="340" controls Autoplay="autoplay">
  			<source src="assets/video/Third-Coast-Terminals-Makes-Donation-To-Benefit-Elementary-Students.mp4" type="video/mp4">
</video>

        </div>
    </div>

<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

         <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
              <p>
                Texas Mutual Insurance Company announced that Third Coast Packaging, Inc. in Galveston County has been awarded the company’s top honor for workplace safety.  
              </p>

            <p>To download the entire press release, <a href="images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<br>
            <a href="images/SafetyAwardsPressRelease-GalvestonCountyFINAL.pdf" target="_blank"><img class="img-responsive" src="images/TXM-logo-2014-ol.png" title="Texas Mutual: Workers' Compensation Insurance"
                 alt="Texas Mutual: Workers' Compensation Insurance logo"></a>


        </div>
    </div>


<div style="width: 100%; padding: 0 0px 0 0; float: left;  border-bottom: 1px #dfdfdf solid"></div>

    
    <div class="mtb-25">
        <div class="col-lg-6">
        	<p><br></p>
            <p>Third Coast Terminals has deployed M-Files in a hybrid cloud environment for disaster recovery
              to ensure the company's content is secured, protected and available in case the company's on-premises
              systems are compromised by a hurricane or another catastrophic event.</p>

              <p>
                “Being located in the hurricane prone Gulf Coast region,
                we required an information management solution that could provide disaster
                recovery capabilities and ensure our vital information assets were protected in the
                event of a natural disaster situation or hardware failure. Implementing M-Files in a
                HYBRID CLOUD environment provides us with a business continuity plan and disaster recovery
                system that will keep us up and running at all times.”  
              </p>

            <p>To download the entire press release, <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank" title="Download press releases.">Click Here</a></p>
        </div>

        <div class="col-lg-6">
        	<p><br></p>
            <a href="https://www.m-files.com/en/press-release-third-coast-terminals" target="_blank"><img class="img-responsive" src="images/press-release-third-coast-hybrid-cloud-disaster-recovery.jpg" title="Online security software press release"
                 alt="Online security software press release"></a>

<p>&nbsp;&nbsp;</p>
        </div>
    </div>


    

</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
