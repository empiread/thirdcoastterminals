<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">
        	<p>Third Coast has many years of experience in railcar loading.</p>

            <h3>Rail &amp; Transportation</h3>

            <p>Third Coast Terminals continued to expand our rail storage to meet the aggressive growth of our customers
                with this need. Our latest expansion has increased on site storage to a new total of more than 80+ rail
                cars. We have also increased the number of process storage positions for railcars up to a total of 12.
                We
                are served by the BNSF main line which provides excellent service/logistics.</p>

            <h3>Rail &amp; Transportation Transloading</h3>



            <p>
                Third Coast Terminals has the capability to perform all of the unloading services related to rail such
                as
                rail to truck transfers, rail access to our drumming facility and rail to terminal transfers.</p>

            <h3>Transloading</h3>

             <p> From loading an ISO container into a storage tank, or loading from a rail car into a tank truck, Third
                Coast
                can handle all of your transloading needs. Transloading with Third Coast will allow your company to
                gain
                the economic benefits of a rail service and the on-time, flexibility of a tank truck delivery. </p>

            <p> With tank truck weight scales throughout the facility, a highly trained staff, and room for 80+ railcars on our property – Third Coast offers your company transloading which is another safe, reliable alternative to achieve all of your logistical needs. </p>
                
               <p>"We have added new ISO and Tank Truck Steaming Racks to our original capacity. This new capability allows us to steam up to 15 ISO’s at a time for more efficient unloading of product. This capability allows our customers a faster turnaround time on ISO’s as they can go directly on steam when they arrive at the plant. Based on our initial success, we will be expanding this capability to 30 spots in the near future."</p>



            <h3>Railcar Loading</h3>

            <p>

                One of our most popular services is railcar loading directly from a storage tank. Customers who store products with us have the ability to quickly and easily send product from their storage tank via rail to any number of their customers. Our experience, tank storage, and access to the BNSF rail line directly from our property makes this process efficient and very cost effective for your transportation requirements.</p>

            <h3>Railcar Unloading</h3>



            <p>

                Due to the layout of our facility, we are able to provide many services surrounding the use of railcars.
                We
                can unload directly into storage tanks, trans-load into tank trucks or ISO containers, or even unload
                directly into our packaging facility. </p>

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/railcar-loading.jpg" title="Railcar Loading" alt="Railcar Loading">
                    </div>
                    <div class="item">
                        <img src="images/transloading-services6.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>

                    <div class="item">
                        <img src="assets/images/transloading.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading-3.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading-2.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    <div class="item">
                        <img src="assets/images/rail-services-transloading.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>

					<div class="item">
                        <img src="images/railcar.jpg" title="Railcar" alt="Railcar">
                    </div>

                    <div class="item">
                        <img src="images/railcar-unloading.jpg" title="Railcar Unloading" alt="Railcar Unloading">
                    </div>

                    <div class="item">
                        <img src="images/railcar-pumping.jpg" title="Railcar Pumping" alt="Railcar Pumping">
                    </div>

					<div class="item">
                        <img src="images/railcar-transloading.jpg" title="Railcar Transloading" alt="Railcar Transloading">
                    </div>
                    <div class="item">
                        <img src="images/transloading-services7.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    
                    <div class="item">
                        <img src="images/transloading-services8.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    
                    <div class="item">
                        <img src="images/transloading-services.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>

                    <div class="item">
                        <img src="images/transloading-services2.jpg" title="Trans-Loading Services"
                             alt="Trans-Loading Services">

                    </div>


                    <div class="item">
                        <img src="images/transloading-services5.jpg" title="Trans-loading Services"
                             alt="Transloading Services">

                    </div>

                    

                </div>

                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>

    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
