$(document).ready(function(){
	var $windowSize = $(window).width();
	
    $(".grides").click(function(){
		if($windowSize >= 480){ 
		$(".gride-over").hide();
		$(this).find(".gride-over").show();
		}else{
			
		}
    });
	
	
	$('.grides_hover').on('mouseenter', function(){
      var dataID = $(this).attr("data-id");
	  if($windowSize >= 480){ 
		$('.grides_hover').css('border','1px solid #d0d0d0');
		$(".gride-over").hide();	
		$(this).find(".gride-over").show();
		google.maps.event.trigger(gmarkers[dataID], 'click');
		var myLatlng2 = new google.maps.LatLng(8.581021, 9.272461);
		map.setCenter(myLatlng2);
		map.setZoom(3);	
	  }else{
		  $(this).find(".gride-over").hide();
	  }
   }).on('mouseleave', function(){
	    if($windowSize >= 480){  
		  $(this).find(".gride-over").show();	
		  $(this).find(".gride-over").hide();
		}
   });

	var mouseLeave = 0;
	
	$("#CSL-btn").click(function(e){
		e.preventDefault();		
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[0], 'click');	
		 
		}else{
		
			//$('.grides_mobile').hide();
			//$('.gride-over').css('display','none');
		    //$('.CSL-btn_open').show();	
		}		 
    });
	
	//if($windowSize <= 480){ 
		$('#grides_01').on("click",function(e){			
		    e.preventDefault(); 
			var $html = '<div class="grides_mobile CSL-btn_open"><h2>Chemical Specialties Ltd.</h2><h3>Jim Thompson</h3>';
			$html += '<h4>Vice President</h4><p class="tel_num">Office <a href="tel:+1 281 412 2888">+1 281 412 2888</a></p>';
			$html += '<p class="tel_num">Mobile <a href="tel:+1 281 736 8959">+1 281 736 8959</a></p><p><a href="mailto:jdthompson@thirdcoastint.com">jdthompson@thirdcoastint.com</a></p>';
			$html += '<br><h3>Billy Lo</h3><h4>Asia Business Manager - HK</h4><p class="tel_num">Office <a href="tel:+852 9810 4527">';
			$html += '+'+'852 9810 4527</a></p><p><a href="mailto:bio-hk@3cchemicals.com">bio-hk@3cchemicals.com</a></p>';
		    $html += '<a class="vist_web" target="_blank" href="http://www.chemspecialties.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();			 
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_02').click(function(e){	
			var $html = '<div class="grides_mobile STAT-btn_open"><h2>STAT</h2><h3>Jim Thompson</h3><h4>Vice President</h4>';
			$html += '<p class="tel_num">Office <a href="tel:+1 281 412 2888">+1 281 412 2888</a></p>';
			$html += '<p><a href="mailto:jdthompson@thirdcoastint.com">jdthompson@thirdcoastint.com</a></p>';
			$html += '<a class="vist_web" target="_blank" href="http://www.stat-sg.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_03').click(function(e){		
			var $html = '<div class="grides_mobile Analytical-btn_open"><h2>Third Coast Analytical</h2><h3>Donny Boynton</h3><h4>Vice President</h4>';
			$html += '<p class="tel_num">Office <a href="tel:+1 281 997 5036">+1 281 997 5036</a></p><p class="tel_num">Mobile <a href="tel:+1 281 389 9996">+1 281 389 9996</a></p>';
			$html += '<p><a href="mailto:dboynton@3cterminals.com">dboynton@3cterminals.com</a></p><br><h3>Yong Ming</h3><h4>Research Scientist</h4><p class="tel_num">Office <a href="tel:+1 346 207 4702">+1 346 207 4702</a></p>';
			$html += '<p> <a href="mailto:Yming@3cterminals.com">Yming@3cterminals.com</a></p><a class="vist_web" target="_blank" href="http://www.thirdcoastanalyticaltechnologies.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_04').click(function(e){		
		  var $html  =  '<div class="grides_mobile Chemicals-btn_open"><h2>Third Coast Chemicals</h2><h3>Steven English</h3>';
			  $html += '<h4>Vice President</h4><p class="tel_num">Office <a href="tel:+44 7841 390992">+44 7841 390992</a></p>';
			  $html += '<p><a href="mailto:s.english@thirdcoastint.com">s.english@thirdcoastint.com</a></p><br>';
			  $html += '<h3>Angie Griffin</h3><h4>Buisness Manager</h4><p class="tel_num">Mobile <a href="tel:+1 713 598 8781">+1 713 598 8781</a></p><p><a href="mailto:agriffin@3cchemicals.com">agriffin@3cchemicals.com</a></p><br>';
			  $html += '<h3>Kenny Keisig</h3><h4>General Manager</h4><p class="tel_num">Office <a href="tel:+1 281 412 2888">+1 281 412 2888</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 832 418 0006">+1 832 418 0006</a></p><p><a href="mailto:kheisig@3cchemicals.com">kheisig@3cchemicals.com</a></p><br>';
			  $html += '<h3>David Chetlin</h3><h4>Business Development Manager</h4><p class="tel_num">Office <a href="tel:+1 281 997 5080">+1 281 997 5080</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 972 533 1777">+1 972 533 1777</a></p><p><a href="mailto:dchetlin@3cchemicals.com">dchetlin@3cchemicals.com</a></p>';
			  $html += '<a class="vist_web" target="_blank" href="http://www.thirdcoastchemicals.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_05').click(function(e){		
		  var $html  = '<div class="grides_mobile Terminals-btn_open"><h2>Third Coast Terminals</h2><h3>Grif Carnes</h3>';	  
			  $html += '<p class="tel_num">Office <a href="tel:+1 281 412 0275">+1 281 412 0275</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 713 817 4832">+1 713 817 4832</a></p>';	       
			  $html += '<p><a href="mailto:gcarnes@3cchemicals.com">gcarnes@3cchemicals.com</a></p><br>';
			  $html += '<h3>Glenn F.Hermann</h3><h4>Sales Account Manager - Automotive Fluids</h4><p class="tel_num">Office <a href="tel:+1 281 997 5082">+1 281 997 5082</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 832 729 4536">+1 832 729 4536</a></p><p><a href="mailto:ghermann@3cchemicals.com">ghermann@3cchemicals.com</a></p><br>';
			  $html += '<h3>Trudy Wells</h3><h4>Business Development Manager</h4>	  <p class="tel_num">Mobile <a href="tel:+1 989 600 1557">+1 989 600 1557</a></p>';
			  $html += '<p><a href="mailto:t.wells@thirdcoastint.com">t.wells@thirdcoastint.com</a></p><a class="vist_web" target="_blank" href="http://thirdcoastterminals.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_06').click(function(e){		
		  var $html  = '<div class="grides_mobile International-btn_open"><h2>Third Coast International</h2><h3>Steven English</h3>';
	          $html += '<h4>Commercial and Marketing Vice President</h4><p class="tel_num">Office <a href="tel:+44 7841 390992">+44 7841 390992</a></p><p><a href="mailto:s.english@thirdcoastint.com">s.english@thirdcoastint.com</a></p><br>';
			  $html += '<h3>Martin Staley</h3><h4>Vice President EMEA</h4><p class="tel_num">Office <a href="tel:+44 1344 423108">+44 1344 423108</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+44 7920 130440">+44 7920 130440</a></p><p><a href="mailto:m.staley@thirdcoastint.com">m.staley@thirdcoastint.com</a></p><br>';
			  $html += '<h3>Kevin Pickin</h3><h4>Vice President</h4><p class="tel_num">Office <a href="tel:+44 7917 44264">+44 7917 44264</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 989 600 1557">+1 989 600 1557</a></p><p><a href="mailto:k.pickin@thirdcoastint.com">k.pickin@thirdcoastint.com</a></p><br>';
			  $html += '<h3>Jim Thompson</h3><h4>Vice President</h4><p class="tel_num">Office <a href="tel:+1 281 412 2888">+1 281 412 2888</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+1 281 736 8959">+1 281 736 8959</a></p><p><a href="mailto:jdthompson@thirdcoastint.com">jdthompson@thirdcoastint.com</a></p>';
			  $html += '<a class="vist_web" target="_blank" href="http://thirdcoastinternational.com/">View Our Website</a></div>';	
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
		$('#grides_07').click(function(e){		
		  var $html  = '<div class="grides_mobile Qatar-btn_open"><h2>Third Coast International - Qatar</h2><h3>Martin Staley</h3>';
			  $html += '<h4>Vice President EMEA</h4><p class="tel_num">Office <a href="tel:+44 1344 423108">+44 1344 423108</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+44 7920 130440">+44 7920 130440</a></p><p><a href="mailto:m.staley@thirdcoastint.com">m.staley@thirdcoastint.com</a></p><br>';
			  $html += '<h3>Fawad Rana</h3><h4>Chief Executive Officer</h4><p class="tel_num">Office <a href="tel:+974 44 58 3277">+974 44 58 3277</a></p>';
			  $html += '<p class="tel_num">Mobile <a href="tel:+974 55 54 8710">+974 55 54 8710</a></p><p><a href="mailto:f.rana@tciqatar.com">f.rana@tciqatar.com</a></p>';
			  $html += '<a class="vist_web" target="_blank" href="http://www.thirdcoastinternationalqatar.com/">View Our Website</a></div>';
			$('.open_content').html($html);
			$('.header_content').hide();
			 e.preventDefault();
			  $('html, body').animate({
                scrollTop: $(".open_content").offset().top
              }, 1000);           
		});
	//}
	
	$("#STAT-btn").click(function(e){
		e.preventDefault();		
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[1], 'click');	
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.STAT-btn_open').show();*/
		}
    });
	
	$("#Analytical-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[2], 'click');	
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Analytical-btn_open').show();*/
		}
    });
	$("#Chemicals-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[3], 'click');
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Chemicals-btn_open').show();*/
		}	
    });
	$("#Terminals-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[4], 'click');
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Terminals-btn_open').show();*/
		}	
    });
	$("#International-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[5], 'click');
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.International-btn_open').show();*/
		}
    });
	$("#Qatar-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[6], 'click');
		}else{
			/*$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Qatar-btn_open').show();*/
		}
    });
});
function gridesHover(){ 
$(".grides_hover").hover(function(){
		var dataID = $(this).attr("data-id");
	  $(".gride-over").hide();	
      $(this).find(".gride-over").show();
		google.maps.event.trigger(gmarkers[dataID], 'click');
		 var myLatlng2 = new google.maps.LatLng(26.509905, 18.149414);
		map.setCenter(myLatlng2);
        map.setZoom(3);	
		$(this).addClass('grides_hover');
    });

}

