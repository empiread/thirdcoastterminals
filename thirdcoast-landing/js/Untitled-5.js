

var locations = [
      ['Third Coast International', 22.297605, 114.170278, 7],
      ['Third Coast Terminals', 29.573179, -95.294812, 6],
      ['Third Coast Chemicals', 29.573179, -95.294812, 5],
      ['Third Coast Analytical', 29.573179, -95.294812, 4],
      ['Chemical Specialties Limited', 1.2736119,103.7190661, 3],
      ['STAT', 1.2736119,103.7190661, 2],
      ['Third Coast International - Qatar', 25.213651, 51.052497, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      center: new google.maps.LatLng(25.213651, 51.052497),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
	
	
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		icon: 'images/map-icon-red-small.png' ,
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
	
