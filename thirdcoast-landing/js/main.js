$(document).ready(function(){
	var $windowSize = $(window).width();
	if($windowSize >= 480){ 
    $(".grides").click(function(){
		$(".gride-over").hide();
		$(this).find(".gride-over").show();
    });
	
	
	$('.grides_hover').on('mouseenter', function(){
      var dataID = $(this).attr("data-id");
	  $('.grides_hover').css('border','1px solid #d0d0d0');
	  $(".gride-over").hide();	
      $(this).find(".gride-over").show();
	  google.maps.event.trigger(gmarkers[dataID], 'click');
	  var myLatlng2 = new google.maps.LatLng(8.581021, 9.272461);
	  map.setCenter(myLatlng2);
      map.setZoom(3);	
   }).on('mouseleave', function(){
      $(this).find(".gride-over").show();	
      $(this).find(".gride-over").hide();
   });
	}

	var mouseLeave = 0;
	
	
	$("#CSL-btn").click(function(e){
		e.preventDefault();		
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[0], 'click');			 
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.CSL-btn_open').show();	
		}*/
		 
    });
	
	$('#grides01').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.CSL-btn_open').show();	
		movetoUpDiv('CSL-btn_open');	
	});
	
	$("#STAT-btn").click(function(e){
		e.preventDefault();		
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[1], 'click');	
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.STAT-btn_open').show();
		}*/
    });
	
	$('#grides02').on("click",function(e){
		e.preventDefault();			
		$('.grides_mobile').hide();
		$('.STAT-btn_open').show();	
		movetoUpDiv('STAT-btn_open');
	});
	
	$("#Analytical-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		 google.maps.event.trigger(gmarkers1[2], 'click');	
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Analytical-btn_open').show();
		}*/
    });
	
	$('#grides03').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.Analytical-btn_open').show();	
		movetoUpDiv('Analytical-btn_open');
	});
	
	$("#Chemicals-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[3], 'click');
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Chemicals-btn_open').show();
		}	*/
    });
	
	$('#grides04').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.Chemicals-btn_open').show();
        movetoUpDiv('Chemicals-btn_open');		
	});
	
	$("#Terminals-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[4], 'click');
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Terminals-btn_open').show();
		}	*/
    });
	
	$('#grides05').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.Terminals-btn_open').show();
		movetoUpDiv('Terminals-btn_open');		
	});
	
	$("#International-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[5], 'click');
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.International-btn_open').show();
		}*/
    });
	
	$('#grides06').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.International-btn_open').show();
        movetoUpDiv('International-btn_open');			
	});
	
	$("#Qatar-btn").click(function(e){
		e.preventDefault();	
		$(this).parent().parent().css('border','2px solid #A1A0A0');
		if($windowSize >= 480){ 
		google.maps.event.trigger(gmarkers1[6], 'click');
		}/*else{
			$('.grides_mobile').hide();
			$('.gride-over').css('display','none');
		    $('.Qatar-btn_open').show();
		}*/
    });
	
	$('#grides07').on("click",function(e){
		e.preventDefault();	
		$('.grides_mobile').hide();
		$('.Qatar-btn_open').show();	
		movetoUpDiv('Qatar-btn_open');	
	});
	
});

function movetoUpDiv(clsName){
	$('html, body').animate({
                scrollTop: $("."+clsName).offset().top
              }, 1000);    
}

function gridesHover(){ 
$(".grides_hover").hover(function(){
		var dataID = $(this).attr("data-id");
	  $(".gride-over").hide();	
      $(this).find(".gride-over").show();
		google.maps.event.trigger(gmarkers[dataID], 'click');
		 var myLatlng2 = new google.maps.LatLng(26.509905, 18.149414);
		map.setCenter(myLatlng2);
        map.setZoom(3);	
		$(this).addClass('grides_hover');
    });

}

