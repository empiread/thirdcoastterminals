<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>Third Coast Terminals is uniquely qualified to mix and process our customer’s basic to complex chemical
                formulations into their final products. Our experience, facilities and quality processes ensure that we
                exceed our customer’s expectations.</p>

            <p>It’s a great idea to use our expertise, specialized equipment, and resources to complete your Toll
                Manufacturing. The use of our tools and experience are a much better alternative than a single company
                incurring the infrastructure cost, training cost, and time to gain experience associated with mixing
                products in-house.</p>

            <p>Then, add our custom packaging, storage and logistics to this offer – and you can see why customers
                continue
                to seek us out for their chemical tolling needs.</p>

        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/toll-blenders.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item">
                        <img src="images/toll-manufacturing.jpg" title="Toll Manufacturing" alt="Toll Manufacturing">
                    </div>

                    <div class="item">
                        <img src="images/toll-blenders-4.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item ">
                        <img src="images/toll-blenders-3.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                    <div class="item ">
                        <img src="assets/images/reaction-operation-area-4.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>

                      <div class="item ">
                        <img src="assets/images/third-coast-terminals-terminaling.jpg" title="Toll Blenders" alt="Toll Blenders">
                    </div>



                </div>


                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
