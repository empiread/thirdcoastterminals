<?php include "header.php" ?>


<div class="container" id="shadow">

    <div class="mtb-25">
        <div class="col-lg-6">

            <p>Trailer storage is offered for our blending, reaction chemistry, trans-loading and packaging customers.
                You will find that the ability and flexibility to store trailers of your drums, totes, or pails will
                expedite the packaging process and delivery of your products to the end user. By limiting the offering
                of this service to only Third Coast customers we ensure you have timely access to your product, product
                packaging or shipping needs.</p>

            <p>Another similar service we offer is our “drop and swap” plan for your ISO and/or 20ft containers. Many of
                our customers take advantage of our ability to accept an empty container today, fill it with product
                over the next few days, and prepare it for shipment to ensure that you meet your cutoff date at the
                port. This service creates a system that helps you get the most out of your pickup and delivery
                fleet.</p>

        </div>

        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/trailer-storage.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-1.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-2.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-3.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>

                    <div class="item">
                        <img src="images/trailer-store-4.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>
                    
                    <div class="item">
                        <img src="images/trailer-storage-2.jpg" title="Container Storage" alt="Drop and Swap">
                    </div>
                </div>

                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>


            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
