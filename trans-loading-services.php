<?php include "header.php" ?>
<div class="container ">
    <div class="mtb-25">
        <div class="col-lg-6">

            <p> From loading an ISO container into a storage tank, or loading from a rail car into a tank truck, Third
                Coast
                can handle all of your trans-loading needs. Trans-loading with Third Coast will allow your company to
                gain
                the economic benefits of a rail service and the on-time, flexibility of a tank truck delivery. </p>

            <p> With tank truck weight scales throughout the facility, a highly trained staff, and room for 80+ railcars
                on
                our property – Third Coast offers your company trans-loading which is another safe, reliable alternative
                to
                achieve all of your logistical needs. </p>
                
               <p>"We have added new ISO and Tank Truck Steaming Racks to our original capacity. This new capability allows us to steam up to 15 ISO’s at a time for more efficient unloading of product. This capability allows our customers a faster turnaround time on ISO’s as they can go directly on steam when they arrive at the plant. Based on our initial success, we will be expanding this capability to 30 spots in the near future."</p>
        </div>
        <div class="col-lg-6">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">


                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                	<div class="item active">
                        <img src="images/transloading-services7.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    
                    <div class="item">
                        <img src="images/transloading-services8.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>
                    
                    <div class="item">
                        <img src="images/transloading-services.jpg" title="Trans-loading Services"
                             alt="Transloading Services">
                    </div>

                    <div class="item">
                        <img src="images/transloading-services2.jpg" title="Trans-Loading Services"
                             alt="Trans-Loading Services">

                    </div>


                    <div class="item">
                        <img src="images/transloading-services5.jpg" title="Trans-loading Services"
                             alt="Transloading Services">

                    </div>

                    <div class="item">
                        <img src="images/transloading-services6.jpg" title="Trans-loading Services"
                             alt="Transloading Services">

                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>

            </div>


        </div>
    </div>
</div>

<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
