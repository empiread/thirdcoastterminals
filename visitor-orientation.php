<?php include "header.php" ?>


<div class="container ">

    <div class="mtb-25">
        <div class="col-lg-6">

             <h2>Take the Quiz</h2>
            <p></p>
            <p>Click here to download our <strong><a href="assets/pdf/Third-Coast-Visitor-Orientation-Quiz.pdf" target="_blank">visitor orientation quiz</a></strong>.</p>
            <p>&nbsp;</p>

            <h2>Planning on visiting our facility?</h2>
            <p></p>
            <p>Watch the appropriate safety orientation video.</p>
            <p>&nbsp;</p>

            </div>

        <div class="col-lg-6">

        </div>

    </div>

    <div class="mtb-25">
        <div class="col-lg-12">
            <iframe class="visitorOrientationVideo1" width="460" height="315" src="https://www.youtube.com/embed/jFC67rMjMj0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

            <iframe class="visitorOrientationVideo2" width="460" height="315" src="https://www.youtube.com/embed/Hv-KVmrXxXA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</div>


<!--========================================================
                        FOOTER
=========================================================-->
<?php include "footer.php" ?>
